<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
      'country_id','name','description','city_image','video_url','geo_lat','geo_long',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    public function cityData(){
        return $this->hasOne(CityData::class,'city_id','id');
    }
    // public function cityCategories()
    // {
    //     return $this->hasMany(CityCategory::class,'city_id','id');
    // }
    public function cityCategoryDetails()
    {
        return $this->hasMany(CityCategoryDetail::class,'city_id','id');
    }
}
