<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FriendRequest extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
        'sender_id', 'recipient_id', 'status' ,
    ];

    protected $casts = [
        'sender_id' => 'integer',
        'recipient_id' => 'integer'
    ];
    /**
     * Relationship with users.
     *
     */
    public function user()
    {
        return $this->hasOne(User::class, 'ID', 'recipient_id');
    }

    public function usersender()
    {
        return $this->hasOne(User::class, 'ID', 'sender_id');
    }

    public function recipientUser()
    {
        return $this->hasOne(User::class, 'ID', 'recipient_id');
    }

    public function senderUser()
    {
        return $this->hasOne(User::class, 'ID', 'sender_id');
    }
}
