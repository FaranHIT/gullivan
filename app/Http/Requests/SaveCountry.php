<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveCountry extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name' => 'required|string',
            'country_img' =>'required|image|mimes:jpeg,png,jpg,svg|max:2048'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Country Name is Required',
            'country_img.required' => 'Country Image is Required',
            'country_img.image' => 'Country Image Must be an Image File',
        ];
    }
}
