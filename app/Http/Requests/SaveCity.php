<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveCity extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'place' => 'required|string',
            'country_id' => 'required',
            'city_img' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
        ];
    }
    public function messages()
    {
        return [
            'place.required' => 'City Name is Required',
            'country_id.required' => 'Country is Required',
            'city_img.required' => 'City Image is Required',
            'city_img.image' => 'City Image Must be an Image File',
        ];
    }
}
