<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use App\FriendRequest;
use App\Friendship;
use App\UserProfile;

class FriendController extends Controller
{
    protected $statustype = [
		'confirmed',
        'pending' ,
        'blocked'
    ];
    /**
     *Send Friend Request
     */
    public function sendFriendRequest(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'recipient_id' => 'required|exists:wp_users,ID',
            ],
            [
                'exists' => 'User Not Exists',
                'required' => 'recipient id requrieds!'
            ]
        );
        if ($validator->fails()) {
            return response()->errors($validator->errors()->first());
        }
        $senderblockcheck = FriendRequest::where('sender_id','=',Auth::id())
                            ->where('recipient_id','=',$request->recipient_id)
                            ->where('status','=','blocked')
                            ->first();
        if($senderblockcheck)
        {
            return response()->errors('blocked user not send request');
        }
        $recipientblockcheck = FriendRequest::where('sender_id','=',$request->recipient_id)
                            ->where('recipient_id','=',Auth::id())
                            ->where('status','=','blocked')
                            ->first();
        if($recipientblockcheck)
        {
            return response()->errors('blocked user not send request');
        }
        $sendFriendRequest = FriendRequest::create(
            [
                'sender_id' => Auth::id(),
                'recipient_id' => $request->recipient_id
            ]
        );
        if ($sendFriendRequest) {
            $token = User::getToken($request->recipient_id);
            if ($token) {
                app()->make('FcmService')->sendNotification($token, "Friend Request", "You Just Got a new friend", ["data" => ['identifier' => 'NR', 'user_id'=>Auth::id()]]);
            }
            return response()->success('Friend Request Send Successful',$sendFriendRequest);
        } else {
            return response()->error('Not Send Friend Request');
        }

    }
    /**
     *Send Friend Request Accepted
     */
     public function FriendRequestFeedback(Request $request)
     {
        $validator          = Validator::make($request->all(), [
            'sender_id'         => 'required',
            'recipient_id'      => 'required',
            'status'            => 'required',
            'id'                => 'required'
        ]);
        if ($validator->fails()) {
            return response()->errors('sender and recipient id requrieds!');
        }
        if ($request->status=='confirmed') {
            $acceptRequest = FriendRequest::where(['id'=>$request->id,'sender_id'=>$request->sender_id])->first();
            if ($acceptRequest) {
                $acceptRequest->status = $request->status;
                $acceptRequest->save();
                // status confirmed then data inset into friendship table
                $friendShip = new Friendship;
                $friendShip->sender_id    = $request->sender_id;
                $friendShip->recipient_id = $request->recipient_id;
                $friendShip->save();
                $token = User::getToken($friendShip->sender_id);
                if ($token) {
                    app()->make('FcmService')->sendNotification($token, "Friend Request", "Your Request has been Accepted", ["data" => "data"]);
                }
                return response()->success('Successful User Accepted Request',$friendShip);
            } else {
                return response()->error('User Not Accepted Request');
            }
        } if ($request->status=='rejected') {
            $acceptRequest = FriendRequest::where(['id'=>$request->id,'sender_id'=>$request->sender_id])->first();
            if ($acceptRequest) {
                $acceptRequest->status = $request->status;
                $acceptRequest->save();
                $token = User::getToken($request->sender_id);
                if ($token) {
                    app()->make('FcmService')->sendNotification($token, "Friend Request", "Your Request has been Rejected", ["data" => "data"]);
                }
                return response()->success('User Rejected the Request',$acceptRequest);
            } else {
                return response()->error('Not rejected!');
            }
        }
     }

     /**
     *List of Friends as Sender
     */
    public function FriendForSender(Request $request)
    {

        $users = Friendship::where([ 'sender_id' => Auth::id() ])->first();
        if ($users) {
         return response()->success('Successful List of Friends',$users);
        } else {
            return response()->error('Freinds Not Found');
        }
    }

    /**
     *List of Friends as Recipient
     */
    public function FriendForRecipient()
    {
        $users = User::with('friendShipRecipients')->get();
        if ($users) {
            return response()->success('Successful List of Friends',$users);
        } else {
            return response()->error('Freinds Not Found');
        }
    }
    /**
     *Elastic Search User Model by Name
     */
    public function search(Request $request)
    {
        $searchUser = User::search($request->search)->get();
        if ($searchUser) {
            return response()->success('Successfull',$searchUser);
        } else {
            return response()->error(' Not Found');
        }
    }

    /**
     * Get all users Pending Request
     * @return \Illuminate\Http\Response
     */
    public function getAllPendingRequest()
    {
        $userPending = User::where('status','=','pending')->get();
        if (($userPending)) {
            return response()->success('Successfull',$userPending);
        } else {
            return response()->error('No Record Found');
        }
    }
    /**
     *Get Profile
     */
    public function getProfile()
    {
        $getProfile = UserProfile::where('id',$id)->first();
        if ($getProfile) {
            return response()->success('Successfull',$getProfile);
        } else {
            return response()->error('Not Found');
        }
    }
    public function acceptRequest($id)
    {
        $acceptRequest  = FriendRequest::where('id',$id)->first();
        $acceptRequest->status = 'confirmed';
        if ($acceptRequest->save()) {
            $token = User::getToken($acceptRequest->sender_id);
            if ($token) {
                app()->make('FcmService')->sendNotification($token, "Friend Request", "Your Request has been Accepted", ["data" => ['identifier' => 'RA', 'user_id'=>Auth::id()]]);
            }
            return response()->success('Successful',$acceptRequest);
        } else {
           return response()->error('Accept User Request');
        }
    }


      public function denyRequest($id)
      {
        $denyRequest  = FriendRequest::where('id',$id)->first();
        $denyRequest->status = 'rejected';
        if ($denyRequest->save()) {
            $token = User::getToken($denyRequest->sender_id);
            if ($token) {
                app()->make('FcmService')->sendNotification($token, "Friend Request", "Your Request has been Rejected", ["data" => ['identifier' =>'RR']]);
            }
           return response()->success('Successful',$denyRequest);
        } else {
           return response()->error('Accept User Request');
        }
    }

 public function FriendRequestStatusUpdate(Request $request)
 {
    $validator = Validator::make($request->all(),[
        'id'    => 'required' ,
        'status' => 'required'
        ]);

        if($validator->fails())
        {
           return response()->errors('all field required');
        }
        if (!in_array($request->status, $this->statustype))
        {
            return response()->error('Invalid status type.. plz enter confirmed or pending');
        }

        $friendRequest = FriendRequest::where(['id'=>$request->id])->first();
        if($friendRequest !== null)
        {
        if($friendRequest->status == $request->status)
        {
           $friendRequest->status = 'delete';
        }
        else
        {
            return response()->errors('sorry request not exist according to id or status');
        }
        if($friendRequest->save())
        {
            return response()->success('successfully',$friendRequest);
        }
        else
        {
            return response()->errors('sorry plz enter valid request ID');
        }
    }
    else
    {
        return response()->errors('plz enter valid request ID');
    }
 }

 public function BlockUnblock(Request $request)
 {
    $validator = Validator::make($request->all(),[
        'id'    => 'required' ,
        'status' => 'required'
        ]);

        if($validator->fails())
        {
           return response()->errors('all field required');
        }
        if (!in_array($request->status, $this->statustype))
        {
            return response()->error('Invalid status type.. plz enter confirmed or blocked');
        }

        $friendRequest = FriendRequest::where(['id'=>$request->id])->first();
        if($friendRequest !== null)
        {
    if($request->status == 'confirmed' && $friendRequest->status =='confirmed')
    {
            $friendRequest->status = 'blocked';
            $friendRequest->blocked_by = Auth::id();
    }
    else if($request->status == 'blocked' && $friendRequest->status =='blocked')
    {
            $friendRequest->status = 'unblocked';
    }
    else
    {
            return response()->errors('sorry request not exist according to id or status');
    }
        if($friendRequest->save())
        {
            return response()->success('successfully',$friendRequest);
        }
        else
        {
            return response()->errors('sorry plz enter valid request ID');
        }
    }
    else
    {
        return response()->errors('plz enter valid request ID');
    }
 }

    // /**
    //  * Get all users Pending Request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function getAllUserPendingRequest()
    // {
    //     $userPending = User::with('friendRequests')->get();
    //     if (($userPending)) {
    //         return response()->success('Successfull',$userPending);
    //     } else {
    //         return response()->error('No Record Found');
    //     }
    // }
    // /**
    //  *Get Profile
    //  */
    // public function getProfile(Request $request)
    // {
    //     $getProfile = UserProfile::where(['user_id'=>$request->user_id])->get();
    //     if ($getProfile) {
    //         return response()->success('Successfull',$getProfile);
    //     } else {
    //         return response()->error('Not Found');
    //     }
    // }
}
