<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Mail\ForgotPasswordMail;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\UserMeta;
use App\FriendRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use phpDocumentor\Reflection\Types\Boolean;
use Illuminate\Support\Facades\Validator;
use phpseclib\Crypt\Random;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use MikeMcLin\WpPassword\Facades\WpPassword;
use App\Traits\ImageUploadTrait;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use App\UserNotification;
use Illuminate\Support\Facades\Storage;



class UserController extends Controller
{
	use ImageUploadTrait;
    protected $profile_picture = 'profile_picture';
    protected $devicetype = [
		'ios',
		'android'
    ];

	protected $friendStatus = [
		'pending',
		'confirmed',
		'rejected',
		'blocked'
    ];

    protected $BlockListStatus = [
        'blocked'
    ];

    protected $UserStatus = ['online','offline','busy'];
    protected $PhoneStatus = ['hidden','unhidden'];
    protected $EmailStatus = ['hidden','unhidden'];
    protected $PrivateStatus = ['yes','no'];
    protected $LoginType = ['email','username'];

	public function friendsStatus(Request $request)
    {
        $userId = Auth::id();
        $status = $request->status;
        $word   = $request->word;
        $merged = null;
        $recipientStatusUser = $senderStatusUser = function($query) use ($word) {
            $query->where(
                'wp_users.user_nicename',
                'LIKE',
                '%' . $word . '%'
            )
            ->orWhere(
                'wp_users.first_name',
                'LIKE',
                '%' . $word . '%'
            )
            ->orWhere(
                'wp_users.last_name',
                'LIKE',
                '%' . $word . '%'
            )
            ->orWhere(
                'wp_users.user_email',
                'LIKE',
                '%' . $word . '%'
            );
        };
        $recipientStatus = $senderStatus = function($query) use ($status,$userId) {
            $query->where('friend_requests.status', '=', $status);
        };
        $validator = Validator::make($request->all(), [
            'page'  => 'required'
        ]);

        if (!in_array($status, $this->friendStatus))
        {
            return response()->error('Invalid Status');
        }
        if($word != '')
        {
            $friendsWith = User::
            with(
                [
                    'senderStatus' => $senderStatus,
                    'senderStatus.user' => $senderStatusUser
                ]
            )
            ->where('wp_users.ID', $userId)
            ->first();

            foreach ($friendsWith->senderStatus as $index => &$friend) {
                if (is_null($friend->user)) {
                    $friendsWith->senderStatus->forget($index);
                }
            }
            $friendsWith->setRelation('senderStatus', $friendsWith->senderStatus->values());

            $friendsOf = User::
            with(
                [
                    'recipientStatus' => $recipientStatus,
                    'recipientStatus.senderuser' => $recipientStatusUser
                ]
            )
            ->where('wp_users.ID', $userId)
            ->first();

            foreach ($friendsOf->recipientStatus as $index => &$friend) {
                if (is_null($friend->senderuser)) {
                    $friendsOf->recipientStatus->forget($index);
                }
                $friend->user=$friend->senderuser;
                unset($friend->senderuser);
            }
            $friendsOf->setRelation('recipientStatus', $friendsOf->recipientStatus->values());
        }
        else
        {
            $friendsWith = User::with(
                [
                    'senderStatus'=> $senderStatus,
                    'senderStatus.user'
                ]
            )
            ->where('wp_users.ID', $userId)
            ->first();
            foreach ($friendsWith->senderStatus as $index => &$friend) {
                if (is_null($friend->user)) {
                    $friendsWith->senderStatus->forget($index);
                }
            }
            $friendsWith->setRelation('senderStatus', $friendsWith->senderStatus->values());

            $friendsOf = User::with(
                [
                    'recipientStatus'=> $recipientStatus,
                    'recipientStatus.senderuser'
                ]
            )
            ->where('wp_users.ID', $userId)
            ->first();
            foreach ($friendsOf->recipientStatus as $index => &$friend) {
                if (is_null($friend->senderuser)) {
                    $friendsOf->recipientStatus->forget($index);
                }
                $friend->user=$friend->senderuser;
                unset($friend->senderuser);
            }
            $friendsOf->setRelation('recipientStatus', $friendsOf->recipientStatus->values());
        }
        $sender = $friendsWith->senderStatus ?? null;
        $recipient = $friendsOf->recipientStatus ?? null;
        if (!is_null($sender) && !is_null($recipient)) {
            $merged = $sender->merge($recipient);
        }
        $page =$request->page;
        $limit =$request->limit;
        $result = $this->paginate($merged, $page, $limit);
        if ($result)
        {
            return response()->json([$result], 200);
        }
        else
        {
            return response()->error('No Record Found');
        }
    }

    public function getBlockUser(Request $request)
    {
        $userId = Auth::id();
        $status = $request->status;
        $word   = $request->word;
        $merged = null;
        $recipientStatusUser = $senderStatusUser = function($query) use ($word) {
            $query->where(
                'wp_users.user_nicename',
                'LIKE',
                '%' . $word . '%'
            )
            ->orWhere(
                'wp_users.first_name',
                'LIKE',
                '%' . $word . '%'
            )
            ->orWhere(
                'wp_users.last_name',
                'LIKE',
                '%' . $word . '%'
            )
            ->orWhere(
                'wp_users.user_email',
                'LIKE',
                '%' . $word . '%'
            );
        };
        $recipientStatus = $senderStatus = function($query) use ($status,$userId) {
            $query->where([
                'friend_requests.status'=>$status ,
                'friend_requests.blocked_by'=>$userId
            ]);
        };
        $validator = Validator::make($request->all(), [
            'page'  => 'required'
        ]);

        if (!in_array($status, $this->BlockListStatus))
        {
            return response()->error('Invalid Status');
        }
        if($word != '')
        {
            $friendsWith = User::
            with(
                [
                    'senderStatus' => $senderStatus,
                    'senderStatus.user' => $senderStatusUser
                ]
            )
            ->where('wp_users.ID', $userId)
            ->first();

            foreach ($friendsWith->senderStatus as $index => &$friend) {
                if (is_null($friend->user)) {
                    $friendsWith->senderStatus->forget($index);
                }
            }
            $friendsWith->setRelation('senderStatus', $friendsWith->senderStatus->values());

            $friendsOf = User::
            with(
                [
                    'recipientStatus' => $recipientStatus,
                    'recipientStatus.senderuser' => $recipientStatusUser
                ]
            )
            ->where('wp_users.ID', $userId)
            ->first();

            foreach ($friendsOf->recipientStatus as $index => &$friend) {
                if (is_null($friend->senderuser)) {
                    $friendsOf->recipientStatus->forget($index);
                }
                $friend->user=$friend->senderuser;
                unset($friend->senderuser);
            }
            $friendsOf->setRelation('recipientStatus', $friendsOf->recipientStatus->values());
        }
        else
        {
            $friendsWith = User::with(
                [
                    'senderStatus'=> $senderStatus,
                    'senderStatus.user'
                ]
            )
            ->where('wp_users.ID', $userId)
            ->first();
            foreach ($friendsWith->senderStatus as $index => &$friend) {
                if (is_null($friend->user)) {
                    $friendsWith->senderStatus->forget($index);
                }
            }
            $friendsWith->setRelation('senderStatus', $friendsWith->senderStatus->values());

            $friendsOf = User::with(
                [
                    'recipientStatus'=> $recipientStatus,
                    'recipientStatus.senderuser'
                ]
            )
            ->where('wp_users.ID', $userId)
            ->first();
            foreach ($friendsOf->recipientStatus as $index => &$friend) {
                if (is_null($friend->senderuser)) {
                    $friendsOf->recipientStatus->forget($index);
                }
                $friend->user=$friend->senderuser;
                unset($friend->senderuser);
            }
            $friendsOf->setRelation('recipientStatus', $friendsOf->recipientStatus->values());
        }
        $sender = $friendsWith->senderStatus ?? null;
        $recipient = $friendsOf->recipientStatus ?? null;
        if (!is_null($sender) && !is_null($recipient)) {
            $merged = $sender->merge($recipient);
        }
        $page =$request->page;
        $limit =$request->limit;
        $result = $this->paginate($merged, $page, $limit);
        if ($result)
        {
            return response()->json([$result], 200);
        }
        else
        {
            return response()->error('No Record Found');
        }
    }

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){

        $validator       = Validator::make($request->all(), [
            'device_token'      => 'required',
            'device_type'       => 'required',
            'login_type'        => 'required',  
        ]);

        if ($validator->fails()) {
            return response()->errors('Registration Unsuccessful');
        }
        if(!in_array($request->login_type,$this->LoginType))
        {
            return response()->errors('Invalid Login Type');
        }

    if($request->login_type == 'email')
    {
          $user = User::where(['user_email' => $request->email])->first();
          if($user && WpPassword::check($request->password, $user->user_pass))
          {
            $users = User::where(['user_email' => $request->email])->update([
            'device_token' => $request->device_token,
            'device_type'  => $request->device_type
            ]);
            $success['user'] = $user;
            $user['token'] =  $user->createToken('Gullivan')->accessToken;

            $LoginId = $user->ID;
            $UpdateFcmToken = UserMeta::where(['user_id'=>$LoginId])->where(['meta_key'=>'fcm_device_token'])
            ->update(['meta_value'=>$request->device_token]);

            return response()->success('Login Successful',$user);
          } 
          else 
          {
            return response()->error('Unauthorized Access');
          }
    }
    else
    {
        $user = User::where(['user_nicename' => $request->email])->first();
        if($user && WpPassword::check($request->password, $user->user_pass))
        {
          $users = User::where(['user_nicename' => $request->email])->update([
          'device_token' => $request->device_token,
          'device_type'  => $request->device_type
          ]);
          $success['user'] = $user;
          $user['token'] =  $user->createToken('Gullivan')->accessToken;

          $LoginId = $user->ID;
          $UpdateFcmToken = UserMeta::where(['user_id'=>$LoginId])->where(['meta_key'=>'fcm_device_token'])
          ->update(['meta_value'=>$request->device_token]);

          return response()->success('Login Successful',$user);
        } 
        else 
        {
          return response()->error('Unauthorized Access');
        }
    }
    }

    /**
     * Logout api
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $user_id = Auth::id();
        UserMeta::where(['user_id'=>$user_id])
       ->where(['meta_key'=>'fcm_device_token'])
       ->update(['meta_value'=>'NULL']);

       return response()->success('Logout Successful');
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator       = Validator::make($request->all(), [
            'name'             => 'required|unique:wp_users,user_nicename',
            'first_name'       => 'required',
            'last_name'        => 'required',
            'email'            => 'required|string|email|unique:wp_users,user_email',
            'password'         => 'required',
            'c_password'       => 'required|same:password',
            'phone'            => 'required',
            'nationality'      => 'required',
            'language'         => 'required',
            // 'profile_picture'  => 'required',
            'fcm_token'        => 'required',
            'device_type'      => 'required'
        ]);

        if ($validator->fails()) {
            return response()->errors($validator->errors()->first());
        }

        $type=$request->device_type;
        if (!in_array($type, $this->devicetype)) {
                return response()->error('Invalid type');
        }

        $user = User::create([
            'user_nicename'     => $request->name,
            'first_name'        => $request->first_name,
            'last_name'         => $request->last_name,
            'user_email'        => $request->email,
            'user_pass'         => WpPassword::make($request->password),
            'phone'             => $request->phone,
            'nationality'       => $request->nationality,
            'language'          => $request->language,
            'profile_picture'   => $request->profile_picture,
        ]);

        if(!is_null($request->profile_picture))
        {
            $RegisterUser = User::where('ID',$user->ID)->first();
            $base64_string = $request->profile_picture; // your base64 encoded      
            $imageName = 'profile'.'-'.$RegisterUser->ID.'.'.'png';   
            Storage::disk('public')->put($imageName, base64_decode($base64_string));
        }
             
            UserMeta::insert([
            [
                'user_id' => $user->ID,
                'meta_key' => 'nickname',
                'meta_value' => $user->user_nicename
            ],
            [
                'user_id' => $user->ID,
                'meta_key' => 'first_name',
                'meta_value' => $user->first_name
            ],
            [
                'user_id' => $user->ID,
                'meta_key' => 'last_name',
                'meta_value' => $user->last_name
            ],
            [
                'user_id' => $user->ID,
                'meta_key' => 'description',
                'meta_value' => $user->description
            ],
            [
                'user_id' => $user->ID,
                'meta_key' => 'rich_editing',
                'meta_value' => 'true'
            ],
            [
                'user_id' => $user->ID,
                'meta_key' => 'syntax_highlighting',
                'meta_value' => 'true'
            ],
            [
                'user_id' => $user->ID,
                'meta_key' => 'comment_shortcuts',
                'meta_value' => 'false'
            ],
            [
                'user_id' => $user->ID,
                'meta_key' => 'admin_color',
                'meta_value' => 'fresh'
            ],
            [
                'user_id' => $user->ID,
                'meta_key' => 'use_ssl',
                'meta_value' => '0'
            ],
            [
                'user_id' => $user->ID,
                'meta_key' => 'show_admin_bar_front',
                'meta_value' => 'true'
            ],
            [
                'user_id' => $user->ID,
                'meta_key' => 'locale',
                'meta_value' => $user->language
            ],
            [
                'user_id' => $user->ID,
                'meta_key' => 'wp_capabilities',
                'meta_value' => 'a:1:{s:10:"subscriber";b:1;}'
            ],
            [
                'user_id' => $user->ID,
                'meta_key' => 'wp_user_level',
                'meta_value' => '0'
            ],
            [
                'user_id' => $user->ID,
                'meta_key' => '_yoast_wpseo_profile_updated',
                'meta_value' => Carbon::now()->timestamp
            ],
            [
                'user_id' => $user->ID,
                'meta_key' => 'dismissed_wp_pointers',
                'meta_value' => ''
            ],
            [
                'user_id' => $user->ID,
                'meta_key' => 'fcm_device_token',
                'meta_value' => $request->fcm_token
            ],
            [
                'user_id' => $user->ID,
                'meta_key' => 'device_type',
                'meta_value' => $type

            ]
        ]);

        if($user)
        {
            $success['user'] = $user;
            $user['token'] =  $user->createToken('Gullivan')->accessToken;

            return response()->success('Successfully created user!',$success);
            }
            else
            {
                return response()->error('Not created user');
            }
    }
    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        if (!empty($user)) {
            return response()->success('Successfully created user!',$user);
        } else {
            return response()->error('No Record Found');
        }
    }
    /**
     * Get all users
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllUser(Request $request)
    {
        $usersCollections=[];
        $CountryFilter = $request->country;
        $NationalityFilter = $request->nationality;
        $LanguageFilter = $request->language;
        $userId = Auth::id();
        if(!is_null($CountryFilter) && !is_null($NationalityFilter) && !is_null($LanguageFilter))
        {
            $CountryFilterUserId = UserMeta::where([
                'meta_key' => 'country' , 
                'meta_value' => $CountryFilter
                ])->pluck('user_id')->toArray();
            
            $blockedByUsers = FriendRequest::where(
                [
                    'sender_id' => $userId,
                    'status' => 'blocked'
                ]
            )->where(
                'blocked_by', '!=' , $userId
            )->pluck('recipient_id')->toArray();
            array_push($blockedByUsers, $userId);
            $validator = Validator::make($request->all(), [
                'page'  => 'required'
            ]);
    
            if($request->word != '')
            {
                foreach($CountryFilterUserId as $ID)
                {
                $users = User::with(
                    [
                        'friendRequests' => function($query) use ($userId) {
                            $query->where('friend_requests.sender_id', '=', $userId);
                            $query->where('friend_requests.status', '!=', 'blocked');
                        },
                        'friendRequests.user',
                        'senderStatus' => function($query) use ($userId) {
                            $query->where('friend_requests.recipient_id', '=', $userId);
                            $query->orWhere(function($subQuery) use ($userId) {
                                $subQuery->where('friend_requests.blocked_by', '=', $userId)->where('friend_requests.status', '=', 'blocked');
                            });
                        },
                        'senderStatus.usersender'
                    ]
                )
                ->whereNotIn('ID', $blockedByUsers)
                ->where('ID', $ID)
                ->where('nationality',$NationalityFilter)
                ->where('language',$LanguageFilter)
                ->where(function($query) use ($request) {
                    $query->where(
                        'user_nicename',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'first_name',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'last_name',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'user_email',
                        'LIKE',
                        '%' . $request->word . '%'
                    );
                })
                ->get();
                $usersCollections[]=$users;
            }
        if(!empty($usersCollections))
        {
            $NewCollection = new Collection();
            foreach($usersCollections as $items)
            {
                foreach($items as $item)
                {
                   $users = $NewCollection->push($item);
                }
            }
            
                $page = $request->page;
                $limit = $request->limit;
                $users = $this->paginate($users, $page, $limit, [], true);
        }    
        else
        {
            $page =$request->page;
            $limit =$request->limit;
            $users = $this->paginate($usersCollections,$page,$limit, [], true);
        }
            }else{
                foreach($CountryFilterUserId as $ID)
                {
                $users = User::with(
                    [
                        'friendRequests' => function($query) use ($userId) {
                            $query->where('friend_requests.sender_id', '=', $userId);
                            $query->where('friend_requests.status', '!=', 'blocked');
                        },
                        'friendRequests.user',
                        'senderStatus' => function($query) use ($userId) {
                            $query->where('friend_requests.recipient_id', '=', $userId);
                            $query->orWhere(function($subQuery) use ($userId) {
                                $subQuery->where('friend_requests.blocked_by', '=', $userId)->where('friend_requests.status', '=', 'blocked');
                            });
                        },
                        'senderStatus.usersender' 
                    ]
                )->whereNotIn('ID', $blockedByUsers)
                 ->where('ID', $ID)
                 ->where('nationality',$NationalityFilter)
                 ->where('language',$LanguageFilter)
                 ->get();

                $usersCollections[]=$users;        
                }
            if(!empty($usersCollections))
            {
                $NewCollection = new Collection();
                foreach($usersCollections as $items)
                {
                    foreach($items as $item)
                    {
                       $users = $NewCollection->add($item);
                    }
                }    
                $page =$request->page;
                $limit =$request->limit;
                $users = $this->paginate($users,$page,$limit, [], true);
            }
            else
            {
                $page =$request->page;
                $limit =$request->limit;
                $users = $this->paginate($usersCollections,$page,$limit, [], true);
            }
                    
            }
        }
        else if(!is_null($CountryFilter) && !is_null($NationalityFilter))
        {
            $CountryFilterUserId = UserMeta::where([
                'meta_key' => 'country' , 
                'meta_value' => $CountryFilter
                ])->pluck('user_id')->toArray();
            
            $blockedByUsers = FriendRequest::where(
                [
                    'sender_id' => $userId,
                    'status' => 'blocked'
                ]
            )->where(
                'blocked_by', '!=' , $userId
            )->pluck('recipient_id')->toArray();
            array_push($blockedByUsers, $userId);
            $validator = Validator::make($request->all(), [
                'page'  => 'required'
            ]);
    
            if($request->word != '')
            {
                foreach($CountryFilterUserId as $ID)
                {
                $users = User::with(
                    [
                        'friendRequests' => function($query) use ($userId) {
                            $query->where('friend_requests.sender_id', '=', $userId);
                            $query->where('friend_requests.status', '!=', 'blocked');
                        },
                        'friendRequests.user',
                        'senderStatus' => function($query) use ($userId) {
                            $query->where('friend_requests.recipient_id', '=', $userId);
                            $query->orWhere(function($subQuery) use ($userId) {
                                $subQuery->where('friend_requests.blocked_by', '=', $userId)->where('friend_requests.status', '=', 'blocked');
                            });
                        },
                        'senderStatus.usersender'
                    ]
                )
                ->whereNotIn('ID', $blockedByUsers)
                ->where('ID', $ID)
                ->where('nationality',$NationalityFilter)
                ->where(function($query) use ($request) {
                    $query->where(
                        'user_nicename',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'first_name',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'last_name',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'user_email',
                        'LIKE',
                        '%' . $request->word . '%'
                    );
                })
                ->get();
                $usersCollections[]=$users;
            }
        if(!empty($usersCollections))
        {    
            $NewCollection = new Collection();
            foreach($usersCollections as $items)
            {
                foreach($items as $item)
                {
                   $users = $NewCollection->push($item);
                }
            }
            
                $page = $request->page;
                $limit = $request->limit;
                $users = $this->paginate($users, $page, $limit, [], true);
        }
        else
        {
            $page = $request->page;
            $limit = $request->limit;
            $users = $this->paginate($usersCollections, $page, $limit, [], true);
        } 
            }else{
                foreach($CountryFilterUserId as $ID)
                {
                $users = User::with(
                    [
                        'friendRequests' => function($query) use ($userId) {
                            $query->where('friend_requests.sender_id', '=', $userId);
                            $query->where('friend_requests.status', '!=', 'blocked');
                        },
                        'friendRequests.user',
                        'senderStatus' => function($query) use ($userId) {
                            $query->where('friend_requests.recipient_id', '=', $userId);
                            $query->orWhere(function($subQuery) use ($userId) {
                                $subQuery->where('friend_requests.blocked_by', '=', $userId)->where('friend_requests.status', '=', 'blocked');
                            });
                        },
                        'senderStatus.usersender' 
                    ]
                )->whereNotIn('ID', $blockedByUsers)
                 ->where('ID', $ID)
                 ->where('nationality',$NationalityFilter)
                 ->get();

                $usersCollections[]=$users;        
                }
            if(!empty($usersCollections))
            {    
                $NewCollection = new Collection();
                foreach($usersCollections as $items)
                {
                    foreach($items as $item)
                    {
                       $users = $NewCollection->add($item);
                    }
                }    
                $page =$request->page;
                $limit =$request->limit;
                $users = $this->paginate($users,$page,$limit, [], true);
            }
            else
            {
                $page =$request->page;
                $limit =$request->limit;
                $users = $this->paginate($usersCollections,$page,$limit, [], true); 
            }
                
            }
        }
        else if(!is_null($CountryFilter) && !is_null($LanguageFilter))
        {
            $CountryFilterUserId = UserMeta::where([
                'meta_key' => 'country' , 
                'meta_value' => $CountryFilter
                ])->pluck('user_id')->toArray();
            
            $blockedByUsers = FriendRequest::where(
                [
                    'sender_id' => $userId,
                    'status' => 'blocked'
                ]
            )->where(
                'blocked_by', '!=' , $userId
            )->pluck('recipient_id')->toArray();
            array_push($blockedByUsers, $userId);
            $validator = Validator::make($request->all(), [
                'page'  => 'required'
            ]);
    
            if($request->word != '')
            {
                foreach($CountryFilterUserId as $ID)
                {
                $users = User::with(
                    [
                        'friendRequests' => function($query) use ($userId) {
                            $query->where('friend_requests.sender_id', '=', $userId);
                            $query->where('friend_requests.status', '!=', 'blocked');
                        },
                        'friendRequests.user',
                        'senderStatus' => function($query) use ($userId) {
                            $query->where('friend_requests.recipient_id', '=', $userId);
                            $query->orWhere(function($subQuery) use ($userId) {
                                $subQuery->where('friend_requests.blocked_by', '=', $userId)->where('friend_requests.status', '=', 'blocked');
                            });
                        },
                        'senderStatus.usersender'
                    ]
                )
                ->whereNotIn('ID', $blockedByUsers)
                ->where('ID', $ID)
                ->where('language',$LanguageFilter)
                ->where(function($query) use ($request) {
                    $query->where(
                        'user_nicename',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'first_name',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'last_name',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'user_email',
                        'LIKE',
                        '%' . $request->word . '%'
                    );
                })
                ->get();
                $usersCollections[]=$users;
            }
        if(!empty($usersCollections))
        {    
            $NewCollection = new Collection();
            foreach($usersCollections as $items)
            {
                foreach($items as $item)
                {
                   $users = $NewCollection->push($item);
                }
            }
            
                $page = $request->page;
                $limit = $request->limit;
                $users = $this->paginate($users, $page, $limit, [], true);
        }
        else
        {
            $page = $request->page;
            $limit = $request->limit;
            $users = $this->paginate($usersCollections, $page, $limit, [], true);
        }
            }else{
                foreach($CountryFilterUserId as $ID)
                {
                $users = User::with(
                    [
                        'friendRequests' => function($query) use ($userId) {
                            $query->where('friend_requests.sender_id', '=', $userId);
                            $query->where('friend_requests.status', '!=', 'blocked');
                        },
                        'friendRequests.user',
                        'senderStatus' => function($query) use ($userId) {
                            $query->where('friend_requests.recipient_id', '=', $userId);
                            $query->orWhere(function($subQuery) use ($userId) {
                                $subQuery->where('friend_requests.blocked_by', '=', $userId)->where('friend_requests.status', '=', 'blocked');
                            });
                        },
                        'senderStatus.usersender' 
                    ]
                )->whereNotIn('ID', $blockedByUsers)
                 ->where('ID', $ID)
                 ->where('language',$LanguageFilter)
                 ->get();

                $usersCollections[]=$users;        
                }
            if(!empty($usersCollections))
            {    
                $NewCollection = new Collection();
                foreach($usersCollections as $items)
                {
                    foreach($items as $item)
                    {
                       $users = $NewCollection->add($item);
                    }
                }    
                $page =$request->page;
                $limit =$request->limit;
                $users = $this->paginate($users,$page,$limit, [], true);
            }
            else
            {
                $page =$request->page;
                $limit =$request->limit;
                $users = $this->paginate($usersCollections,$page,$limit, [], true);
            }
                
            }
        }
        else if(!is_null($NationalityFilter) && !is_null($LanguageFilter))
        {
            $blockedByUsers = FriendRequest::where(
                [
                    'sender_id' => $userId,
                    'status' => 'blocked'
                ]
            )->where(
                'blocked_by', '!=' , $userId
            )->pluck('recipient_id')->toArray();
            array_push($blockedByUsers, $userId);
            $validator = Validator::make($request->all(), [
                'page'  => 'required'
            ]);
    
            if($request->word != '')
            {
                $users = User::with(
                    [
                        'friendRequests' => function($query) use ($userId) {
                            $query->where('friend_requests.sender_id', '=', $userId);
                            $query->where('friend_requests.status', '!=', 'blocked');
                        },
                        'friendRequests.user',
                        'senderStatus' => function($query) use ($userId) {
                            $query->where('friend_requests.recipient_id', '=', $userId);
                            $query->orWhere(function($subQuery) use ($userId) {
                                $subQuery->where('friend_requests.blocked_by', '=', $userId)->where('friend_requests.status', '=', 'blocked');
                            });
                        },
                        'senderStatus.usersender'
                    ]
                )
                ->whereNotIn('ID', $blockedByUsers)
                ->where('nationality',$NationalityFilter)
                ->where('language',$LanguageFilter)
                ->where(function($query) use ($request) {
                    $query->where(
                        'user_nicename',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'first_name',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'last_name',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'user_email',
                        'LIKE',
                        '%' . $request->word . '%'
                    );
                })
                ->get();
                $page = $request->page;
                $limit = $request->limit;
                $users = $this->paginate($users, $page, $limit, [], true);
            }else{
                $users = User::with(
                    [
                        'friendRequests' => function($query) use ($userId) {
                            $query->where('friend_requests.sender_id', '=', $userId);
                            $query->where('friend_requests.status', '!=', 'blocked');
                        },
                        'friendRequests.user',
                        'senderStatus' => function($query) use ($userId) {
                            $query->where('friend_requests.recipient_id', '=', $userId);
                            $query->orWhere(function($subQuery) use ($userId) {
                                $subQuery->where('friend_requests.blocked_by', '=', $userId)->where('friend_requests.status', '=', 'blocked');
                            });
                        },
                        'senderStatus.usersender'
                    ]
                )->whereNotIn('ID', $blockedByUsers)
                ->Where('nationality',$NationalityFilter)
                ->Where('language',$LanguageFilter)
                ->get();
    
                $page =$request->page;
                $limit =$request->limit;
                $users = $this->paginate($users,$page,$limit, [], true);
            }    
        }
        else if(!is_null($CountryFilter))
        { 
            $CountryFilterUserId = UserMeta::where([
                'meta_key' => 'country' , 
                'meta_value' => $CountryFilter
                ])->pluck('user_id')->toArray();
            
            $blockedByUsers = FriendRequest::where(
                [
                    'sender_id' => $userId,
                    'status' => 'blocked'
                ]
            )->where(
                'blocked_by', '!=' , $userId
            )->pluck('recipient_id')->toArray();
            array_push($blockedByUsers, $userId);
            $validator = Validator::make($request->all(), [
                'page'  => 'required'
            ]);
    
            if($request->word != '')
            {
                foreach($CountryFilterUserId as $ID)
                {
                $users = User::with(
                    [
                        'friendRequests' => function($query) use ($userId) {
                            $query->where('friend_requests.sender_id', '=', $userId);
                            $query->where('friend_requests.status', '!=', 'blocked');
                        },
                        'friendRequests.user',
                        'senderStatus' => function($query) use ($userId) {
                            $query->where('friend_requests.recipient_id', '=', $userId);
                            $query->orWhere(function($subQuery) use ($userId) {
                                $subQuery->where('friend_requests.blocked_by', '=', $userId)->where('friend_requests.status', '=', 'blocked');
                            });
                        },
                        'senderStatus.usersender'
                    ]
                )
                ->whereNotIn('ID', $blockedByUsers)
                ->where('ID', $ID)
                ->where(function($query) use ($request) {
                    $query->where(
                        'user_nicename',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'first_name',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'last_name',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'user_email',
                        'LIKE',
                        '%' . $request->word . '%'
                    );
                })
                ->get();
                $usersCollections[]=$users;
            }
        if(!empty($usersCollections))
        {
            $NewCollection = new Collection();
            foreach($usersCollections as $items)
            {
                foreach($items as $item)
                {
                   $users = $NewCollection->push($item);
                }
            }
                $page = $request->page;
                $limit = $request->limit;
                $users = $this->paginate($users, $page, $limit, [], true);
        }
        else
        {
            $page = $request->page;
            $limit = $request->limit;
            $users = $this->paginate($usersCollections, $page, $limit, [], true);
        }
            }else{
                foreach($CountryFilterUserId as $ID)
                {
                $users = User::with(
                    [
                        'friendRequests' => function($query) use ($userId) {
                            $query->where('friend_requests.sender_id', '=', $userId);
                            $query->where('friend_requests.status', '!=', 'blocked');
                        },
                        'friendRequests.user',
                        'senderStatus' => function($query) use ($userId) {
                            $query->where('friend_requests.recipient_id', '=', $userId);
                            $query->orWhere(function($subQuery) use ($userId) {
                                $subQuery->where('friend_requests.blocked_by', '=', $userId)->where('friend_requests.status', '=', 'blocked');
                            });
                        },
                        'senderStatus.usersender' 
                    ]
                )->whereNotIn('ID', $blockedByUsers)
                 ->where('ID', $ID)
                ->get();

                $usersCollections[]=$users;        
                }
            if(!empty($usersCollections))
            {    
                $NewCollection = new Collection();
                foreach($usersCollections as $items)
                {
                    foreach($items as $item)
                    {
                       $users = $NewCollection->add($item);
                    }
                }    
                $page =$request->page;
                $limit =$request->limit;
                $users = $this->paginate($users,$page,$limit, [], true);
            }
            else
            {
                $page =$request->page;
                $limit =$request->limit;
                $users = $this->paginate($usersCollections,$page,$limit, [], true);
            }   

            }
        }
        else if(!is_null($NationalityFilter))
        {
            $blockedByUsers = FriendRequest::where(
                [
                    'sender_id' => $userId,
                    'status' => 'blocked'
                ]
            )->where(
                'blocked_by', '!=' , $userId
            )->pluck('recipient_id')->toArray();
            array_push($blockedByUsers, $userId);
            $validator = Validator::make($request->all(), [
                'page'  => 'required'
            ]);
    
            if($request->word != '')
            {
                $users = User::with(
                    [
                        'friendRequests' => function($query) use ($userId) {
                            $query->where('friend_requests.sender_id', '=', $userId);
                            $query->where('friend_requests.status', '!=', 'blocked');
                        },
                        'friendRequests.user',
                        'senderStatus' => function($query) use ($userId) {
                            $query->where('friend_requests.recipient_id', '=', $userId);
                            $query->orWhere(function($subQuery) use ($userId) {
                                $subQuery->where('friend_requests.blocked_by', '=', $userId)->where('friend_requests.status', '=', 'blocked');
                            });
                        },
                        'senderStatus.usersender'
                    ]
                )
                ->whereNotIn('ID', $blockedByUsers)
                ->where('nationality',$NationalityFilter)
                ->where(function($query) use ($request) {
                    $query->where(
                        'user_nicename',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'first_name',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'last_name',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'user_email',
                        'LIKE',
                        '%' . $request->word . '%'
                    );
                })
                ->get();
                $page = $request->page;
                $limit = $request->limit;
                $users = $this->paginate($users, $page, $limit, [], true);
            }else{
                $users = User::with(
                    [
                        'friendRequests' => function($query) use ($userId) {
                            $query->where('friend_requests.sender_id', '=', $userId);
                            $query->where('friend_requests.status', '!=', 'blocked');
                        },
                        'friendRequests.user',
                        'senderStatus' => function($query) use ($userId) {
                            $query->where('friend_requests.recipient_id', '=', $userId);
                            $query->orWhere(function($subQuery) use ($userId) {
                                $subQuery->where('friend_requests.blocked_by', '=', $userId)->where('friend_requests.status', '=', 'blocked');
                            });
                        },
                        'senderStatus.usersender'
                    ]
                )->whereNotIn('ID', $blockedByUsers)
                ->Where('nationality',$NationalityFilter)
                ->get();
    
                // $users = User::whereHas(['friendRequests.user' => function($query) use ($userId) {
                //     $query->where('friend_requests.sender_id', '=', $userId);
                // }])->whereNotIn('ID', [$userId])->get();
    
                // $users = User::with('friendRequests')->whereNotIn('ID', [$request->ID])->get();
    
                // $users = User::with('friendRequests.user')->whereNotIn('ID', [$request->ID])->get();
                $page =$request->page;
                $limit =$request->limit;
                $users = $this->paginate($users,$page,$limit, [], true);
            }    
        }
        else if(!is_null($LanguageFilter))
        {
            $blockedByUsers = FriendRequest::where(
                [
                    'sender_id' => $userId,
                    'status' => 'blocked'
                ]
            )->where(
                'blocked_by', '!=' , $userId
            )->pluck('recipient_id')->toArray();
            array_push($blockedByUsers, $userId);
            $validator = Validator::make($request->all(), [
                'page'  => 'required'
            ]);
    
            if($request->word != '')
            {
                $users = User::with(
                    [
                        'friendRequests' => function($query) use ($userId) {
                            $query->where('friend_requests.sender_id', '=', $userId);
                            $query->where('friend_requests.status', '!=', 'blocked');
                        },
                        'friendRequests.user',
                        'senderStatus' => function($query) use ($userId) {
                            $query->where('friend_requests.recipient_id', '=', $userId);
                            $query->orWhere(function($subQuery) use ($userId) {
                                $subQuery->where('friend_requests.blocked_by', '=', $userId)->where('friend_requests.status', '=', 'blocked');
                            });
                        },
                        'senderStatus.usersender'
                    ]
                )
                ->whereNotIn('ID', $blockedByUsers)
                ->where('language',$LanguageFilter)
                ->where(function($query) use ($request) {
                    $query->where(
                        'user_nicename',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'first_name',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'last_name',
                        'LIKE',
                        '%' . $request->word . '%'
                    )
                    ->orWhere(
                        'user_email',
                        'LIKE',
                        '%' . $request->word . '%'
                    );
                })
                ->get();
                $page = $request->page;
                $limit = $request->limit;
                $users = $this->paginate($users, $page, $limit, [], true);
            }else{
                $users = User::with(
                    [
                        'friendRequests' => function($query) use ($userId) {
                            $query->where('friend_requests.sender_id', '=', $userId);
                            $query->where('friend_requests.status', '!=', 'blocked');
                        },
                        'friendRequests.user',
                        'senderStatus' => function($query) use ($userId) {
                            $query->where('friend_requests.recipient_id', '=', $userId);
                            $query->orWhere(function($subQuery) use ($userId) {
                                $subQuery->where('friend_requests.blocked_by', '=', $userId)->where('friend_requests.status', '=', 'blocked');
                            });
                        },
                        'senderStatus.usersender'
                    ]
                )->whereNotIn('ID', $blockedByUsers)
                ->Where('language',$LanguageFilter)
                ->get();
    
                // $users = User::whereHas(['friendRequests.user' => function($query) use ($userId) {
                //     $query->where('friend_requests.sender_id', '=', $userId);
                // }])->whereNotIn('ID', [$userId])->get();
    
                // $users = User::with('friendRequests')->whereNotIn('ID', [$request->ID])->get();
    
                // $users = User::with('friendRequests.user')->whereNotIn('ID', [$request->ID])->get();
                $page =$request->page;
                $limit =$request->limit;
                $users = $this->paginate($users,$page,$limit, [], true);
            }    
        }
        else
        {
        $blockedByUsers = FriendRequest::where(
            [
                'sender_id' => $userId,
                'status' => 'blocked'
            ]
        )->where(
            'blocked_by', '!=' , $userId
        )->pluck('recipient_id')->toArray();
        array_push($blockedByUsers, $userId);
        $validator = Validator::make($request->all(), [
            'page'  => 'required'
        ]);

        if($request->word != '')
        {
            $users = User::with(
                [
                    'friendRequests' => function($query) use ($userId) {
                        $query->where('friend_requests.sender_id', '=', $userId);
                        $query->where('friend_requests.status', '!=', 'blocked');
                    },
                    'friendRequests.user',
                    'senderStatus' => function($query) use ($userId) {
                        $query->where('friend_requests.recipient_id', '=', $userId);
                        $query->orWhere(function($subQuery) use ($userId) {
                            $subQuery->where('friend_requests.blocked_by', '=', $userId)->where('friend_requests.status', '=', 'blocked');
                        });
                    },
                    'senderStatus.usersender'
                ]
            )
            ->whereNotIn('ID', $blockedByUsers)
            ->where(function($query) use ($request) {
                $query->where(
                    'user_nicename',
                    'LIKE',
                    '%' . $request->word . '%'
                )
                ->orWhere(
                    'first_name',
                    'LIKE',
                    '%' . $request->word . '%'
                )
                ->orWhere(
                    'last_name',
                    'LIKE',
                    '%' . $request->word . '%'
                )
                ->orWhere(
                    'user_email',
                    'LIKE',
                    '%' . $request->word . '%'
                );
            })
            ->get();
            $page = $request->page;
            $limit = $request->limit;
            $users = $this->paginate($users, $page, $limit, [], true);
        }else{
            $users = User::with(
                [
                    'friendRequests' => function($query) use ($userId) {
                        $query->where('friend_requests.sender_id', '=', $userId);
                        $query->where('friend_requests.status', '!=', 'blocked');
                    },
                    'friendRequests.user',
                    'senderStatus' => function($query) use ($userId) {
                        $query->where('friend_requests.recipient_id', '=', $userId);
                        $query->orWhere(function($subQuery) use ($userId) {
                            $subQuery->where('friend_requests.blocked_by', '=', $userId)->where('friend_requests.status', '=', 'blocked');
                        });
                    },
                    'senderStatus.usersender'
                ]
            )->whereNotIn('ID', $blockedByUsers)
            ->get();
            
            // $users = User::whereHas(['friendRequests.user' => function($query) use ($userId) {
            //     $query->where('friend_requests.sender_id', '=', $userId);
            // }])->whereNotIn('ID', [$userId])->get();

            // $users = User::with('friendRequests')->whereNotIn('ID', [$request->ID])->get();

            // $users = User::with('friendRequests.user')->whereNotIn('ID', [$request->ID])->get();
            $page =$request->page;
            $limit =$request->limit;
            $users = $this->paginate($users,$page,$limit, [], true);
        }
    }

        if ($users){
            return response()->json([$users], 200);
        } else {
            return response()->error('No Record Found');
        }
    }

     public function paginate($items,$page, $limit = null, $options = [], $updateRelation = false)
    {
        $limit = is_null($limit) ? 5 : $limit;
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        $filterItems = $items->forPage($page, $limit);
        if ($updateRelation) {
            foreach ($filterItems as &$item) {
                foreach($item->senderStatus as &$value)
                {
                    $value->setRelation('user', $value->usersender);
                    $value->unsetRelation('usersender');
                }

                $friendRequests = collect();

                $friendRequests = $friendRequests->merge($item->friendRequests)
                    ->merge($item->senderStatus);
                $item->unsetRelation('friendRequests')->unsetRelation('senderStatus');
                $friendRequests = $friendRequests->sortBy('id')->values();
                $item->setRelation('friendRequests', $friendRequests);
        }
        }
    // dd($filterItems);
        return new LengthAwarePaginator(
            $filterItems,
            // $items->skip( ($page - 1) * $limit)->take($limit),
            $items->count(),
            $limit,
            $page,
            $options
        );
    }
    /**
     * Get all users Pending Request
     * @return \Illuminate\Http\Response
     */
    public function getAllPendingRequest(Request $request)
    {
        $userPending = FriendRequest::with('senderUser')
            ->where('status','=','pending')
            ->where('recipient_id', Auth::id())
            ->get();
        foreach ($userPending as $key => &$user) {
            $user->user = $user->senderUser;
            unset($user->senderUser);
        }
        if (($userPending)) {
            return response()->success('Successfull',$userPending);
        } else {
            return response()->error('No Record Found');
        }
    }
   public function UpdatePassword(Request $request)
   {
    $validator       = Validator::make($request->all(), [
        'old_password'         => 'required',
        'new_password'         => 'required',
        'confirm_password'       => 'required|same:new_password'
    ]);

    if ($validator->fails()) {
        return response()->errors($validator->errors()->first());
    }
       $NewPassword = Hash::make($request->new_password);
       $UserPassword = User::where('ID',Auth::id())->first();
         if($UserPassword && WpPassword::check($request->old_password, $UserPassword->user_pass))
         {
            $UserPassword->user_pass = $NewPassword;
            $UserPassword->save();
            return response()->success('successfully update password');
         }
         return response()->errors('sorry not update password successfully');
   }
   public function SaveStatus(Request $request)
   {
       $status = $request->status;
       $user = Auth::id();
       $validator = Validator::make($request->all() ,[
            'status' => 'required'
       ]);
       if($validator->fails())
       {
           return response()->errors($validator->errors()->first());
       }
       if(!in_array($status,$this->UserStatus))
       {
           return response()->errors('invalid status');
       }
       $checkstatus = UserMeta::where('user_id',$user)->where('meta_key','status')->first();
       if(!is_null($checkstatus))
       {
           $checkstatus->meta_value = $status;
           $checkstatus->save();
           return response()->success('update status successfully',$checkstatus);
       }
       else
       {
           UserMeta::insert([
           'user_id'    => $user ,
           'meta_key'   => 'status' ,
           'meta_value' => $status
           ]);
           return response()->success('save status successsfully');
       }
       return response()->errors('sorry not status save or update successsfully');
   }
    public function forgotPassword(Request $request){
        $data = $request->all();
        $getUser = User::where('user_email', $data['email'])->first();
        if (!$getUser) {
            return response()->json(['status' => '401',
                'data' => ['message' => 'Email Does Not Exist! Please Try Again!']], 401);
        }else
            {
                $user_id = $getUser->ID;
                $token = Str::random(15);
                $saveToken = new UserMeta([
                    'user_id' => $getUser->ID,
                    'meta_key' => 'password_reset',
                    'meta_value' => $token
                ]);
                if ($saveToken->save())
                {
                    $token_id = $saveToken->umeta_id;
                    Mail::to($getUser->user_email)->send(new ForgotPasswordMail($user_id,$token,$token_id));
                    return response()->json(['status' => '200', 'data' => ['message' => 'Password Reset Email has been sent Your Email!']],200);
                }
            }
    }
    public function PrivateStatus(Request $request)
    {
       $status = $request->status;
       $user = Auth::id();
       $validator = Validator::make($request->all() ,[
            'status' => 'required'
       ]);
       if($validator->fails())
       {
           return response()->errors($validator->errors()->first());
       }
       if(!in_array($status,$this->PrivateStatus))
       {
           return response()->errors('invalid status');
       }
       $checkstatus = UserMeta::where('user_id',$user)->where('meta_key','private_status')->first();
       if(!is_null($checkstatus))
       {
           $checkstatus->meta_value = $status;
           $checkstatus->save();
           return response()->success('update status successfully',$checkstatus);
       }
       else
       {
           UserMeta::insert([
           'user_id'    => $user ,
           'meta_key'   => 'private_status' ,
           'meta_value' => $status
           ]);
           return response()->success('save status successsfully');
       }
       return response()->errors('sorry not status save or update successsfully');        
    }
    public function CheckUsername(Request $request)
    {
        $CheckUsername = User::where('user_nicename',$request->username)->first();
        if($CheckUsername)
        {
            return response()->errors('Username Already Exists');
        }
        else
        {
            return response()->success('Username Not Exists');
        }
    }
    public function CheckEmail(Request $request)
    {
        $CheckEmail = User::where('user_email',$request->email)->first();
        if($CheckEmail)
        {
            return response()->errors('Email Already Exists');
        }
        else
        {
            return response()->success('Email Not Exists');
        }
    }
    public function NotificationByAdmin()
    {
        $User = Auth::id();
        $Notifications = UserNotification::select('title','message','created_at')->orderBy('id','DESC')->whereJsonContains('user_id',$User)->get();
        if($Notifications)
        {
            return response()->success('notifications',$Notifications);
        }
        else
        {
            return response()->errors('Sorry No Records Found');
        }
    }
//    public function forgot_password(Request $request){
//        $input = $request->only('user_email');
//        $validator = Validator::make($input, [
//            'user_email' => "required|email"
//        ]);
//        if ($validator->fails()) {
//            return response()->json($validator->errors());
//        }
//        $response = Password::sendResetLink($input);
//
//        $message = $response == Password::RESET_LINK_SENT ? 'Mail send successfully' : 'Not Sent Something Went Wrong! Try Again';
//
//        return response()->json($message);
//    }


}
