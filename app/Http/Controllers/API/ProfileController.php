<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\UserMeta;
use App\UserProfile;
use App\FriendRequest;
use App\CountryMobile;
use App\Country;
use App\CountryData;
use App\City;
use App\CityData;
use App\CityCategory;
use App\CityCategoryDetail;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\Null_;

class ProfileController extends Controller
{
    private $profileAttributes = [
        'firstname',
        'lastname',
        'email',
        'city',
        'country',
        'language',
        'nationality',
        'location',
        'phone_number'
    ];
    /**
     * Create User Profile
     *
     * @return \Illuminate\Http\Response
     */
    public function getProfile(Request $request) {
        $user = User::select(
            'ID',
            'user_email',
            'user_nicename',
            'display_name',
            'nationality',
            'language',
            'first_name',
            'last_name',
            'phone',
            // 'profile_picture',
            'device_token as device_token'
        )
        ->where(
            [
                'ID' => Auth::id()
            ]
        )
        ->first();
        // $user["cover_photo"] = (
        //     UserMeta::where(
        //         [
        //             'meta_key' => "cover_photo",
        //             'user_id' => Auth::id()
        //         ]
        //     )->first()
        // )->meta_value ?? null;
        $user["location"] = $user["address"] = (
            UserMeta::where(
                [
                    'meta_key' => "location",
                    'user_id' => Auth::id()
                ]
            )->first()
        )->meta_value ?? null;
        $user["miles_away"] = null;
        $user["city"] = (
            UserMeta::where(
                [
                    'meta_key' => "city",
                    'user_id' => Auth::id()
                ]
            )->first()
        )->meta_value ?? null;
        $user["country"] = (
            UserMeta::where(
                [
                    'meta_key' => "country",
                    'user_id' => Auth::id()
                ]
            )->first()
        )->meta_value ?? null;
        if (!empty($user)) {
           return response()->success('Successful',$user);
        } else {
            return response()->error('User Not Found');
        }
    }

    public function SenderProfile(Request $request) {
        $sender = $request->user_id;
        $userPending = FriendRequest::with('senderUser')
        ->where('status','=','pending')
        ->where('sender_id', $sender)
        ->where('recipient_id', Auth::id())
        ->get();
    foreach ($userPending as $key => &$user) {
        $user->user = $user->senderUser;
        unset($user->senderUser);
    }
    if (($userPending)) {
        return response()->success('Successfull',$userPending);
    } else {
        return response()->error('No Record Found');
    }
       
    }

    public function RecipientProfile(Request $request)
    {
        $recipient = $request->user_id;
        $users = User::with(['friendRequests' => function($query) use ($recipient) {
            $query->where('friend_requests.sender_id', '=',Auth::id() )
                  ->where('friend_requests.recipient_id', '=',$recipient)
                  ->where('friend_requests.status', '=', 'confirmed');
        }, 'friendRequests.user'])->where('ID', $recipient)->first();

        if (($users)) {
            return response()->success('Successfull',$users);
        } else {
            return response()->error('No Record Found');
        }
    }

    public function CheckUserProfile(Request $request) {
        $recipient = $request->user_id;
        $user = User::with(['senderStatus'=>function($query){
            $query->where('friend_requests.recipient_id',Auth::id())
                  ->where('friend_requests.status','confirmed');
        },'senderStatus.senderUser'])->where('ID',$recipient)->first();
        $check=[];
        foreach($user->senderStatus as $userr)
        {         
            $userr->user = $userr->senderUser;
            unset($userr->senderUser);
        }          
        
        $user1 = User::with(['recipientStatus'=>function($query){
            $query->where('friend_requests.sender_id',Auth::id())
                  ->where('friend_requests.status','confirmed');
        },'recipientStatus.user'])->where('ID',$recipient)->first();

        $user = $user->senderStatus ?? null;
        $user1 = $user1->recipientStatus ?? null;
        if (!is_null($user) && !is_null($user1)) {
            $merged = $user->merge($user1);
        }

        if($merged)
        {
            return response()->success('successfully',$merged);
        }
        else
        {
            return response()->errors('not successfully');
        }
        
       
    }

    public function updateDeviceToken(Request $request)
    {
        $LoginId = Auth::id();  
        $UpdateFcmToken = UserMeta::where(['user_id'=>$LoginId])
                                  ->where(['meta_key'=>'fcm_device_token'])
                                  ->update(['meta_value'=>$request->device_token]);
    }
    /**
     * Update User Profile
     *
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request){ 
        $validator = Validator::make($request->all(), [
            // 'firstname'  => 'required',
            // 'lastname'   => 'required',
            // 'email'     => 'required',
            // 'city'        => 'required',
            'country'    => 'required',
            'language'    => 'required',
            'nationality'    => 'required',
            // 'location'    => 'required',
            // 'phone_number'    => 'required'
        ]);
        if ($validator->fails()) {
            return response()->errors('All Feild Requried');
        }
        // if(!is_null($request->profile_picture))
        // {
        //     $LoginUser = Auth::user();
        //     $exists = Storage::disk('public')->exists('profile'.'-'.$LoginUser->ID.'.'.'png');
        //     if($exists == false)
        //     {
        //     $base64_string = $request->profile_picture; // your base64 encoded      
        //     $imageName = 'profile'.'-'.$LoginUser->ID.'.'.'png';   
        //     Storage::disk('public')->put($imageName, base64_decode($base64_string));
        //     }
        //     else
        //     {
        //         Storage::disk('public')->delete('profile'.'-'.$LoginUser->ID.'.'.'png');
        //         $base64_string = $request->profile_picture; // your base64 encoded      
        //         $imageName = 'profile'.'-'.$LoginUser->ID.'.'.'png';   
        //         Storage::disk('public')->put($imageName, base64_decode($base64_string));
        //     }
        // }
        // if(!is_null($request->cover_photo))
        // {
        //     $LoginUser = Auth::user();
        //     $exists = Storage::disk('public')->exists('cover'.'-'.$LoginUser->ID.'.'.'png');
        //     if($exists == false)
        //     {
        //     $base64_string = $request->cover_photo; // your base64 encoded      
        //     $imageName = 'cover'.'-'.$LoginUser->ID.'.'.'png';   
        //     Storage::disk('public')->put($imageName, base64_decode($base64_string));
        //     }
        //     else
        //     {
        //         Storage::disk('public')->delete('cover'.'-'.$LoginUser->ID.'.'.'png');
        //         $base64_string = $request->cover_photo; // your base64 encoded      
        //         $imageName = 'cover'.'-'.$LoginUser->ID.'.'.'png';   
        //         Storage::disk('public')->put($imageName, base64_decode($base64_string));   
        //     }
        // }
        User::where(
            [
                'ID'=>Auth::id()
            ]
        )->update(
            [
               /// 'profile_picture' => $request->profile_picture,
                'user_email' => $request->email,
                'first_name' => $request->firstname,
                'last_name' => $request->lastname,
                'language' => $request->language,
                'nationality' => $request->nationality,
                'phone' => $request->phone_number,
                'user_nicename' => $request->user_nicename,
                'display_name' => $request->display_name,
            ]
        );
        foreach ($request->except('_token', 'user_id', 'profile_picture','cover_photo', 'email', 'firstname', 'lastname' ,'language' ,'nationality', 'phone_number', 'user_nicename', 'display_name') as $key => $value) {
            if (is_null(UserMeta::where([ 'meta_key' => $key, 'user_id' => Auth::id() ])->first())) {
                $data = [
                    'user_id' => Auth::id(),
                    'meta_key' => $key,
                    'meta_value' => $value
                ];
                UserMeta::create($data);
            }
            else {
                $userMeta = UserMeta::where([ 'meta_key' => $key, 'user_id' => Auth::id() ])->first();
                $userMeta->meta_value = $value;
                $userMeta->save();
           }
        }
        $userMeta = $this->getUserProfile(Auth::id());
        if (!empty($userMeta)) {
           return response()->success('Successful',$userMeta);
        } else {
            return response()->error('Not create Profile');
        }
    }
    public function UpdateProfilePicture(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'profile_picture' => 'required'
        ]);
        if($validator->fails())
        {
            return response()->error($validator->errors()->first());
        }
        $LoginUser = Auth::user();
            $exists = Storage::disk('public')->exists('profile'.'-'.$LoginUser->ID.'.'.'png');
            if($exists == false)
            {
                $base64_string = $request->profile_picture; // your base64 encoded      
                $imageName = 'profile'.'-'.$LoginUser->ID.'.'.'png';   
                Storage::disk('public')->put($imageName, base64_decode($base64_string));
            }
            else
            {
                Storage::disk('public')->delete('profile'.'-'.$LoginUser->ID.'.'.'png');
                $base64_string = $request->profile_picture; // your base64 encoded      
                $imageName = 'profile'.'-'.$LoginUser->ID.'.'.'png';   
                Storage::disk('public')->put($imageName, base64_decode($base64_string));
            }

            User::where(
                [
                    'ID'=>Auth::id()
                ]
            )->update(
                [
                    'profile_picture' => $request->profile_picture
                ]);

                return response()->success('successfully update profile picture.');
    }
    public function UpdateCoverPhoto(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'cover_photo' => 'required'
        ]);
        if($validator->fails())
        {
            return response()->error($validator->errors()->first());
        }

        $LoginUser = Auth::user();
        $exists = Storage::disk('public')->exists('cover'.'-'.$LoginUser->ID.'.'.'png');
        if($exists == false)
        {
        $base64_string = $request->cover_photo; // your base64 encoded      
        $imageName = 'cover'.'-'.$LoginUser->ID.'.'.'png';   
        Storage::disk('public')->put($imageName, base64_decode($base64_string));
        }
        else
        {
            Storage::disk('public')->delete('cover'.'-'.$LoginUser->ID.'.'.'png');
            $base64_string = $request->cover_photo; // your base64 encoded      
            $imageName = 'cover'.'-'.$LoginUser->ID.'.'.'png';   
            Storage::disk('public')->put($imageName, base64_decode($base64_string));   
        }
            $check = UserMeta::where('user_id',Auth::id())->where('meta_key','cover_photo')->first();
            if(is_null($check))
            {
                UserMeta::create([
                    'user_id' => Auth::id(),
                    'meta_key' => 'cover_photo',
                    'meta_value'=> $request->cover_photo
                ]);
            }
            else
            {
                $check->meta_value = $request->cover_photo;
                $check->update();
            }

            return response()->success('successfully update cover photo.');
    }
    protected function getUserProfile($userId){
        $userMeta = UserMeta::where(['user_id' => $userId ])->whereIn('meta_key', $this->profileAttributes);
        $userProfile = ['ID' => $userId];
        foreach ($userMeta as &$users) {
            $userProfile[$users->meta_key] = $users->meta_value;
        }

        return $userProfile;
    }
    public function GetCountriesMobile()
    {
        $countries = CountryMobile::all();
        if($countries)
        {
            return response()->success('successfull',$countries);
        }
        else
        {
            return response()->errors('not successfull');
        }
    }
    public function GetStatusLockUnlock()
    {
        $user_id = Auth::id();
        $status = UserMeta::where('meta_key','status_lock_unlock')->where('user_id',$user_id)->first();
        if($status)
        {
            return response()->success('available status',$status->meta_value);
        }
        return response()->success('available status','unlocked');
    }
    public function CountryData(Request $request)
    {
        $CountryId = Country::where('name',$request->country_name)->pluck('id')->first();
        $CountryData = Country::with('countryData')->where('id',$CountryId)->first();
        if($CountryData)
        {
            return response()->success('sucessfully',$CountryData);
        }
        else
        {
            return response()->errors('sorry no available data for this country');
        }       
    }
    public function CityData(Request $request)
    {
        $CityId = City::where('name',$request->city_name)->pluck('id')->first();
        $CityData = City::with(
            [
                'cityData','cityCategoryDetails'
            ]
           // 'cityData','cityCategories','cityCategories.cityCategoryDetails'
        )->where('id',$CityId)->first();
        if($CityData)
        {
            return response()->success('sucessfully',$CityData);
        }
        else
        {
            return response()->errors('sorry no available data for this city');
        }
    }
}
