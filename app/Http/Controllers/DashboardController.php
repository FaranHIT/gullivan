<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Auth;
use Carbon\Carbon;
use File;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Illuminate\Validation\Rule;
use DB;
use App\Constant;
use App\User;
use App\UserMeta;
use URL;
use App\CountryMobile;
use App\FullCalendar;
use Yajra\DataTables\Facades\DataTables;

class DashboardController extends Controller
{
	public function index(Request $request)
	{
		$AndroidUsers=[];
		$IosUsers=[];
		$ChekLockUser = UserMeta::where('meta_key','status_lock_unlock')->where('meta_value','locked')->where('user_id',Auth::id())->first();
		if($ChekLockUser)
		{
			return Redirect::to("login")->withErrors(['error' => 'Opps! Your account have locked']);
		}
		else
		{
			$Users = User::all();
			$TotalUsers = count($Users);

			$DeviceType = UserMeta::where('meta_key','device_type')->where('meta_value','android')->orwhere('meta_value','ios')->get();
			foreach($DeviceType as $device)
			{
				if($device->meta_value == 'android')
				{
					$AndroidUsers[] = $device; 
				}
				else
				{
					$IosUsers[] = $device;
				}
			}
			$TotalAndroidUsers = count($AndroidUsers);
			$TotalIosUsers = count($IosUsers);
		return view('dashboard',compact('TotalUsers','TotalAndroidUsers','TotalIosUsers','Users'));
		}
	}

	public function users(Request $request)
	{
		if($request->ajax())
		{
			$users = User::query();
			return DataTables::of($users)->make(true);	
		}
		return view('users');
	}
	public function DashboardUsers(Request $request)
	{
		if($request->ajax())
		{
			$users = User::query();
			return DataTables::of($users)->make(true);
		}
	}

	public function checkCountryUsers($country_name)
	{
		$country_id = CountryMobile::where('country_name_en',$country_name)->pluck('id')->first();
		$checkUsers = UserMeta::where('meta_key','country')->where('meta_value',$country_id)->get();
		$TotalUsers = count($checkUsers);
		return response()->json($TotalUsers);
	}

	public function toDoList(Request $request)
	{
		if($request->ajax())
		{
			$toDoList = FullCalendar::query()->where('user_id',Auth::id());
			return DataTables::of($toDoList)->editColumn('start', function ($toDoList)
    {
        return date('M d, Y', strtotime($toDoList->start) );
    })->editColumn('end', function ($toDoList)
    {
        return date('M d, Y', strtotime($toDoList->end) );
    })->make(true);
		}
		
	}
}
