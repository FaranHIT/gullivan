<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Session;
use MikeMcLin\WpPassword\Facades\WpPassword;

use App\Constant;
use App\User;
use App\UserMeta;

class AuthController extends Controller
{
	public function login(Request $request)
	{
		if ($request->isMethod('post')) {
			request()->validate([
				'email' => 'required',
				'password' => 'required'
			]);
			$user = User::where(['user_email' => $request->input('email')])->first();
			if($user)
			{
			$ChekLockUser = UserMeta::where('meta_key','status_lock_unlock')->where('meta_value','locked')->where('user_id',$user->ID)->first();
			}
			else
			{
				return Redirect::to("login")->withErrors(['error' => 'Opps! You have entered invalid credentials']);
			}
			if($ChekLockUser)
			{
				return Redirect::to("login")->withErrors(['error' => 'Opps! Your account have locked']);
			}
			else
			{
			if($user && WpPassword::check($request->input('password'), $user->user_pass)) {
				Auth::login($user);
				return Redirect::to("dashboard");
			}
			return Redirect::to("login")->withErrors(['error' => 'Opps! You have entered invalid credentials']);
			}
		}
		return view('login');
	}

	public function logout() {
        Session::flush();
        Auth::logout();
        return Redirect('login');
    }
    public function resetPasswordForm($id,$token,$token_id)
    {
        $getToken = UserMeta::findOrFail($token_id);
        if ($getToken->meta_value != null)
        {
            $user_id = $id;
            return view('reset-password', compact('user_id','token_id'));
        }else
        {
            return view('expire-link');
        }
    }
    public function resetPassword(Request $request)
    {
        $validator       = Validator::make($request->all(), [
            'new_password'         => 'required|min:6',
            'confirm_password'       => 'required|same:new_password'
        ]);

        if ($validator->fails()) {
            return response()->errors($validator->errors()->first());
        }
        else{
            $user = User::findOrFail($request->user_id);
            $user->user_pass = Hash::make($request->input('new_password'));
            if ($user->save())
            {
                $updateToken = UserMeta::findOrFail($request->input('token_id'));
                $updateToken->meta_value = null;
                $updateToken->save();
                return view('reset-success');
            }else
            {
                return redirect()->back()->with('error','Something Went Wrong! Try Again');
            }

        }

    }
}
