<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\User;
use App\Role;
use App\Permission;
use App\RoleUser;
use App\PermissionUser;
use App\Country;
use App\UserMeta;
use Response;
use App\City;
use Yajra\DataTables\Facades\DataTables;
class RbacController extends Controller
{
	public function AssignRemoveRole(Request $request)
	{
	$LoginUser = Auth::user();
	if($LoginUser->hasRole('superadmin'))
	{
		if($request->ajax())
		{
			// $NoUserRole = [];
			// $SuperAdminAndAdmin = [];
			// $LockUsers = [];
			// $users = User::all();
			// foreach($users as $user)
			// {
			// 	if($user->hasRole(['superadmin','admin']))
			// 	{
			// 		$SuperAdminAndAdmin[] = $user; 
			// 	}
			// 	elseif($lock = UserMeta::where('meta_key','status_lock_unlock')->where('meta_value','locked')->where('user_id',$user->ID)->first())
			// 	{
			// 		$LockUsers[] = $user;
			// 	}
			// 	else
			// 	{
			// 	    $NoUserRole[] = $user;
			// 	}
			// }
		  	$NoUserRole = User::query()->doesntHave('roles')->orwhereRoleIs('manager');
			//$NoUserRole = User::query()->whereRoleIs('regular-user')->orwhereRoleIs('manager');
			// $NoUserRole = User::query()->with(['userMeta' => function($query){
			// 	$query->where('wp_usermeta.meta_key','status_lock_unlock')
			// 	->where('wp_usermeta.meta_value','!==','locked');
			// }])->doesntHave('roles')->orwhereRoleIs('manager')->get();
			//$RoleUser = RoleUser::where('role_id','2')->get();
			return DataTables::of($NoUserRole)
            ->addColumn('action',function($NoUserRole){
				// foreach($RoleUser as $r)
				// {
				// 	if($NoUserRole->ID == $r->user_id)
				if(is_null(UserMeta::where('meta_key','status_lock_unlock')->where('meta_value','locked')->where('user_id',$NoUserRole->ID)->first()))
				{
					if($NoUserRole->hasRole('manager'))
						{
							$button = '<a href="'.$NoUserRole->ID.'" onclick="RemoveManager(this,event)" class="float-left"><i class="fa fa-minus-circle" style="font-size:15px;color:red;" aria-hidden="true">&nbsp Remove Manager</i></a>';
							return $button;
						}						
				//} 
					else
					{
						$button = '<a href="'.$NoUserRole->ID. '" onclick="AssignManager(this,event)" class="float-left"><i class="fa fa-plus-circle" style="font-size:15px;color:black;" aria-hidden="true">&nbsp Assign Manager</i></a>';
						return $button;
					}
				}
				return 'User has been locked';	
                })
                ->rawColumns(['action'])
                ->make(true);
		}
		return view('roles.Assign-Remove-Role');
	}
	// elseif($LoginUser->hasRole('admin'))
	// {
	// 	if($request->ajax())
	// 	{
	// 		$NoUserRole = [];
	// 		$SuperAdminAndAdmin = [];
	// 		$LockUsers = [];
	// 		$ShowManager = [];
	// 		$NonShowManager = [];
	// 		$users = User::all();
	// 		foreach($users as $user)
	// 		{
	// 			if($user->hasRole(['superadmin','admin']))
	// 			{
	// 				$SuperAdminAndAdmin[] = $user; 
	// 			}
	// 			elseif($lock = UserMeta::where('meta_key','status_lock_unlock')->where('meta_value','locked')->where('user_id',$user->ID)->first())
	// 			{
	// 				$LockUsers[] = $user;
	// 			}
	// 			elseif($user->hasRole('manager'))
	// 			{
	// 				if(RoleUser::where(['role_id'=>'2','user_id'=>$user->ID])->where('created_by',Auth::id())->first())
	// 				{
	// 					$ShowManager[] = $user;
	// 				}
	// 				else
	// 				{
	// 					$NonShowManager[] = $user;
	// 				}
	// 			}
	// 			else
	// 			{					
	// 				$NoUserRole[] = $user;
	// 			}
	// 		}
	// 		$MergedUser = array_merge($ShowManager,$NoUserRole);

	// 		$RoleUser = RoleUser::where('role_id','2')->get();
			
	// 		return DataTables::of($MergedUser)
    //         ->addColumn('action',function($MergedUser) use($RoleUser){
	// 			foreach($RoleUser as $r)
	// 			{
	// 				if($MergedUser->ID == $r->user_id)
	// 				{
	// 					$button = '<a href="'.$r->user_id.'" onclick="RemoveManager(this,event)" class="float-left"><i class="fa fa-minus-circle" style="font-size:15px;color:red;" aria-hidden="true">&nbsp Remove Manager</i></a>';
	// 					return $button;
	// 				}						
	// 			}
	// 			$button = '<a href="'.$MergedUser->ID. '" onclick="AssignManager(this,event)" class="float-left"><i class="fa fa-plus-circle" style="font-size:15px;color:black;" aria-hidden="true">&nbsp Assign Manager</i></a>';
	// 			return $button;
    //             })
    //             ->rawColumns(['action'])
    //             ->make(true);
	// 	}
	// 	return view('roles.Assign-Remove-Role');
	// }

	elseif($LoginUser->hasRole('admin'))
	{
		if($request->ajax())
		{
			$MergedUser = User::query()->doesntHave('roles')->orWhereRoleIs('manager');
			return DataTables::of($MergedUser)
            ->addColumn('action',function($MergedUser){
				if(is_null(UserMeta::where('meta_key','status_lock_unlock')->where('meta_value','locked')->where('user_id',$MergedUser->ID)->first()))
				{	
					if($MergedUser->hasRole('manager'))
					{
						if(!is_null(RoleUser::where('user_id',$MergedUser->ID)->where('created_by',Auth::id())->first()))
						{
							$button = '<a href="'.$MergedUser->ID.'" onclick="RemoveManager(this,event)" class="float-left"><i class="fa fa-minus-circle" style="font-size:15px;color:red;" aria-hidden="true">&nbsp Remove Manager</i></a>';
							return $button;
						}
						return 'This Manager Make by other Admin.';
					}
					else
					{
						$button = '<a href="'.$MergedUser->ID. '" onclick="AssignManager(this,event)" class="float-left"><i class="fa fa-plus-circle" style="font-size:15px;color:black;" aria-hidden="true">&nbsp Assign Manager</i></a>';
						return $button;
					}
				}
				return 'User has been locked';						
                })
                ->rawColumns(['action'])
                ->make(true);
		}
		return view('roles.Assign-Remove-Role');
	}	


	}
	public function AssignRole($ID)
	{
		$LoginUser = Auth::user();
		if($LoginUser->hasRole(['admin','superadmin']))
		{
		$Roledata = Role::where('id', '2')->first();
		$users = User::where('ID',$ID)->first();
		$users->attachRole($Roledata);
		$ManagerRecord = RoleUser::where('role_id','2')->where('user_id',$ID)->update([
			'created_by' => Auth::id()
		]);
		return response()->json('successfully assign Manager role');
		}
		else
		{
			return response()->json('Sorry only Admin and SuperAdmin can assign Manager role');	
		}
	}
	public function RemoveRole($ID)
	{
		$LoginUser = Auth::user();
		if($LoginUser->hasRole(['admin','superadmin']))
		{
		$Roledata = Role::where('id', '2')->first();
		$users = User::where('ID',$ID)->first();
		$users->detachRole($Roledata);
		return response()->json('successfully remove Manager role');
		}
		else
		{
			return response()->json('Sorry only admin and superadmin can remove role');	
		}
	}
	public function AssignRemoveAdmin(Request $request)
	{
		if($request->ajax())
		{
			// $NoUserRole = [];
			// $SuperAdminRole = [];
			// $LockUsers = [];
			// $users = User::all();
			// foreach($users as $user)
			// {
			// 	if($user->hasRole('superadmin'))
			// 	{
			// 		$SuperAdminRole[] = $user; 
			// 	}
			// 	elseif($lock = UserMeta::where('meta_key','status_lock_unlock')->where('meta_value','locked')->where('user_id',$user->ID)->first())
			// 	{
			// 		$LockUsers[] = $user;
			// 	}
			// 	else
			// 	{
			// 	    $NoUserRole[] = $user;
			// 	}
			// }
			$RoleUser = RoleUser::where('role_id','1')->get();
			$NoUserRole = User::query()->WhereDoesntHave('roles')->orWhereRoleIs('admin')->orWhereRoleIs('manager');
			return DataTables::of($NoUserRole)
            ->addColumn('action',function($NoUserRole) use($RoleUser) {
			if(is_null(UserMeta::where('meta_key','status_lock_unlock')->where('meta_value','locked')->where('user_id',$NoUserRole->ID)->first()))
			{
				foreach($RoleUser as $r)
				{
					if($NoUserRole->ID == $r->user_id)
					{
						$button1 = '<a href="' .$r->user_id. '" onclick="RemoveAdmin(this,event)" class="float-left"><i class="fa fa-minus-circle" style="font-size:15px;color:red;" aria-hidden="true">&nbsp Remove Admin</i></a>';
						return $button1;
					}
				}
				$button1 = '<a href="' .$NoUserRole->ID. '" onclick="AssignAdmin(this,event)" class="float-left"><i class="fa fa-plus-circle" style="font-size:15px;color:black;" aria-hidden="true">&nbsp Assign Admin</i></a>';
				return $button1;
			}
			return 'User has been locked';

                })
                ->rawColumns(['action'])
                ->make(true);
		}
		return view('roles.Assign-Remove-Admin');
	}
	public function AssignAdmin($ID)
	{
		$LoginUser = Auth::user();
		if($LoginUser->hasRole('superadmin'))
		{
		$Roledata = Role::where('id', '1')->first();
		$users = User::where('ID',$ID)->first();
		$users->attachRole($Roledata);
		return response()->json('successfully assign Admin role');
		}
		else
		{
			return response()->json('sorry only SuperAdmin can assign Admin role');	
		}
	}
	public function RemoveAdmin($ID)
	{
		$LoginUser = Auth::user();
		if($LoginUser->hasRole('superadmin'))
		{
		$Roledata = Role::where('id', '1')->first();
		$users = User::where('ID',$ID)->first();
		$users->detachRole($Roledata);
		$ManagerRecord = RoleUser::where('role_id','2')->where('created_by',$ID)->update([
			'created_by' => '0'
		]);
		return response()->json('successfully remove Admin role');
		}
		else
		{
			return response()->json('sorry only SuperAdmin can remove Admin role');	
		}
	}
	public function AssignRemovePermissions(Request $request)
	{
	$LoginUser = Auth::user();
	if($LoginUser->hasRole('superadmin'))
	{	
		if($request->ajax())
		{
			if(!empty($request->country))
			{
			//$users=[];	
			//$managers=[];
			$CountryPermission=[];
			$CityPermission=[];
			$FilterCountry = $request->country;
			// $PermissionId = Permission::where('name',$FilterCountry)->first();
			// $PermissionUserId = PermissionUser::where('permission_id',$PermissionId->id)->get();
			// foreach($PermissionUserId as $PId)
			// { 
			// 	$users[] = User::where('ID',$PId->user_id)->first();
			// }	
			
			// foreach($users as $user)
			// {
			// 	if($user->hasRole('manager'))
			// 	{
			// 		$managers[]=$user;
			// 	}
				
			// }
			$managers = User::query()->whereRoleIs('manager')->wherePermissionIs($FilterCountry);				
			return DataTables::of($managers)
            ->addColumn('action',function($managers)  {

						$button = '<a href="'.route('assign-country-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-info btn-sm float-left mx-2">Country Permission</a>';
						$button .= '<a href="'.route('assign-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-info btn-sm float-left mx-2">City Permission</a>';
						$button .= '<a href="'.route('remove-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-danger btn-sm float-left mx-2">Remove Permissions</a>';
						return $button;		
				})
			->addColumn('CountryPermissions',function($managers) use($CountryPermission,$FilterCountry){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Countries = Country::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->name;
					foreach($Countries as $country)
					{ 
						$CountryName = $country->name ;
						if($PermissionName == $CountryName)
						{
							$CountryPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CountryPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CountryPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
			->addColumn('CityPermissions',function($managers) use($CityPermission){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Cities = City::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->display_name;
					foreach($Cities as $city)
					{ 
						$CityName = $city->name ;
						if($PermissionName == $CityName)
						{
							$CityPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CityPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CityPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
				
                ->rawColumns(['CountryPermissions','CityPermissions','action'])
                ->make(true);
			}
			
			elseif(!empty($request->city))
			{
			//$users=[];	
			//$managers=[];
			$CountryPermission=[];
			$CityPermission=[];
			$FilterCity = $request->city;
			// $PermissionId = Permission::where('name',$FilterCity)->first();
			// $PermissionUserId = PermissionUser::where('permission_id',$PermissionId->id)->get();
			// foreach($PermissionUserId as $PId)
			// { 
			// 	$users[] = User::where('ID',$PId->user_id)->first();
			// }	
			
			// foreach($users as $user)
			// {
			// 	if($user->hasRole('manager'))
			// 	{
			// 		$managers[]=$user;
			// 	}
				
			// }	
			$managers = User::query()->whereRoleIs('manager')->wherePermissionIs($FilterCity);			
			return DataTables::of($managers)
            ->addColumn('action',function($managers)  {

						$button = '<a href="'.route('assign-country-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-info btn-sm float-left mx-2">Country Permission</a>';
						$button .= '<a href="'.route('assign-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-info btn-sm float-left mx-2">City Permission</a>';
						$button .= '<a href="'.route('remove-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-danger btn-sm float-left mx-2">Remove Permissions</a>';
						return $button;		
				})
			->addColumn('CountryPermissions',function($managers) use($CountryPermission,$FilterCountry){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Countries = Country::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->name;
					foreach($Countries as $country)
					{ 
						$CountryName = $country->name ;
						if($PermissionName == $CountryName)
						{
							$CountryPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CountryPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CountryPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
			->addColumn('CityPermissions',function($managers) use($CityPermission){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Cities = City::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->display_name;
					foreach($Cities as $city)
					{ 
						$CityName = $city->name ;
						if($PermissionName == $CityName)
						{
							$CityPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CityPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CityPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
				
                ->rawColumns(['CountryPermissions','CityPermissions','action'])
                ->make(true);
			}

			else
			{
			// $managers=[];
			$CountryPermission=[];
			$CityPermission=[];
			// $users = User::all();
			// foreach($users as $user)
			// {
			// 	if($user->hasRole('manager'))
			// 	{
			// 		$managers[]=$user;
			// 	}	
			// }
			$managers = User::query()->whereRoleIs('manager');				
			return DataTables::of($managers)
            ->addColumn('action',function($managers)  {

						$button = '<a href="'.route('assign-country-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-info btn-sm float-left mx-2">Country Permission</a>';
						$button .= '<a href="'.route('assign-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-info btn-sm float-left mx-2">City Permission</a>';
						$button .= '<a href="'.route('remove-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-danger btn-sm float-left mx-2">Remove Permissions</a>';
						return $button;		
				})
			->addColumn('CountryPermissions',function($managers) use($CountryPermission){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Countries = Country::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->name;
					foreach($Countries as $country)
					{ 
						$CountryName = $country->name ;
						if($PermissionName == $CountryName)
						{
							$CountryPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CountryPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CountryPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
			->addColumn('CityPermissions',function($managers) use($CityPermission){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Cities = City::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->display_name;
					foreach($Cities as $city)
					{ 
						$CityName = $city->name ;
						if($PermissionName == $CityName)
						{
							$CityPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CityPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CityPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
				
                ->rawColumns(['CountryPermissions','CityPermissions','action'])
                ->make(true);
			}
		}
		$countries = Country::all();
		$cities = City::all();
		return view('permissions.Assign-Remove-Permissions',compact('countries','cities'));
	}
	elseif($LoginUser->hasRole('admin'))
	{
		if($request->ajax())
		{
			if(!empty($request->country))
			{
			//$users=[];	
			//$managers=[];
			$CountryPermission=[];
			$CityPermission=[];
			$FilterCountry = $request->country;
			// $PermissionId = Permission::where('name',$FilterCountry)->first();
			// $PermissionUserId = PermissionUser::where('permission_id',$PermissionId->id)->get();
			// foreach($PermissionUserId as $PId)
			// { 
			// 	$users[] = User::where('ID',$PId->user_id)->first();
			// }	
			
			// foreach($users as $user)
			// {
			// 	if(RoleUser::where(['role_id'=>'2','user_id'=>$user->ID])->where('created_by',Auth::id())->first())
			// 	{
			// 		$managers[]=$user;
			// 	}
				
			// }
			$managers = User::query()->whereHas('roles',function($query){
				$query->where('name','=','manager')->where('created_by',Auth::id());
			})->wherePermissionIs($FilterCountry);				
			return DataTables::of($managers)
            ->addColumn('action',function($managers)  {

						$button = '<a href="'.route('assign-country-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-info btn-sm float-left mx-2">Country Permission</a>';
						$button .= '<a href="'.route('assign-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-info btn-sm float-left mx-2">City Permission</a>';
						$button .= '<a href="'.route('remove-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-danger btn-sm float-left mx-2">Remove Permissions</a>';
						return $button;		
				})
			->addColumn('CountryPermissions',function($managers) use($CountryPermission,$FilterCountry){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Countries = Country::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->name;
					foreach($Countries as $country)
					{ 
						$CountryName = $country->name ;
						if($PermissionName == $CountryName)
						{
							$CountryPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CountryPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CountryPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
			->addColumn('CityPermissions',function($managers) use($CityPermission){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Cities = City::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->display_name;
					foreach($Cities as $city)
					{ 
						$CityName = $city->name ;
						if($PermissionName == $CityName)
						{
							$CityPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CityPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CityPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
				
                ->rawColumns(['CountryPermissions','CityPermissions','action'])
                ->make(true);
			}
			
			elseif(!empty($request->city))
			{
			//$users=[];	
			//$managers=[];
			$CountryPermission=[];
			$CityPermission=[];
			$FilterCity = $request->city;
			// $PermissionId = Permission::where('name',$FilterCity)->first();
			// $PermissionUserId = PermissionUser::where('permission_id',$PermissionId->id)->get();
			// foreach($PermissionUserId as $PId)
			// { 
			// 	$users[] = User::where('ID',$PId->user_id)->first();
			// }	
			
			// foreach($users as $user)
			// {
			// 	if(RoleUser::where(['role_id'=>'2','user_id'=>$user->ID])->where('created_by',Auth::id())->first())
			// 	{
			// 		$managers[]=$user;
			// 	}
				
			// }
			$managers = User::query()->whereHas('roles',function($query){
				$query->where('name','=','manager')->where('created_by',Auth::id());
			})->wherePermissionIs($FilterCity);				
			return DataTables::of($managers)
            ->addColumn('action',function($managers)  {

						$button = '<a href="'.route('assign-country-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-info btn-sm float-left mx-2">Country Permission</a>';
						$button .= '<a href="'.route('assign-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-info btn-sm float-left mx-2">City Permission</a>';
						$button .= '<a href="'.route('remove-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-danger btn-sm float-left mx-2">Remove Permissions</a>';
						return $button;		
				})
			->addColumn('CountryPermissions',function($managers) use($CountryPermission,$FilterCountry){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Countries = Country::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->name;
					foreach($Countries as $country)
					{ 
						$CountryName = $country->name ;
						if($PermissionName == $CountryName)
						{
							$CountryPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CountryPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CountryPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
			->addColumn('CityPermissions',function($managers) use($CityPermission){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Cities = City::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->display_name;
					foreach($Cities as $city)
					{ 
						$CityName = $city->name ;
						if($PermissionName == $CityName)
						{
							$CityPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CityPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CityPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
				
                ->rawColumns(['CountryPermissions','CityPermissions','action'])
                ->make(true);
			}

			else
			{
			//$managers=[];
			$CountryPermission=[];
			$CityPermission=[];
			// $users = User::all();
			// foreach($users as $user)
			// {
			// 	if(RoleUser::where(['role_id'=>'2','user_id'=>$user->ID])->where('created_by',Auth::id())->first())
			// 	{
			// 		$managers[]=$user;
			// 	}	
			// }
			$managers = User::query()->whereHas('roles',function($query){
				$query->where('name','=','manager')->where('created_by',Auth::id());
			});				
			return DataTables::of($managers)
            ->addColumn('action',function($managers)  {

						$button = '<a href="'.route('assign-country-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-info btn-sm float-left mx-2">Country Permission</a>';
						$button .= '<a href="'.route('assign-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-info btn-sm float-left mx-2">City Permission</a>';
						$button .= '<a href="'.route('remove-permissions-form',$managers->ID).'" type="button" name="" class="btn btn-danger btn-sm float-left mx-2">Remove Permissions</a>';
						return $button;		
				})
			->addColumn('CountryPermissions',function($managers) use($CountryPermission){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Countries = Country::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->name;
					foreach($Countries as $country)
					{ 
						$CountryName = $country->name ;
						if($PermissionName == $CountryName)
						{
							$CountryPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CountryPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CountryPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
			->addColumn('CityPermissions',function($managers) use($CityPermission){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Cities = City::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->display_name;
					foreach($Cities as $city)
					{ 
						$CityName = $city->name ;
						if($PermissionName == $CityName)
						{
							$CityPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CityPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CityPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
				
                ->rawColumns(['CountryPermissions','CityPermissions','action'])
                ->make(true);
			}
		}
		$countries = Country::all();
		$cities = City::all();
		return view('permissions.Assign-Remove-Permissions',compact('countries','cities'));
	}
	}
	public function SelectCountriesToCities($id)
	{	
	  	$cities = City::where('country_id', $id)->get();
	  	return response()->json($cities);	
	}
	public function AssignPermissionsForm($ID)
	{
		$id = $ID;
		$permissions = Country::all();
		return view('permissions.Assign-Permissions-Form',compact('permissions','id'));
	}
	public function PostAssignPermissionsForm(Request $request)
	{
		$UnAssignedPermission=[];
		request()->validate([
            'Country-Permission' => 'required' ,
            'City-Permission'=> 'required'
        ]);
		$LoginUser = Auth::user();

		$userid = $request->input('user_id');
		$user = User::where('ID',$userid)->first();
			
		$CityName = $request->input('City-Permission');

		foreach($CityName as $city)
		{
			$CityPermission = Permission::where('name',$city)->first();
			$CheckPermission = PermissionUser::where('permission_id',$CityPermission->id)->where('user_id',$userid)->first();
			if(is_null($CheckPermission))
			{
				$UnAssignedPermission[] = $CityPermission;
				if($LoginUser->hasRole(['admin','superadmin']))
				{
					if($user->hasRole('manager'))
					{
						$user->attachPermission($CityPermission);
					}
					else
					{
					return redirect('assign-remove-permissions')->with('error-msg','sorry plz assign manager role firstly');
					}
				}
				else
				{
				return redirect('assign-remove-permissions')->with('error-msg','only Super Admin & Admin can assign permission');
				}
			}	
		}
		if(empty($UnAssignedPermission))
		{
			return redirect('assign-remove-permissions')->with('error-msg','permission have already assigned');
		}
		else
		{
			return redirect('assign-remove-permissions')->with('success-msg','Successfully Assigned Permission');
		}					  
	}
	public function AssignCountryPermissionsForm($ID)
	{
		$id = $ID;
		$permissions = Country::all();
		return view('permissions.Assign-Country-Permissions-Form',compact('permissions','id'));
	}
	public function PostAssignCountryPermissionsForm(Request $request)
	{
		$UnAssignedPermission=[];
		$LoginUser = Auth::user();

		$userid = $request->input('user_id');
		$user = User::where('ID',$userid)->first();

		$PermissionName = $request->input('permission');
		foreach($PermissionName as $PName)
		{
			$Permission = Permission::where('name',$PName)->first();
			$CheckPermission = PermissionUser::where('permission_id',$Permission->id)->where('user_id',$userid)->first();
			if(is_null($CheckPermission))
			{
				$UnAssignedPermission[] = $Permission;
				if($LoginUser->hasRole(['admin','superadmin']))
				{
					if($user->hasRole('manager'))
					{
						$user->attachPermission($Permission);
					}
					else
					{
						return redirect('assign-remove-permissions')->with('error-msg','sorry plz assign manager role firstly');
					}
				}
				else
				{
					return redirect('assign-remove-permissions')->with('error-msg','only Super Admin $ Admin can assign permission');
				}
			}
		}
		if(empty($UnAssignedPermission))
		{
			return redirect('assign-remove-permissions')->with('error-msg','permission have already assigned');
		}
		else
		{
			return redirect('assign-remove-permissions')->with('success-msg','Successfully Assigned Permission');
		}
	}
	public function RemovePermissionsForm($ID)
	{
		$permissions = [];
		$id = $ID;
		$PermissionsId = PermissionUser::where('user_id',$id)->get();
		foreach($PermissionsId as $pid)
		{
			$permissions[] = Permission::where('id',$pid->permission_id)->first();
		}
		return view('permissions.Remove-Permissions-Form',compact('permissions','id'));
	}
	public function PostRemovePermissionsForm(Request $request)
	{
		$LoginUser = Auth::user();
		$PermissionName = $request->input('permission');
		foreach($PermissionName as $PName)
		{
		$Permission[] = Permission::where('name',$PName)->first();
		}
		$userid = $request->input('user_id');
		$user = User::where('ID',$userid)->first();
		
		if($LoginUser->hasRole(['admin','superadmin']))
			{
				if($user->hasRole('manager'))
				{
					foreach($Permission as $permsson)
					{
						$user->detachPermission($permsson);
					}
				    return redirect('assign-remove-permissions')->with('success-msg','successfully remove permission');
				}
				return redirect('assign-remove-permissions')->with('error-msg','sorry plz assign manager role firstly');
			}
			else
			{
				return redirect('assign-remove-permissions')->with('error-msg','only Super Admin $ Admin can remove permission');
			}
	}
	public function ViewCountryPermissions($ID)
	{ 
		$CountryPermission=[];
		$UserPermissions = PermissionUser::where('user_id',$ID)->get();
		$Countries = Country::all();
		foreach($UserPermissions as $permission)
		{
			$permissions = Permission::where('id',$permission->permission_id)->first();
			$PermissionName = $permissions->name;
			foreach($Countries as $country)
			{ 
				$CountryName = $country->name ;
				if($PermissionName == $CountryName)
				{
					$CountryPermission[] = $PermissionName.','.' ';
				}
			}
		}
		return response()->json($CountryPermission);
	}
	public function ViewCityPermissions($ID)
	{ 
		$CityPermission=[];
		$UserPermissions = PermissionUser::where('user_id',$ID)->get();
		$Cities = City::all();
		foreach($UserPermissions as $permission)
		{
			$permissions = Permission::where('id',$permission->permission_id)->first();
			$PermissionName = $permissions->display_name;
			foreach($Cities as $city)
			{ 
				$CityName = $city->name ;
				if($PermissionName == $CityName)
				{
					$CityPermission[] = $PermissionName.','.' ';
				}
			}
		}
		return response()->json($CityPermission);
	}
	public function ViewAdmin(Request $request)
	{
		// if($request->ajax())
		// {
		// 	$Admin=[];
		// 	$LoginUser = Auth::user();
		// 	if($LoginUser->hasRole('superadmin'))
		// 	{
		// 		$users = User::all();
		// 		foreach($users as $user)
		// 		{
		// 			if($user->hasRole('admin'))
		// 			{
		// 				$Admin[] = $user;
		// 			}
		// 		}
		// 		return DataTables::of($Admin)->make(true);
		// 	}
		if($request->ajax())
		{
			if(Auth::user()->hasRole('superadmin'))
			{
				$users = User::query()->whereRoleIs('admin');
				return DataTables::of($users)->make(true);
			}
			else
			{
				return redirect()->back()->with('error-msg','SuperAdmin only view Admin');
			}
		}
		return view('roles.View-Admin');
	}
	// public function ViewManager(Request $request)
	// {
	// 	if($request->ajax())
	// 	{
	// 		$Manager=[];
	// 		$users=[];
	// 		$LoginUser = Auth::user();
	// 		if($LoginUser->hasRole('superadmin'))
	// 		{
	// 			$users = User::all();
	// 			foreach($users as $user)
	// 			{
	// 				if($user->hasRole('manager'))
	// 				{
	// 					$Manager[] = $user;
	// 				}
	// 			}
	// 			return DataTables::of($Manager)
	// 			->addColumn('CreatedBy', function($Manager){
	// 				$Createdby = RoleUser::where('user_id',$Manager->ID)->first();
	// 				$UserCreatedBy = User::where('ID',$Createdby->created_by)->first();
	// 				return $UserCreatedBy->user_nicename ?? null;
	// 			})
	// 			->rawColumns(['CreatedBy'])
	// 			->make(true);
	// 		}
	// 		elseif($LoginUser->hasRole('admin'))
	// 		{
	// 			$UserCreatedManagerId = RoleUser::where('role_id','2')->where('created_by',Auth::id())->get();
	// 			foreach($UserCreatedManagerId as $id)
	// 			{
	// 				$users[] = User::where('ID',$id->user_id)->first();
	// 			}
	// 			foreach($users as $user)
	// 			{
	// 				$Manager[] = $user;
	// 			}
	// 			return DataTables::of($Manager)
	// 			->addColumn('CreatedBy', function($Manager){
	// 				$Createdby = RoleUser::where('user_id',$Manager->ID)->first();
	// 				$UserCreatedBy = User::where('ID',$Createdby->created_by)->first();
	// 				return $UserCreatedBy->user_nicename ?? null;
	// 			})
	// 			->rawColumns(['CreatedBy'])
	// 			->make(true);
	// 		}
	// 		else
	// 		{
	// 			return redirect()->back()->with('error-msg','SuperAdmin & Admin only view Manager');
	// 		}
	// 	}
	// 	return view('roles.View-Manager');
	// }
	public function ViewManager(Request $request)
	{
		if($request->ajax())
		{
			// $Manager=[];
	 		// $users=[];
			if(Auth::user()->hasRole('superadmin'))
			{
				$Manager = User::query()->whereRoleIs('manager');
				return DataTables::of($Manager)
				->addColumn('CreatedBy', function($Manager){
					$Createdby = RoleUser::where('user_id',$Manager->ID)->first();
					$UserCreatedBy = User::where('ID',$Createdby->created_by)->first();
					return $UserCreatedBy->user_nicename ?? null;
				})
				->rawColumns(['CreatedBy'])
				->make(true);
			}
			elseif(Auth::user()->hasRole('admin'))
			{
				// $UserCreatedManagerId = RoleUser::where('role_id','2')->where('created_by',Auth::id())->get();
				// foreach($UserCreatedManagerId as $id)
				// {
				// 	$users[] = User::where('ID',$id->user_id)->first();
				// }
				// foreach($users as $user)
				// {
				// 	$Manager[] = $user;
				// }
				$Manager = User::query()->whereHas('roles',function($query){
					$query->where('name','=','manager')->where('created_by',Auth::id());
				});	
				return DataTables::of($Manager)
				->addColumn('CreatedBy', function($Manager){
					$Createdby = RoleUser::where('user_id',$Manager->ID)->first();
					$UserCreatedBy = User::where('ID',$Createdby->created_by)->first();
					return $UserCreatedBy->user_nicename ?? null;
				})
				->rawColumns(['CreatedBy'])
				->make(true);
			}
			else
			{
				return redirect()->back()->with('error-msg','SuperAdmin & Admin only view Manager');
			}
		}
		return view('roles.View-Manager');
	}
	public function UnallocatedManager(Request $request)
	{
		if($request->ajax())
		{
			if(!empty($request->country))
			{
			//$users=[];	
			//$managers=[];
			$CountryPermission=[];
			$CityPermission=[];
			$FilterCountry = $request->country;
			// $PermissionId = Permission::where('name',$FilterCountry)->first();
			// $PermissionUserId = PermissionUser::where('permission_id',$PermissionId->id)->get();
			// foreach($PermissionUserId as $PId)
			// { 
			// 	$users[] = User::where('ID',$PId->user_id)->first();
			// }	
			
			// foreach($users as $user)
			// {
			// 	if($user->hasRole('manager'))
			// 	{
			// 		if(RoleUser::where('user_id',$user->ID)->where('created_by','0')->first())
			// 		{
			// 			$managers[]=$user;
			// 		}
			// 	}
				
			// }
			$managers = User::query()->whereHas('roles',function($query){
				$query->where('name','=','manager')->where('created_by',0);
			})->wherePermissionIs($FilterCountry);				
			return DataTables::of($managers)
            ->addColumn('action',function($managers)  {

				$button = '<a href="'.$managers->ID.'" onclick="AdminModal(this,event)" data-toggle="modal" data-target=".bd-example-modal-l" type="button" name="" class="btn btn-info btn-sm float-left">Choose to allocate Admin</a>';						
				return $button;		
				})
			->addColumn('CountryPermissions',function($managers) use($CountryPermission,$FilterCountry){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Countries = Country::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->name;
					foreach($Countries as $country)
					{ 
						$CountryName = $country->name ;
						if($PermissionName == $CountryName)
						{
							$CountryPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CountryPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CountryPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
			->addColumn('CityPermissions',function($managers) use($CityPermission){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Cities = City::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->display_name;
					foreach($Cities as $city)
					{ 
						$CityName = $city->name ;
						if($PermissionName == $CityName)
						{
							$CityPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CityPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CityPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
				
                ->rawColumns(['CountryPermissions','CityPermissions','action'])
                ->make(true);
			}
			
			elseif(!empty($request->city))
			{
			// $users=[];	
			// $managers=[];
			$CountryPermission=[];
			$CityPermission=[];
			$FilterCity = $request->city;
			// $PermissionId = Permission::where('name',$FilterCity)->first();
			// $PermissionUserId = PermissionUser::where('permission_id',$PermissionId->id)->get();
			// foreach($PermissionUserId as $PId)
			// { 
			// 	$users[] = User::where('ID',$PId->user_id)->first();
			// }	
			
			// foreach($users as $user)
			// {
			// 	if($user->hasRole('manager'))
			// 	{
			// 		if(RoleUser::where('user_id',$user->ID)->where('created_by','0')->first())
			// 		{
			// 		$managers[]=$user;
			// 		}
			// 	}
				
			// }				
			$managers = User::query()->whereHas('roles',function($query){
				$query->where('name','=','manager')->where('created_by',0);
			})->wherePermissionIs($FilterCity);
			return DataTables::of($managers)
            ->addColumn('action',function($managers)  {

				$button = '<a a href="'.$managers->ID.'" onclick="AdminModal(this,event)" data-toggle="modal" data-target=".bd-example-modal-l" type="button" name="" class="btn btn-info btn-sm float-left">Choose to allocate Admin</a>';						
				return $button;		
				})
			->addColumn('CountryPermissions',function($managers) use($CountryPermission,$FilterCountry){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Countries = Country::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->name;
					foreach($Countries as $country)
					{ 
						$CountryName = $country->name ;
						if($PermissionName == $CountryName)
						{
							$CountryPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CountryPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CountryPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
			->addColumn('CityPermissions',function($managers) use($CityPermission){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Cities = City::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->display_name;
					foreach($Cities as $city)
					{ 
						$CityName = $city->name ;
						if($PermissionName == $CityName)
						{
							$CityPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CityPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CityPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
				
                ->rawColumns(['CountryPermissions','CityPermissions','action'])
                ->make(true);
			}

			else
			{
			//$managers=[];
			$CountryPermission=[];
			$CityPermission=[];
			// $users = User::whereRoleIs('manager')->get();
			// foreach($users as $user)
			// {
			// 	if(RoleUser::where('user_id',$user->ID)->where('created_by','0')->first())
			// 	{
			// 	$managers[]=$user;
			// 	}	
			// }
			$managers = User::query()->whereHas('roles',function($query){
				$query->where('name','=','manager')->where('created_by',0);
			});				
			return DataTables::of($managers)
            ->addColumn('action',function($managers)  {

			$button = '<a a href="'.$managers->ID.'" onclick="AdminModal(this,event)" data-toggle="modal" data-target=".bd-example-modal-l" type="button" name="" class="btn btn-info btn-sm float-left">Choose to allocate Admin</a>';						
			return $button;		
				})
			->addColumn('CountryPermissions',function($managers) use($CountryPermission){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Countries = Country::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->name;
					foreach($Countries as $country)
					{ 
						$CountryName = $country->name ;
						if($PermissionName == $CountryName)
						{
							$CountryPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CountryPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CountryPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
			->addColumn('CityPermissions',function($managers) use($CityPermission){
				$UserPermissions = PermissionUser::where('user_id',$managers->ID)->get();
				$Cities = City::all();
				foreach($UserPermissions as $permission)
				{
					$permissions = Permission::where('id',$permission->permission_id)->first();
					$PermissionName = $permissions->display_name;
					foreach($Cities as $city)
					{ 
						$CityName = $city->name ;
						if($PermissionName == $CityName)
						{
							$CityPermission[] = $PermissionName;
						}
					}
				}
				$Collection = collect($CityPermission);
				$OnePermission = $Collection->slice(0,1);
				if(count($Collection)<1)
				{
					return '-';
				}
				if(count($Collection)<2)
				{
					return $OnePermission;
				}
				$button1 = '<a href="'.$managers->ID.'" onclick="CityPermissonsClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
				return $OnePermission.' '.$button1;
			})
				
                ->rawColumns(['CountryPermissions','CityPermissions','action'])
                ->make(true);
			}
		}
		$countries = Country::all();
		$cities = City::all();
		return view('roles.Unallocated-Managers',compact('countries','cities'));
	}
	public function Admins()
	{
		$admins = User::whereRoleIs('admin')->get();
		return response()->json($admins);
	}
	public function AllocateAdmin($managerID,$adminID)
	{
			RoleUser::where('user_id',$managerID)->where('role_id','2')->update([
				'created_by' => $adminID
			]);
			return response()->json('successfully allocate Admin');
	}		
}
