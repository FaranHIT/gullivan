<?php

namespace App\Http\Controllers;

use App\FullCalendar;
use Illuminate\Http\Request;
use Redirect,Response;
use Auth;

class FullCalendarController extends Controller
{
    public function index()
    {
        if(request()->ajax()) 
        {
 
         $start = (!empty($_GET["start"])) ? ($_GET["start"]) : ('');
         $end = (!empty($_GET["end"])) ? ($_GET["end"]) : ('');
 
         $data = FullCalendar::where('user_id',Auth::id())->whereDate('start', '>=', $start)->whereDate('end',   '<=', $end)->get(['id','title','start', 'end']);
        //  return Response::json($data);
        return Response::json($data);
        }
        return view('fullcalendar');
    }
    public function create(Request $request)
    {  
        $insertArr = [ 'title' => $request->title,
                       'start' => $request->start,
                       'end' => $request->end ,
                       'user_id' => Auth::id()
                    ];
        $event = FullCalendar::create($insertArr);   
        return Response::json($event);
    }
    public function update(Request $request)
    {   
        $where = array('id' => $request->id);
        $updateArr = ['title' => $request->title,'start' => $request->start, 'end' => $request->end];
        $event  = FullCalendar::where($where)->update($updateArr);
 
        return Response::json($event);
    } 
    public function destroy(Request $request)
    {
        $event = FullCalendar::where('id',$request->id)->delete();
   
        return Response::json($event);
    }    
 
}
