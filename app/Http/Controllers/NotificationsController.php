<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\CountryMobile;
use App\UserMeta;
use App\UserNotification;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;
use Yajra\DataTables\Facades\DataTables;

class NotificationsController extends Controller
{
    public function Notifications()
    {
       $countries = CountryMobile::all();
       $nationalities = CountryMobile::all();
       $languages = ['English','Chinese','Arabic','Russian'];
       $users = User::all();

       return view('Notifications.notifications',compact('countries','nationalities','languages','users'));
    }
    public function SendNotifications(Request $request)
    {
        $countries     = $request->input('countries');
        $nationalities = $request->input('nationalities');
        $Languages     = $request->input('languages');
        $title         = $request->input('title');
        $message       = $request->input('message');
        $Users         = $request->input('users');

        request()->validate([
            'title' => 'required' ,
            'message'=> 'required'
        ]);
        
      if(!empty($countries))
      {
        foreach($countries as $country)
        {
          $Countries = CountryMobile::where('id',$country)->first();
          $TotalCountries[] = $Countries->country_name_en;
        }
      }
      if(!empty($nationalities))
      { 
        foreach($nationalities as $nationality)
        {
          $Nationalities = CountryMobile::where('id',$nationality)->first();
          $TotalNationalities[] = $Nationalities->country_name_en;
        }
      }
      if(!empty($Languages))
      {
        foreach($Languages as $languages)
        {
          $TotalLanguages[] = $languages;
        }
      } 

  if(is_null($countries) && is_null($nationalities) && is_null($Languages) && is_null($Users))
  {
            $users = User::all();
            foreach($users as $user)
            {
               $fcm_token = UserMeta::where('meta_key','fcm_device_token')->where('user_id',$user->ID)->first();
               if($fcm_token)
              {
                $IdUsers[] = $user->ID;
                $NameUsers = $user->user_nicename;
                app()->make('FcmService')->sendNotification($fcm_token->meta_value, $title,'HI'.' '.$NameUsers.'!'.' '. $message,["data" => ['identifier' => 'AP']]);
              }
            }
            
            $AllUsers[] = 'All Users';
            UserNotification::create([
              'user_id'=>$IdUsers ,
              'title'  =>$title ,
              'message'=>$message,
              'countries'=> '-',
              'nationalities'=>'-',
              'languages'=>'-',
              'users'=>$AllUsers
              ]);
            return redirect()->back()->with('success-msg','send notifications successfully');
  }
  
  elseif(!is_null($countries) && !is_null($nationalities) && !is_null($Languages) && is_null($Users))
  {
          foreach($countries as $country)
          { 
            $users = UserMeta::where('meta_key','country')->where('meta_value',$country)->get();
            foreach($users as $user)
            {
              foreach($nationalities as $nationality)
              {
                foreach($Languages as $languages)
                {
                  $users2 = User::where('ID',$user->user_id)->where('nationality',$nationality)->where('language',$languages)->first();
                  if(!is_null($users2))
                  {
                    $fcm_token = UserMeta::where('meta_key','fcm_device_token')->where('user_id',$users2->ID)->first();
                    if($fcm_token)
                    {
                      $IdUsers[] = $users2->ID;
                      $NameUsers = $users2->user_nicename;
                      app()->make('FcmService')->sendNotification($fcm_token->meta_value, $title,'HI'.' '.$NameUsers.'!'.' '.  $message,["data" => ['identifier' => 'AP']]);
                    }      
                  }
                  else
                  {
                    unset($users2);
                  }  
                }
              }
            }
          }
          if(!empty($IdUsers))
          {
            UserNotification::create([
              'user_id'=>$IdUsers ,
              'title'  =>$title ,
              'message'=>$message,
              'countries'=>$TotalCountries,
              'nationalities'=>$TotalNationalities,
              'languages'=>$TotalLanguages,
              'users'=>'-'
              ]);
            return redirect()->back()->with('success-msg','Send Notification Successfully');
          }
          else
          {
            return redirect()->back()->with('error-msg','sorry no available users according to your credentials');
          }
  }

  elseif(!is_null($countries) && !is_null($nationalities) && is_null($Languages) && is_null($Users))
  {
          foreach($countries as $country)
          {
            $users = UserMeta::where('meta_key','country')->where('meta_value',$country)->get();
            foreach($users as $user)
            {
              foreach($nationalities as $nationality)
              {
                $users2 = User::where('ID',$user->user_id)->where('nationality',$nationality)->first();
                if(!is_null($users2))
                { 
                  $fcm_token = UserMeta::where('meta_key','fcm_device_token')->where('user_id',$users2->ID)->first();
                  if($fcm_token)
                  {
                    $IdUsers[] = $users2->ID;
                    $NameUsers = $users2->user_nicename;
                    app()->make('FcmService')->sendNotification($fcm_token->meta_value, $title,'HI'.' '.$NameUsers.'!'.' '. $message,["data" => ['identifier' => 'AP']]);
                  }
                }
                else
                {
                  unset($users2);
                }
              }
            }
          }
          if(!empty($IdUsers))
          {
            UserNotification::create([
              'user_id'=>$IdUsers ,
              'title'  =>$title ,
              'message'=>$message,
              'countries'=>$TotalCountries,
              'nationalities'=>$TotalNationalities,
              'languages'=>'-',
              'users'=>'-'
              ]);
            return redirect()->back()->with('success-msg','Send Notification Successfully');
          }
          else
          {
            return redirect()->back()->with('error-msg','sorry no available users according to your credentials');
          }
  }

  elseif(!is_null($countries) && is_null($nationalities) && !is_null($Languages) && is_null($Users))
  {
          foreach($countries as $country)
          {
            $users = UserMeta::where('meta_key','country')->where('meta_value',$country)->get();
            foreach($users as $user)
            {
              foreach($Languages as $languages)
              {
                $users2 = User::where('ID',$user->user_id)->where('language',$languages)->first();
                if(!is_null($users2))
                {
                  $fcm_token = UserMeta::where('meta_key','fcm_device_token')->where('user_id',$users2->ID)->first();
                  if($fcm_token)
                  {
                    $IdUsers[] = $users2->ID;
                    $NameUsers = $users2->user_nicename;
                    app()->make('FcmService')->sendNotification($fcm_token->meta_value, $title,'HI'.' '.$NameUsers.'!'.' '. $message,["data" => ['identifier' => 'AP']]);
                  }
                }
                else
                {
                  unset($users2);
                }    
              }
            }
          }
          if(!empty($IdUsers))
          {
            UserNotification::create([
              'user_id'=>$IdUsers ,
              'title'  =>$title ,
              'message'=>$message,
              'countries'=>$TotalCountries,
              'nationalities'=>'-',
              'languages'=>$TotalLanguages,
              'users'=>'-'
              ]);
            return redirect()->back()->with('success-msg','Send Notification Successfully');
          }
          else
          {
            return redirect()->back()->with('error-msg','sorry no available users according to your credentials');
          }
  }

  elseif(is_null($countries) && !is_null($nationalities) && !is_null($Languages) && is_null($Users))
  {
          foreach($nationalities as $nationality)
          {
            foreach($Languages as $languages)
            {
              $users = User::where('nationality',$nationality)->where('language',$languages)->get();
              foreach($users as $user)
              {
                $fcm_token = UserMeta::where('meta_key','fcm_device_token')->where('user_id',$user->ID)->first();
                if($fcm_token)
                {
                $IdUsers[] = $user->ID;
                $NameUsers = $user->user_nicename;
                app()->make('FcmService')->sendNotification($fcm_token->meta_value, $title,'HI'.' '.$NameUsers.'!'.' '. $message,["data" => ['identifier' => 'AP']]);
                }
              }
            }
          }
          if(!empty($IdUsers))
            {
              UserNotification::create([
                'user_id'=>$IdUsers ,
                'title'  =>$title ,
                'message'=>$message,
                'countries'=>'-',
                'nationalities'=>$TotalNationalities,
                'languages'=>$TotalLanguages,
                'users'=>'-'
                ]);
              return redirect()->back()->with('success-msg','Send Notification Successfully');
            }
            else
            {
              return redirect()->back()->with('error-msg','sorry no available users according to your credentials');
            }
      }

      elseif(is_null($countries) && is_null($nationalities) && !is_null($Languages) && is_null($Users))
      {
            foreach($Languages as $language)
            {
              $LanguageUser = User::where('language',$language)->get();
              foreach($LanguageUser as $lnguser)
              {
                
                $fcm_token = UserMeta::where('meta_key','fcm_device_token')->where('user_id',$lnguser->ID)->first();
                if($fcm_token)
                {
                  $IdUsers[] = $lnguser->ID;
                  $NameUsers = $lnguser->user_nicename; 
                  app()->make('FcmService')->sendNotification($fcm_token->meta_value, $title,'HI'.' '.$NameUsers.'!'.' '. $message,["data" => ['identifier' => 'AP']]);
                }
              }
            }
            if(!empty($IdUsers))
            {
              UserNotification::create([
                'user_id'=>$IdUsers ,
                'title'  =>$title ,
                'message'=>$message,
                'countries'=>'-',
                'nationalities'=>'-',
                'languages'=>$TotalLanguages,
                'users'=>'-'
                ]);
              return redirect()->back()->with('success-msg','Send Notification Successfully');
            }
            else
            {
              return redirect()->back()->with('error-msg','sorry no available users according to your credentials');
            }
      }

      elseif(!is_null($countries) && is_null($nationalities) && is_null($Languages) && is_null($Users))
      {
            foreach($countries as $country)
            {
              $CountryUser = UserMeta::where('meta_key','country')->where('meta_value',$country)->get();
              foreach($CountryUser as $cuser)
              {
                $fcm_token = UserMeta::where('meta_key','fcm_device_token')->where('user_id',$cuser->user_id)->first();
                if($fcm_token)
                {
                  $IdUsers[] = $cuser->user_id;
                  $NameUsers = User::where('ID',$cuser->user_id)->pluck('user_nicename')->first(); 
                  app()->make('FcmService')->sendNotification($fcm_token->meta_value, $title,'HI'.' '.$NameUsers.'!'.' '. $message,["data" => ['identifier' => 'AP']]);
                }
              }
            }
            if(!empty($IdUsers))
            {
              UserNotification::create([
                'user_id'=>$IdUsers ,
                'title'  =>$title ,
                'message'=>$message,
                'countries'=>$TotalCountries,
                'nationalities'=>'-',
                'languages'=>'-',
                'users'=>'-'
                ]);
              return redirect()->back()->with('success-msg','Send Notification Successfully');
            }
            else
            {
              return redirect()->back()->with('error-msg','sorry no available users according to your credentials');
            }
      }

      elseif(is_null($countries) && !is_null($nationalities) && is_null($Languages) && is_null($Users))
      {
            foreach($nationalities as $nationality)
            {
              $NationalityUser = User::where('nationality',$nationality)->get();
              foreach($NationalityUser as $nuser)
              {
                $fcm_token = UserMeta::where('meta_key','fcm_device_token')->where('user_id',$nuser->ID)->first();
                if($fcm_token)
                {
                  $IdUsers[] = $nuser->ID;
                  $NameUsers = $nuser->user_nicename;
                  app()->make('FcmService')->sendNotification($fcm_token->meta_value, $title,'HI'.' '.$NameUsers.'!'.' '. $message,["data" => ['identifier' => 'AP']]);
                }
              }
            }    
                  if(!empty($IdUsers))
                  {
                    UserNotification::create([
                      'user_id'=>$IdUsers ,
                      'title'  =>$title ,
                      'message'=>$message,
                      'countries'=>'-',
                      'nationalities'=>$TotalNationalities,
                      'languages'=>'-',
                      'users'=>'-'
                      ]);
                    return redirect()->back()->with('success-msg','Send Notification Successfully');
                  }
                  else
                  {
                    return redirect()->back()->with('error-msg','sorry no available users according to your credentials');
                  }
      }
      elseif(is_null($countries) && is_null($nationalities) && is_null($Languages) && !is_null($Users))
      {
        foreach($Users as $UserId)
        {
          $fcm_token = UserMeta::where('meta_key','fcm_device_token')->where('user_id',$UserId)->first();
          if($fcm_token)
                {
                  $Userss = User::where('ID',$UserId)->first();
                  $IdUsers[] = $Userss->ID;
                  $SpecificUsers[] = $Userss->user_nicename;
                  $NameUsers = $Userss->user_nicename;   
                 app()->make('FcmService')->sendNotification($fcm_token->meta_value, $title,'HI'.' '.$NameUsers.'!'.' '. $message,["data" => ['identifier' => 'AP']]);
                }
        }
        UserNotification::create([
          'user_id'=>$IdUsers ,
          'title'  =>$title ,
          'message'=>$message,
          'countries'=>'-',
          'nationalities'=>'-',
          'languages'=>'-',
          'users'=>$SpecificUsers
          ]);
        return redirect()->back()->with('success-msg','Send Notification Successfully');
      }
      elseif(!is_null($countries) && !is_null($nationalities) && !is_null($Languages) && !is_null($Users))
      {
        return redirect()->back()->with('error-msg','Sorry Not Send Notification Successfully! Plz Select only users');      
      }
      elseif(!is_null($Users) && !is_null($countries) && !is_null($nationalities))
      {
        return redirect()->back()->with('error-msg','Sorry Not Send Notification Successfully! Plz Select only users');
      }
      elseif(!is_null($Users) && !is_null($countries) && !is_null($Languages))
      {
        return redirect()->back()->with('error-msg','Sorry Not Send Notification Successfully! Plz Select only users');
      }
      elseif(!is_null($Users) && !is_null($nationalities) && !is_null($Languages))
      {
        return redirect()->back()->with('error-msg','Sorry Not Send Notification Successfully! Plz Select only users');
      }
      elseif(!is_null($Users) && !is_null($countries))
      {
        return redirect()->back()->with('error-msg','Sorry Not Send Notification Successfully! Plz Select only users');
      }
      elseif(!is_null($Users) && !is_null($nationalities))
      {
        return redirect()->back()->with('error-msg','Sorry Not Send Notification Successfully! Plz Select only users');
      }
      elseif(!is_null($Users) && !is_null($Languages))
      {
        return redirect()->back()->with('error-msg','Sorry Not Send Notification Successfully! Plz Select only users');
      }
}


    public function ViewNotifications(Request $request)
    {
      
        if($request->ajax())
		    {
            $notifications = UserNotification::query();
            return DataTables::of($notifications)
            ->addColumn('action',function($notifications){
            $button = '<a href="'.route('resend-notificatons',$notifications->id).'" type="button" name="add" id="resend-notifications" class="btn btn-info btn-sm float-left">Resend</a>';
            return $button; 
            })
            ->addColumn('Countries',function( $notifications){
              $Countries = collect($notifications->countries);
              $SliceCat = $Countries->slice(0,1);
              $button1 = '<a href="'.$notifications->id.'"  onclick="CountryClick(this,event)" class=" float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
              if($SliceCat == '["-"]')
              {
                return '-';
              }
              if(count($Countries)<2)
              {
                return $SliceCat;
              }
              return $SliceCat.$button1;
            })
            ->addColumn('Nationalities',function( $notifications){
              $Nationalities = collect($notifications->nationalities);
              $SliceNat = $Nationalities->slice(0,1);
              $button2 = '<a href="'.$notifications->id.'" onclick="NationalityClick(this,event)" class=" float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
              if($SliceNat == '["-"]')
              {
                return '-';
              }
              if(count($Nationalities)<2)
              {
                return $SliceNat;
              }
              return $SliceNat.$button2;
            })
            ->addColumn('Languages',function( $notifications){
              $Languages = collect($notifications->languages);
              $SliceLan = $Languages->slice(0,1);
              $button3 = '<a href="'.$notifications->id.'" onclick="LanguageClick(this,event)" class=" float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
              if($SliceLan == '["-"]')
              {
                return '-';
              }
              if(count($Languages)<2)
              {
                return $SliceLan;
              }
              return $SliceLan.$button3;
            })
            ->addColumn('Users',function( $notifications){
              $Users = collect($notifications->users);
              $SliceUser = $Users->slice(0,1);
              $button4 = '<a href="'.$notifications->id.'" onclick="UsersClick(this,event)" class="float-left" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-caret-down" style="font-size:30px;color:black;"></i></a>';
              if($SliceUser == '["-"]')
              {
                return '-';
              }
              if($SliceUser == '["All Users"]')
              {
                return 'All Users';
              }
              if(count($Users)<2)
              {
                return $SliceUser;
              }
              return $SliceUser.$button4;
            })
            ->rawColumns(['action','Countries','Nationalities','Languages','Users'])
            ->make(true);
        }
		    return view('Notifications.View-Notifications');
    }
    public function ResendNotifications($notification_id)
    {
      $Notification = UserNotification::findorfail($notification_id);
      $SaveNotification =  UserNotification::create([
          'user_id'       => $Notification->user_id ,
          'title'         => $Notification->title ,
          'message'       => $Notification->message ,
          'countries'     => $Notification->countries ,
          'nationalities' => $Notification->nationalities ,
          'languages'     => $Notification->languages ,
          'users'         => $Notification->users 
      ]);
      $title = $Notification->title;
      $message = $Notification->message;
      $Users_id = $Notification->user_id;
      foreach($Users_id as $user)
      {
        $fcm_token = UserMeta::where('meta_key','fcm_device_token')->where('user_id',$user)->first();
        if($fcm_token)
        {
          $UserName = User::where('ID',$user)->pluck('user_nicename')->first();  
          app()->make('FcmService')->sendNotification($fcm_token->meta_value, $title,'HI'.' '.$UserName.'!'.' '. $message,[]);
        }        
      }
      return redirect()->back()->with('success-msg','Notification Resend Successfully');
    }
    public function MoreCountries($id)
    {
      $notifications = UserNotification::where('id',$id)->first();
      $countries[] = $notifications->countries;
      return response()->json($countries);
    }
    public function MoreNationalities($id)
    {
      $notifications = UserNotification::where('id',$id)->first();
      $nationalities[] = $notifications->nationalities;
      return response()->json($nationalities);
    }
    public function MoreLanguages($id)
    {
      $notifications = UserNotification::where('id',$id)->first();
      $languages[] = $notifications->languages;
      return response()->json($languages);
    }
    public function MoreUsers($id)
    {
      $notifications = UserNotification::where('id',$id)->first();
      $users[] = $notifications->users;
      return response()->json($users);
    }

}
