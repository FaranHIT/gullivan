<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\Http\Requests\SaveCity;
use App\Http\Requests\SaveCountry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use MikeMcLin\WpPassword\Facades\WpPassword;
use Auth;
use App\User;
use App\Role;
use App\Permission;
use App\RoleUser;
use App\PermissionUser;
use App\CountryData;
use App\CityData;
use App\CityCategory;
use App\CityCategoryDetail;
use Yajra\DataTables\Facades\DataTables;
class AdminController extends Controller
{
    public function countries(Request $request)
    {
        $name = [];
        $countries=[];
        $LoginUser = Auth::user();
        if($LoginUser->hasRole(['superadmin','admin']))
        {
            if ($request->ajax())
            {
            $LoginUser = Auth::user();
            $countries = Country::query();
            return DataTables::of($countries)
                ->addColumn('action',function ($countries) use($LoginUser){
                    $button = '<a href="'.route('updateCountryForm',$countries->id).'" type="button" name="update" class="btn btn-info btn-sm float-left mx-2">Update Country</a>';
                    if($LoginUser->hasRole('superadmin'))
                    {
                    $button .= '<a href="' .$countries->id. '" onclick="deleteCountry(this,event)" class="float-right mx-2" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-trash" style="font-size:30px;color:red;" aria-hidden="true"></i></a>';
                    }
                    $CheckCountry = CountryData::where('country_id',$countries->id)->first();
                    if(!is_null($CheckCountry))
                    {
                        $button .= '<a href="'.route('view-country-data-form',$countries->id).'" type="button" name="" class="btn btn-info btn-sm float-right mx-2">View Country Data</a>';
                        $button .= '<a href="'.route('update-country-data-form',$countries->id).'" type="button" name="" class="btn btn-info btn-sm float-right mx-2">Update Country Data</a>';
                        return $button;
                    }
                    $button .= '<a href="'.route('country-data-form',$countries->id).'" type="button" name="" class="btn btn-info btn-sm float-right mx-2">Add Country Data</a>';
                    return $button;

                })
                ->rawColumns(['action'])
                ->make(true);
            }
            return view('admin-panel.countries');
        }
        elseif($LoginUser->hasRole('manager'))
        {
            if ($request->ajax())
            {
                $permissionID = PermissionUser::where('user_id',Auth::id())->get();
                foreach($permissionID as $pid)
                {
                    $permissionName =Permission::where('id',$pid->permission_id)->get();
                    foreach($permissionName as $pname)
                    {
                        $name[] = $pname;
                    }
                }

                foreach($name as $n)
                {
                    $countries1 = Country::where('name',$n->name)->first();
                    if(is_null($countries1))
                    {
                        $CountriesNull = $countries1;
                    }
                    else
                    {
                        $countries[] = $countries1;
                    }
                }
                return DataTables::of($countries)
                ->addColumn('action',function ($countries){
                    $button = '<a href="'.route('updateCountryForm',$countries->id).'" type="button" name="update" class="btn btn-info btn-sm float-left">Update Country</a>';
                    // $button .= '<a href="'.route('removeCountry',$countries->id).'" type="button" name="delete" class="btn btn-danger btn-sm float-right">Delete Country</a>';
                    $CheckCountry = CountryData::where('country_id',$countries->id)->first();
                    if(!is_null($CheckCountry))
                    {
                        $button .= '<a href="'.route('view-country-data-form',$countries->id).'" type="button" name="" class="btn btn-info btn-sm float-right">View Country Data</a>';
                        $button .= '<a href="'.route('update-country-data-form',$countries->id).'" type="button" name="" class="btn btn-info btn-sm float-right">Update Country Data</a>';
                        return $button;
                    }
                    $button .= '<a href="'.route('country-data-form',$countries->id).'" type="button" name="" class="btn btn-info btn-sm float-right">Add Country Data</a>';
                    return $button;

                })
                ->rawColumns(['action'])
                ->make(true);
            }
            return view('admin-panel.countries');
        }

    }
    public function getCountries()
    {
        return DataTable::of(Country::query())->make(true);
    }
    public function addCountryForm()
    {
        return view('admin-panel.add-countries');
    }
    public function saveCountries(SaveCountry $request)
    {
        $loginuser = Auth::user();
      if($loginuser->hasRole(['admin','superadmin']))
      {
        $check_country = Country::where('name',$request->input('name'))->get();
        if (count($check_country) > 0)
        {
            return redirect()->back()->with('alert-msg','Country Already Exist!');
        }
        else
        {
            $country_data = $request->except('country_img');
            $save_country = new Country();
            $save_country->country_image = $request->file('country_img')->store('countries','public');

            // Start Laratrust Add Country Permission
            $permission = new Permission();
		    $permission->name         = $request->input('name');
		    $permission->display_name = $request->input('name'); // optional
		    $permission->description  = 'only add , edit and delete data of this country'; // optional
            $permission->save();
            // End Laratrust Add Country Permission

            if ($save_country->fill($country_data)->save())
            {
                return redirect('countries')->with('success-msg','Country Has Been Saved Successfully!');
            }
            else
            {
                return redirect('countries')->with('error-msg','Country Not Saved! Try Again.');
            }
        }
      }
      else
      {
        return redirect()->back()->with('error-msg','only Super Admin and Admin can add country.');
      }
    }
    public function updateCountryForm($id)
    {
        $get_country = Country::find($id);
        return view('admin-panel.update-countries',compact('get_country'));
    }
    public function updateCountry(Request $request,$id)
    {
        $country = Country::findOrFail($id);

            // Start Laratrust Update Country Permission
            $permission = Permission::where('name',$country->name)->first();
            if($permission)
            {
                $permission->name = $request->input('name');
                $permission->display_name = $request->input('name');
                $permission->save();
            }
            // End Laratrust Update Country Permission

            $country->country_image = $request->file('country_img')->store('countries','public');

            if ($country->save())
            {
                return redirect('countries')->with('success-msg','Country Has Been Updated Successfully!');
            }
            else
            {
                return redirect('countries')->with('error-msg','Country Not Updated! Try Again.');
            }
    }
    public function removeCountry($cid,$pwd)
    {
      $LoginUser = Auth::user();
      $user = User::where(['ID' => $LoginUser->ID])->first();
    if($user && WpPassword::check($pwd, $user->user_pass))
    {
        if($LoginUser->hasRole('superadmin'))
      {
        $country = Country::findOrFail($cid);

        // Start Laratrust Remove Country Permission and all cities remove of selected country
        $permission = Permission::where('name',$country->name)->first();
        $CountryId = Country::where('name',$permission->name)->pluck('id')->first();
        $Cities = City::where('country_id',$CountryId)->get();
        if($permission)
            {
                $permission->delete();
                foreach($Cities as $city)
                {
                    $CheckPermission = Permission::where('display_name',$city->name)->first();
                    $CheckPermission->delete();

                }
            }
        // End Laratrust Remove Country Permission

        if ($country->delete())
        {
            $success = "country delete successfully";
            return response()->json($success);
        }
        else
        {
            $success = "Sorry Country cannot be deleted";
            return response()->json($success);
        }
    }
    else
    {
        $success = "Sorry Only Admin Can Country deleted";
        return response()->json($success);
    }
}
else
{
    $success = "Incorrect Password";
    return response()->json($success);
}

    }

    public function cities(Request $request)
    {
        $LoginUser = Auth::user();
        if($LoginUser->hasRole(['admin','superadmin']))
        {
            if ($request->ajax())
        {
            // $cities = City::with('country')->get();
            $cities = City::query()->with('country');
            return DataTables::of($cities)
                ->addColumn('action',function ($cities) use($LoginUser){

                    $button = '<a href="'.route('updateCityForm',$cities->id).'" type="button" name="update" class="btn btn-info btn-sm float-left mx-2">Update City</a>';
                    $button .= '<a href="'.route('add-category-items-form',$cities->id).'" type="button" class="btn btn-info btn-sm mx-2 float-left">Add Category Items</a>';
                    $button .= '<a href="'.route('ViewCategoryItems',$cities->id).'" type="button" class="btn btn-info btn-sm mx-2 float-left">View Category Items</a>';
                    if($LoginUser->hasRole('superadmin'))
                    {
                    $button .= '<a href="'.$cities->id.'" onclick="deleteCity(this,event)" class="float-right" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-trash" style="font-size:30px;color:red;" aria-hidden="true"></i></a>';
                    }
                    $CheckCity = CityData::where('city_id',$cities->id)->first();
                    if(!is_null($CheckCity))
                    {
                        $button .= '<a href="'.route('view-city-data-form',$cities->id).'" type="button" name="delete" class="btn btn-info btn-sm float-right mx-2">View City Data</a>';
                        $button .= '<a href="'.route('update-city-data-form',$cities->id).'" type="button" name="delete" class="btn btn-info btn-sm float-right mx-2">Update City Data</a>';
                        return $button;
                    }
                    $button .= '<a href="'.route('city-data-form',$cities->id).'" type="button" name="delete" class="btn btn-info btn-sm float-right">Add City Data</a>';
                    return $button;
                })
                ->addColumn('country_name',function($cities){
                    return $cities->country->name;
                })
                ->rawColumns(['country_name','action'])
                ->make(true);
        }
        return view('admin-panel.cities');
        }
        elseif($LoginUser->hasRole('manager'))
        {
            if ($request->ajax())
            {
        $name = [];
        $cities= [];
        $permissionID = PermissionUser::where('user_id',Auth::id())->get();
        foreach($permissionID as $pid)
        {
            $permissionName =Permission::where('id',$pid->permission_id)->get();
            foreach($permissionName as $pname)
            {
                $name[] = $pname;
            }
        }
        foreach($name as $n)
        {
            $citiess = City::with('country')->where('name',$n->display_name)->first();
            if(!is_null($citiess))
            {
                $cities[] = $citiess;
            }
        }

            return DataTables::of($cities)
            ->addColumn('action',function ($cities){
                $button = '<a href="'.route('updateCityForm',$cities->id).'" type="button" name="update" class="btn btn-info btn-sm float-left mx-2">Update City</a>';
                $button .= '<a href="'.route('add-category-form',$cities->id).'" type="button" class="btn btn-info btn-sm float-left mx-2">Add Categories</a>';
                $button .= '<a href="'.route('view-category-form',$cities->id).'" type="button" class="btn btn-info btn-sm float-left mx-2">View Categories</a>';
                // $button .= '<a href="'.route('removeCity',$cities->id).'" type="button" name="delete" class="btn btn-danger btn-sm float-right">Delete City</a>';
                $CheckCity = CityData::where('city_id',$cities->id)->first();
                if(!is_null($CheckCity))
                {
                    $button .= '<a href="'.route('view-city-data-form',$cities->id).'" type="button" name="delete" class="btn btn-info btn-sm float-right mx-2">View City Data</a>';
                    $button .= '<a href="'.route('update-city-data-form',$cities->id).'" type="button" name="delete" class="btn btn-info btn-sm float-right mx-2">Update City Data</a>';
                    return $button;
                }
                $button .= '<a href="'.route('city-data-form',$cities->id).'" type="button" name="delete" class="btn btn-info btn-sm float-right mx-2">Add City Data</a>';
                return $button;
            })
            ->addColumn('country_name',function ($cities){
                return $cities->country->name;
            })
            ->rawColumns(['country_name','action'])
            ->make(true);
        }
        return view('admin-panel.cities');
        }
    }
    public function addCityForm()
    {
        $user = Auth::user();
        if($user->hasRole(['admin','superadmin']))
        {
        $countries =Country::all();
        return view('admin-panel.add-cities',compact('countries'));
        }

        elseif($user->hasRole('manager'))
        {
        $name =[];
        $countries =[];
        $permissionID = PermissionUser::where('user_id',Auth::id())->get();

        foreach($permissionID as $pid)
        {
            $permissionName =Permission::where('id',$pid->permission_id)->get();
            foreach($permissionName as $pname)
            {
                $name[] = $pname;
            }
        }
        foreach($name as $n)
        {
            $ChooseCountries =Country::where('name',$n->name)->get();
            foreach($ChooseCountries as $cn)
            {
                $countries[] = $cn;
            }
        }
            return view('admin-panel.add-cities',compact('countries'));
        }

    }
    public function saveCity(SaveCity $request)
    {
        $check_city = City::where('name',$request->input('name'))->where('country_id',$request->input('country'))->get();
        if (count($check_city) > 0)
        {
            return redirect()->back()->with('error-msg','City Already Exist for Selected Country!');
        }
        else
        {
            $city_data = $request->except('city_img','place');
            $save_city = new City();
            $save_city->city_image = $request->file('city_img')->store('cities','public');

            // Start Laratrust add City permission
            $permission = new Permission();
            $permission->name         = str_replace(' ', '',$request->input('name'));
		    $permission->display_name = $request->input('name'); // optional
		    $permission->description  = 'only add , edit and delete data of this city'; // optional
            $permission->save();
            // End Laratrust add City permission

            if ($save_city->fill($city_data)->save())
            {
                return redirect('cities')->with('success-msg','City Has Been Saved Successfully!');
            }
            else
            {
                return redirect('cities')->with('error-msg','City Not Saved Successfully!');
            }
        }
    }
    public function updateCityForm($id)
    {
        $get_city = City::findOrFail($id);
        $countries = Country::all();
        return view('admin-panel.update-cities',compact('countries','get_city'));
    }
    public function updateCity(Request $request, $id)
    {
        $city = City::with('country')->find($id);

        // Start Laratrust Update City Permission
        $permission = Permission::where('display_name',$city->name)->first();
        if($permission)
        {
            $permission->name = str_replace(' ','',$request->input('cityName'));
            $permission->display_name = $request->input('cityName');
            $permission->save();
        }
        // End Laratrust Update City Permission

            $city->name = $request->input('cityName');
            $city->country_id = $request->input('country');
            $city->city_image = $request->file('city_img')->store('cities','public');
            $city->geo_lat =  $request->input('city_lat');
            $city->geo_long = $request->input('city_long');
            if ($city->save())
            {
                return redirect('cities')->with('success-msg','City Has Been Updated Successfully!');
            }
            else
            {
                return redirect('cities')->with('error-msg','City Not Updated! Try Again.');
            }
    }
    public function removeCity($cid,$pwd)
    {
      $LoginUser = Auth::user();
      $user = User::where(['ID' => $LoginUser->ID])->first();
    if($user && WpPassword::check($pwd, $user->user_pass))
    {
        if($LoginUser->hasRole('superadmin'))
        {
                $city = City::findOrFail($cid);
                // Start Laratrust Remove City Permission
                $permission = Permission::where('display_name',$city->name)->first();
                if($permission)
                {
                    $permission->delete();
                }
                // End Laratrust Remove City Permission
            if ($city->delete())
            {
                $success = "city delete successfully";
                return response()->json($success);
            }
            else
            {
                $success = "Sorry City cannot be deleted";
                return response()->json($success);
            }
        }
        else
        {
            $success = "Sorry Only Admin Can City deleted";
            return response()->json($success);
        }
    }
    else
    {
        $success = "Incorrect Password";
        return response()->json($success);
    }
}
    public function CountryDataForm($id)
    {
        $country_id = $id;
        return view('admin-panel.Country-Data-Form',compact('country_id'));
    }
    public function SaveCountryData(Request $request)
    {
        $country_id         = $request->input('country_id');
        $Video              = $request->input('Video-Link');
        $BestTime           = $request->input('Best-Time');
        $Transportation     = $request->input('Transportation');
        $Weather            = $request->input('Weather');
        $Information        = $request->input('Information');
        $Electric           = $request->input('Electric');
        $Language           = $request->input('Language');
        $Currency           = $request->input('Currency');
        $Constitution       = $request->input('Constitution');
        $EmergenctNo        = $request->input('emergency-no');
        $Emabssie           = $request->input('embassies');
        $NewOffer           = $request->input('New-Offer');
        $Rules              = $request->input('Rules');
        $History            = $request->input('History');
        $Driving            = $request->input('Driving');
        $Universities       = $request->input('Universities');
        $visaUniFee         = $request->input('visa-uni-fee');
        $Tours              = $request->input('Tours');

        request()->validate([
            'Video-Link'        => 'required',
            'Best-Time'         => 'required',
            'Transportation'    => 'required',
            'Weather'           => 'required',
            'Information'       => 'required',
            'Electric'          => 'required',
            'Language'          => 'required',
            'Currency'          => 'required'
        ]);

        $CountryData = CountryData::create([
            'country_id'                => $country_id ,
            'constitution'              => $Constitution ,
            'emergency_no'              => $EmergenctNo ,
            'embassies'                 => $Emabssie ,
            'new_offer'                 => $NewOffer ,
            'rules'                     => $Rules ,
            'history'                   => $History ,
            'driving'                   => $Driving ,
            'universities'              => $Universities ,
            'visa_uni_acceptance_fee'   => $visaUniFee ,
            'tours'                     => $Tours ,
            'youtube_link'              => $Video ,
            'best_time_to_go'           => $BestTime ,
            'transportation'            => $Transportation ,
            'weather'                   => $Weather ,
            'information'               => $Information ,
            'the_electric'              => $Electric ,
            'language'                  => $Language ,
            'currency'                  => $Currency
        ]);

        if($CountryData->save())
        {
            return redirect('countries')->with('success-msg','Country Data Inserted Successfully');
        }
        else
        {
            return redirect('countries')->with('error-msg','Country Data Not Inserted Successfully');
        }
    }
    public function UpdateCountryDataForm($id)
    {
        $CountryData = CountryData::where('country_id',$id)->first();
        return view('admin-panel.Update-Country-Data-Form',compact('CountryData'));
    }
    public function UpdateCountryData(Request $request)
    {
        $country_id         = $request->input('country_id');
        $Video              = $request->input('Video-Link');
        $BestTime           = $request->input('Best-Time');
        $Transportation     = $request->input('Transportation');
        $Weather            = $request->input('Weather');
        $Information        = $request->input('Information');
        $Electric           = $request->input('Electric');
        $Language           = $request->input('Language');
        $Currency           = $request->input('Currency');
        $Constitution       = $request->input('Constitution');
        $EmergenctNo        = $request->input('emergency-no');
        $Emabssie           = $request->input('embassies');
        $NewOffer           = $request->input('New-Offer');
        $Rules              = $request->input('Rules');
        $History            = $request->input('History');
        $Driving            = $request->input('Driving');
        $Universities       = $request->input('Universities');
        $visaUniFee         = $request->input('visa-uni-fee');
        $Tours              = $request->input('Tours');

        request()->validate([
            'Video-Link'        => 'required',
            'Best-Time'         => 'required',
            'Transportation'    => 'required',
            'Weather'           => 'required',
            'Information'       => 'required',
            'Electric'          => 'required',
            'Language'          => 'required',
            'Currency'          => 'required'
        ]);

        $CheckData = CountryData::where('country_id',$country_id)->first();
        if($CheckData)
        {
            $CheckData->constitution              = $Constitution ;
            $CheckData->emergency_no              = $EmergenctNo ;
            $CheckData->embassies                 = $Emabssie ;
            $CheckData->new_offer                 = $NewOffer ;
            $CheckData->rules                     = $Rules ;
            $CheckData->history                   = $History ;
            $CheckData->driving                   = $Driving ;
            $CheckData->universities              = $Universities ;
            $CheckData->visa_uni_acceptance_fee   = $visaUniFee ;
            $CheckData->tours                     = $Tours ;
            $CheckData->youtube_link              = $Video ;
            $CheckData->best_time_to_go           = $BestTime ;
            $CheckData->transportation            = $Transportation ;
            $CheckData->weather                   = $Weather ;
            $CheckData->information               = $Information ;
            $CheckData->the_electric              = $Electric ;
            $CheckData->language                  = $Language ;
            $CheckData->currency                  = $Currency ;
            $CheckData->update();
        }
        if($CheckData->update())
        {
            return redirect('countries')->with('success-msg','Country Data Update Successfully');
        }
        else
        {
            return redirect('countries')->with('error-msg','Country Data Not Update successfully');
        }
    }
    public function ViewCountryDataForm($id)
    {
        $CountryData = CountryData::where('country_id',$id)->first();
        return view('admin-panel.View-Country-Data',compact('CountryData'));
    }
    public function CityDataForm($id)
    {
        $city_id = $id;
        return view('admin-panel.City-Data-form',compact('city_id'));
    }
    public function SaveCityData(Request $request)
    {
        $city_id            = $request->input('city_id');
        $Video              = $request->input('Video-Link');
        $BestTime           = $request->input('Best-Time');
        $Transportation     = $request->input('Transportation');
        $Weather            = $request->input('Weather');
        $Information        = $request->input('Information');
        $RentACar           = $request->input('Rent-A-Car');
        $RealEstate         = $request->input('Real-Estate');
        $Hotels             = $request->input('Hotels');
        $Coffee             = $request->input('Coffee');
        $Restaurants        = $request->input('Restaurants');
        $Bars               = $request->input('Bars');
        $AddYourBuisness    = $request->input('Add-Your-Buisness');
        $Taxis              = $request->input('Taxis');
        $RoadLaw            = $request->input('Road-Law');
        $Lawyers            = $request->input('Lawyers');
        $Events             = $request->input('Events');
        $Tours              = $request->input('Tours');

        request()->validate([
            'Video-Link'        => 'required',
            'Best-Time'         => 'required',
            'Transportation'    => 'required',
            'Weather'           => 'required',
            'Information'       => 'required'
        ]);

        $CityData = CityData::create([
            'city_id'                 => $city_id ,
            'rent_a_car'              => $RentACar ,
            'real_estate'             => $RealEstate ,
            'hotels'                  => $Hotels ,
            'coffee_shops'            => $Coffee ,
            'restaurants'             => $Restaurants ,
            'bars'                    => $Bars ,
            'add_ur_bussiness'        => $AddYourBuisness ,
            'taxis'                   => $Taxis ,
            'road_laws'               => $RoadLaw ,
            'lawyers'                 => $Lawyers ,
            'events'                  => $Events ,
            'tours'                   => $Tours ,
            'youtube'                 => $Video ,
            'best_time_to_go'         => $BestTime ,
            'transportation'          => $Transportation ,
            'weather'                 => $Weather ,
            'information'             => $Information
        ]);

        if($CityData->save())
        {
            return redirect('cities')->with('success-msg','City Data Inserted Successfully');
        }
        else
        {
            return redirect('cities')->with('error-msg','City Data Inserted Successfully');
        }
    }
    public function UpdateCityDataForm($id)
    {
        $CityData = CityData::where('city_id',$id)->first();
        return view('admin-panel.Update-City-Data-Form',compact('CityData'));
    }
    public function UpdateCityData(Request $request)
    {
        $city_id            = $request->input('city_id');
        $Video              = $request->input('Video-Link');
        $BestTime           = $request->input('Best-Time');
        $Transportation     = $request->input('Transportation');
        $Weather            = $request->input('Weather');
        $Information        = $request->input('Information');
        $RentACar           = $request->input('Rent-A-Car');
        $RealEstate         = $request->input('Real-Estate');
        $Hotels             = $request->input('Hotels');
        $Coffee             = $request->input('Coffee');
        $Restaurants        = $request->input('Restaurants');
        $Bars               = $request->input('Bars');
        $AddYourBuisness    = $request->input('Add-Your-Buisness');
        $Taxis              = $request->input('Taxis');
        $RoadLaw            = $request->input('Road-Law');
        $Lawyers            = $request->input('Lawyers');
        $Events             = $request->input('Events');
        $Tours              = $request->input('Tours');

        request()->validate([
            'Video-Link'        => 'required',
            'Best-Time'         => 'required',
            'Transportation'    => 'required',
            'Weather'           => 'required',
            'Information'       => 'required'
        ]);

        $CheckCity = CityData::where('city_id',$city_id)->first();
        if($CheckCity)
        {
            $CheckCity->rent_a_car              = $RentACar ;
            $CheckCity->real_estate             = $RealEstate ;
            $CheckCity->hotels                  = $Hotels ;
            $CheckCity->coffee_shops            = $Coffee ;
            $CheckCity->restaurants             = $Restaurants ;
            $CheckCity->bars                    = $Bars ;
            $CheckCity->add_ur_bussiness        = $AddYourBuisness ;
            $CheckCity->taxis                   = $Taxis ;
            $CheckCity->road_laws               = $RoadLaw ;
            $CheckCity->lawyers                 = $Lawyers ;
            $CheckCity->events                  = $Events ;
            $CheckCity->tours                   = $Tours ;
            $CheckCity->youtube                 = $Video ;
            $CheckCity->best_time_to_go         = $BestTime ;
            $CheckCity->transportation          = $Transportation ;
            $CheckCity->weather                 = $Weather ;
            $CheckCity->information             = $Information ;
            $CheckCity->update();
        }
        if($CheckCity->update())
        {
            return redirect('cities')->with('success-msg','City Data Update Successfully');
        }
        else
        {
            return redirect('cities')->with('error-msg','City Data Not Update Successfully');
        }
    }
    public function ViewCityDataForm($id)
    {
        $CityData = CityData::where('city_id',$id)->first();
        return view('admin-panel.View-City-Data-Form',compact('CityData'));
    }
    public function AddCategoryForm()
    {
        return view('admin-panel.Add-Category-Form');
    }
    public function SaveCategory(Request $request)
    {
        $save_category = new CityCategory([
            'category_name' => $request->input('category_name')
        ]);
        if ($save_category->save()){
            return redirect()->back()->with('success-msg','Category Has Been Added Successfully!');
        }else
        {
            return redirect()->back()->with('error-msg','Category Not Added!');
        }
    }
    public function ViewCategoryForm(Request $request)
    {
     if($request->ajax())
        {
            // $categories = CityCategory::all();
            $categories = CityCategory::query();
            return DataTables::of($categories)
            ->addColumn('action', function($categories) {
                $button = '<a href="'.route('DeleteCategory',$categories->id).'" type="button" class="btn btn-danger btn-sm float-right">Delete Category</a>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        return view('admin-panel.View-Category-Form');
    }
    public function CategoryItemsForm($id)
    {
        $all_categories = CityCategory::all();
        $city_id = $id;
        return view('admin-panel.Add-Category-Items',compact('city_id','all_categories'));
    }
    public function SaveCategoryItems(Request $request)
    {
        request()->validate([
            'category' => 'required',
            'picture' => 'required',
            'monday_time' => 'required',
            'tuesday_time' => 'required',
            'wednesday_time' => 'required',
            'thursday_time' => 'required',
            'friday_time' => 'required',
            'saturday_time' => 'required',
            'sunday_time' => 'required'
        ]);
        $categoryId = $request->input('category');
        $cityId = $request->input('city_id');
        $picture = $request->file('picture');
        if(count($picture)>5)
        {
            return redirect()->back()->with('error-msg','sorry images quantity not greater than 5');
        }

        foreach($picture as $p){
            $data[]= $p->store('CityItems','public');
        }
        $title = $request->input('title');
        $detail = $request->input('detail');
        $address = $request->input('address');
        $website = $request->input('website');
        $number = $request->input('number');
        $mondayTime = $request->input('monday_time');
        $tuesdayTime = $request->input('tuesday_time');
        $wednesdayTime = $request->input('wednesday_time');
        $thursdayTime = $request->input('thursday_time');
        $fridayTime = $request->input('friday_time');
        $saturdayTime = $request->input('saturday_time');
        $sundayTime = $request->input('sunday_time');

        $CityCategoryItem = CityCategoryDetail::create([
            'category_id' => $categoryId ,
            'picture'     => json_encode($data,JSON_UNESCAPED_SLASHES),
            'title'       => $title ,
            'details'     => $detail ,
            'address'     => $address ,
            'website'     => $website ,
            'number'      => $number ,
            'moday_time'      => $mondayTime ,
            'tuesday_time'      => $tuesdayTime ,
            'wednesday_time'      => $wednesdayTime ,
            'thursday_time'      => $thursdayTime ,
            'friday_time'      => $fridayTime ,
            'saturday_time'      => $saturdayTime ,
            'sunday_time'      => $sundayTime,
            'city_id' => $cityId
            ]);

        if($CityCategoryItem->save())
        {
            return redirect()->route('ViewCategoryItems',['id'=>$cityId])->with('success-msg','successfully save category Item');
        }
        else
        {
            return redirect()->with('error-msg','Sorry category Item not save successfully');
        }
    }
    public function DeleteCategory($id)
    {
        $category = CityCategory::where('id',$id)->first();
        $category->delete();
        return redirect()->back()->with('success','successfully remove Category');
    }
    public function ViewCategoryItems(Request $request,$id)
    {
        $CityId = $id;
        if($request->ajax())
        {
            // $categories = CityCategoryDetail::where('city_id',$CityId)->get();
            $categories = CityCategoryDetail::query()->where('city_id',$CityId);
            return DataTables::of($categories)
            ->addColumn('Timing',function($categories){
                return 'Monday:'.' '.$categories->moday_time.', '.'Tuesday:'.' '.$categories->tuesday_time.', '.'Wednesday:'.' '.$categories->wednesday_time.', '.'Thursday:'.' '.$categories->thursday_time.', '.'Friday:'.' '.$categories->friday_time.', '.'Saturday:'.' '.$categories->saturday_time.', '.'Sunday:'.' '.$categories->sunday_time;
            })
            ->editColumn('picture',function($categories){
                return json_decode($categories->picture,JSON_UNESCAPED_SLASHES);
            })
            ->addColumn('action', function($categories) {
                $button = '<a href="'.route('DeleteItems',$categories->id).'" type="button" class="btn btn-danger btn-sm float-right my-2">Delete Category Item</a>';
                $button .= '<a href="'.route('UpdateItems',$categories->id).'" type="button" class="btn btn-info btn-sm float-right my-2">Update Category Item</a>';
				return $button;
            })
            ->rawColumns(['Timing','action'])
            ->make(true);
        }
        return view('admin-panel.View-Category-Items',compact('CityId'));
    }
    public function UpdateCategoryItemsForm($id)
    {
        $ID = $id;
        $Items = CityCategoryDetail::where('id',$ID)->first();
        return view('admin-panel.Update-Categories-Items-Form',compact('Items'));
    }
    public function UpdateCategoryItems(Request $request)
    {
        $ItemID = $request->input('Items_id');
        $title = $request->input('title');
        $detail = $request->input('detail');
        $address = $request->input('address');
        $website = $request->input('website');
        $number = $request->input('number');
     //   $timing = $request->input('timing');
     $mondayTime = $request->input('monday_time');
     $tuesdayTime = $request->input('tuesday_time');
     $wednesdayTime = $request->input('wednesday_time');
     $thursdayTime = $request->input('thursday_time');
     $fridayTime = $request->input('friday_time');
     $saturdayTime = $request->input('saturday_time');
     $sundayTime = $request->input('sunday_time');

        $Items = CityCategoryDetail::where('id',$ItemID)->first();
        // $Items->title = $request->file('picture')->store('CityItems','public');
        $Items->title = $title;
        $Items->details = $detail;
        $Items->address = $address;
        $Items->website = $website;
        $Items->number  = $number;
       // $Items->timing  = $timing;
        $Items->moday_time = $mondayTime;
        $Items->tuesday_time = $tuesdayTime;
        $Items->wednesday_time = $wednesdayTime;
        $Items->thursday_time = $thursdayTime;
        $Items->friday_time = $fridayTime;
        $Items->saturday_time = $saturdayTime; 
        $Items->sunday_time = $sundayTime;
        $Items->update();

        if($Items->update())
        {
            return redirect()->route('ViewCategoryItems',['id'=>$Items->city_id])->with('success-msg','Item successfully updated');
        }
        else
        {
            return redirect()->back()->with('error-msg','sorry Item not update successfully.');
        }
    }
    public function DeleteCategoryItems($id)
    {
        $Items = CityCategoryDetail::where('id',$id)->first();
        $Items->delete();
        return redirect()->back()->with('success','successfully removed Item');
    }

    public function ImportDataView()
    {
        return view('admin-panel.import-data');
    }
    public function saveFileData(Request $request)
    {
        // $validate = $request->validate([
        //     'data_file' => 'required|mimes:json'
        // ]);
        $path = $request->file('data_file')->storeAs('Data','json-data');
        $contents = Storage::get($path);
        $all_data = json_decode($contents);
        if ($all_data->Country)
        {
            foreach ($all_data->Country as $data)
            {
                $cn_data1 = $data->City_Detail->list_array;
                $cn_data2 = $data->City_Detail->Tag_Array;
                $country_data = array_merge($cn_data1,$cn_data2);
                $Best_Time_To_Go = null; $Transportation = null; $Information = null; $Weather = null; $The_Electric = null;
                $Language = null; $Currency = null; $Constitution = null; $Emergency_Numbers = null; $Embassies = null;
                $New_Offers = null; $Rules = null; $History  =null; $Driving  = null; $Universities =null; $Visa_Uni_Acceptance_Free = null; $Tours = null;

                $save_country = new Country([
                    'name' => $data->title,
                    // 'country_image' =>$data->image_name,
                    'geo_lat' => $data->City_Detail->country_lat,
                    'geo_long' => $data->City_Detail->country_long
                ]);
                $save_country->save();

                // Start Laratrust Add Country Permission
                $permission = new Permission();
		        $permission->name         = $data->title;
		        $permission->display_name = $data->title; // optional
		        $permission->description  = 'only add , edit and delete data of this country'; // optional
                $permission->save();
                // End Laratrust Add Country Permission        


                for ($i=0; $i<count($country_data); $i++)
                {
                    for ($j=0; $j<count($country_data); $j++)
                    {
                        if ($country_data[$j]->title == 'Best Time To Go')
                        {
                            $Best_Time_To_Go = $country_data[$j]->desc;
                        }
                        elseif ($country_data[$j]->title == 'Transportation'){
                            $Transportation = $country_data[$j]->desc;
                        }
                        elseif ($country_data[$j]->title == 'Weather'){
                            $Weather = $country_data[$j]->desc;
                        }
                        elseif ($country_data[$j]->title == 'Information'){
                            $Information = $country_data[$j]->desc;
                        }
                        elseif ($country_data[$j]->title == 'The Electric'){
                            $The_Electric = $country_data[$j]->desc;
                        }
                        elseif ($country_data[$j]->title == 'Language'){
                            $Language = $country_data[$j]->desc;
                        }
                        elseif ($country_data[$j]->title == 'Currency'){
                            $Currency = $country_data[$j]->desc;
                        }
                        elseif ($country_data[$j]->title == $data->title.' Constitution'){
                            $Constitution = $country_data[$j]->url;
                        }
                        elseif ($country_data[$j]->title == 'Emergency Numbers'){
                            $Emergency_Numbers = $country_data[$j]->url;
                        }
                        elseif ($country_data[$j]->title == 'Embassies'){
                            $Embassies = $country_data[$j]->url;
                        }
                        elseif ($country_data[$j]->title == 'New Offers'){
                            $New_Offers = $country_data[$j]->url;
                        }
                        elseif ($country_data[$j]->title == $data->title.' Rules'){
                            $Rules = $country_data[$j]->url;
                        }
                        elseif ($country_data[$j]->title == $data->title.' Rules'){
                            $Rules = $country_data[$j]->url;
                        }
                        elseif ($country_data[$j]->title == 'History'){
                            $History = $country_data[$j]->url;
                        }
                        elseif ($country_data[$j]->title == 'Driving'){
                            $Driving = $country_data[$j]->url;
                        }
                        elseif ($country_data[$j]->title == 'Universities'){
                            $Universities = $country_data[$j]->url;
                        }
                        elseif ($country_data[$j]->title == 'Visa + Uni Acceptance Free'){
                            $Visa_Uni_Acceptance_Free = $country_data[$j]->url;
                        }
                        elseif ($country_data[$j]->title == 'Tours'){
                            $Tours = $country_data[$j]->url;
                        }
                    }
                }
                $save_country_data = new CountryData([
                    'country_id' => $save_country->id,
                    'constitution' =>$Constitution,
                    'emergency_no' => $Emergency_Numbers,
                    'embassies' => $Embassies,
                    'new_offer' => $New_Offers,
                    'rules' => $Rules,
                    'history' => $History,
                    'driving' => $Driving,
                    'universities' => $Universities,
                    'visa_uni_acceptance_fee' => $Visa_Uni_Acceptance_Free,
                    'tours' => $Tours,
                    'youtube_link' => $data->City_Detail->video_url,
                    'best_time_to_go' => $Best_Time_To_Go,
                    'transportation' => $Transportation,
                    'weather' => $Weather,
                    'information'=> $Information,
                    'the_electric' => $The_Electric,
                    'language' => $Language,
                    'currency' => $Currency
                ]);
                $save_country_data->save();
                foreach ($data->City_Detail->City_Array as $city)
                {
                    $ct_data1 = $city->list_array;
                    $ct_data2 = $city->Tag_Array;
                    $city_data = array_merge($ct_data1,$ct_data2);
                    $city_rent_a_car = null; $city_real_estate = null; $city_hotels = null; $city_coffee_shops = null; $city_restaurants = null; $city_Best_Time_To_Go = null; $city_Best_Time_To_Go = null; $city_Best_Time_To_Go = null;
                    $city_bars = null; $city_add_ur_bussiness = null; $city_taxis = null; $city_road_laws = null; $city_lawyers = null;
                    $city_events = null; $city_tours = null; $city_best_time_to_go = null; $city_transportation = null; $city_weather = null;
                    $city_information = null;
                    $save_cities = new City([
                        'country_id' => $save_country->id,
                        'name' => $city->title,
                        // 'city_image' => $city->image_name,
                        'geo_lat' => $city->city_lat,
                        'geo_long' => $city->city_long
                    ]);
                    $save_cities->save();

                    // Start Laratrust add City permission
                    $permission = new Permission();
                    $permission->name         = $city->title;
		            $permission->display_name = $city->title; // optional
		            $permission->description  = 'only add , edit and delete data of this city'; // optional
                    $permission->save();
                    // End Laratrust add City permission    

                    for ($i=0; $i<count($city_data); $i++)
                    {
                        for ($j=0; $j<count($city_data); $j++)
                        {
                            if ($city_data[$j]->title == 'Best Time To Go')
                            {
                                $city_best_time_to_go = $city_data[$j]->desc;
                            }
                            elseif ($city_data[$j]->title == 'Transportation'){
                                $city_transportation = $city_data[$j]->desc;
                            }
                            elseif ($city_data[$j]->title == 'Weather'){
                                $city_weather = $city_data[$j]->desc;
                            }
                            elseif ($city_data[$j]->title == 'Information'){
                                $city_information = $city_data[$j]->desc;
                            }
                            elseif ($city_data[$j]->title == 'Rent a car'){
                                $city_rent_a_car = $city_data[$j]->url;
                            }
                            elseif ($city_data[$j]->title == 'Real Estate'){
                                $city_real_estate = $city_data[$j]->url;
                            }
                            elseif ($city_data[$j]->title == 'Hotels'){
                                $city_hotels = $city_data[$j]->url;
                            }
                            elseif ($city_data[$j]->title == 'Restaurants'){
                                $city_restaurants = $city_data[$j]->url;
                            }
                            elseif ($city_data[$j]->title == 'Coffee'){
                                $city_coffee_shops = $city_data[$j]->url;
                            }
                            elseif ($city_data[$j]->title == 'Bars'){
                                $city_bars = $city_data[$j]->url;
                            }
                            elseif ($city_data[$j]->title == 'Add  Your Business'){
                                $city_add_ur_bussiness = $city_data[$j]->url;
                            }
                            elseif ($city_data[$j]->title == 'Taxis'){
                                $city_taxis = $city_data[$j]->url;
                            }
                            elseif ($city_data[$j]->title == 'Roads Law'){
                                $city_road_laws = $city_data[$j]->url;
                            }
                            elseif ($city_data[$j]->title == 'Lawyers'){
                                $city_lawyers = $city_data[$j]->url;
                            }
                            elseif ($city_data[$j]->title == 'Events'){
                                $city_events = $city_data[$j]->url;
                            }
                            elseif ($city_data[$j]->title == 'Tours'){
                                $city_tours = $city_data[$j]->url;
                            }
                        }
                    }
                    $save_cities_data = new CityData([
                        'city_id' => $save_cities->id,
                        'rent_a_car' => $city_rent_a_car,
                        'real_estate' => $city_real_estate,
                        'hotels' => $city_hotels,
                        'coffee_shops' => $city_coffee_shops,
                        'restaurants' => $city_restaurants,
                        'bars' => $city_bars,
                        'add_ur_bussiness' => $city_add_ur_bussiness,
                        'taxis' => $city_taxis,
                        'road_laws' => $city_road_laws,
                        'lawyers' => $city_lawyers,
                        'events' => $city_events,
                        'tours' => $city_tours,
                        'youtube' => $city->video_url,
                        'best_time_to_go' => $city_best_time_to_go,
                        'transportation' => $city_transportation,
                        'weather' => $city_weather,
                        'information' => $city_information,
                    ]);
                    $save_cities_data->save();
                    foreach ($city->item_Array as $category)
                    {
                        $get_city_categories = CityCategory::all();
                        $get_city_categories_id = null;
                        for ($x=0; $x<count($get_city_categories); $x++)
                        {
                            for ($y=0; $y<count($get_city_categories); $y++)
                            {
                                if (strtolower(str_replace(str_split('&+,-/_ '),"",$get_city_categories[$y]->category_name)) == strtolower(str_replace(str_split('&+,-/_ '),"",$category->title)))
                                {
                                    $get_city_categories_id = $get_city_categories[$y]->id;
                                }
                            }
                        }
                        $website_url = null;
                        if (property_exists($category, 'URL'))
                        {
                            $website_url = $category->URL;
                        }
                        else{
                            $website_url = null;
                        }
                        $save_cities_categories_details = new CityCategoryDetail([
                            'category_id' => $get_city_categories_id,
                            //'picture' => json_encode($category->image_name),
                            'title' => $category->title,
                            'website' => $website_url,
                            'moday_time' => '24 Hours',
                            'tuesday_time' => '24 Hours',
                            'wednesday_time' => '24 Hours',
                            'thursday_time' => '24 Hours',
                            'friday_time' => '24 Hours',
                            'saturday_time' => '24 Hours',
                            'sunday_time' => '24 Hours',
                            'city_id' => $save_cities->id,
                        ]);
                        $save_cities_categories_details->save();
                    }
                }
            }
            return redirect()->back()->with('success-msg','File Imported Successfully!');
        }
        else{
            return redirect()->back()->with('alert-msg','Invalid Data in the File!');
        }
    }
}
