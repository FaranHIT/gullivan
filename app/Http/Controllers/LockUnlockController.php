<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\User;
use App\UserMeta;
use App\Role;
use Auth;

class LockUnlockController extends Controller
{
    public function UsersLockUnlock(Request $request)
    { 
        if($request->ajax())
		{
            // $AdminSuperadmin=[];
            // $MangerAndUsers=[];
            // $users = User::all();
            $users = User::query()->doesntHave('roles')->orWhereRoleIs('manager');
            // foreach($users as $user)
            // {
            //   if($user->hasRole(['admin','superadmin']))
            //   {
            //     $AdminSuperadmin[] = $user;
            //   }  
            //   else
            //   {
            //     $MangerAndUsers[] = $user;
            //   }
            // }
            $LockUsers = UserMeta::where('meta_key','status_lock_unlock')->where('meta_value','locked')->get();
			      return DataTables::of($users)
            ->addColumn('action',function($users)  use ($LockUsers){
			    foreach($LockUsers as $lu)
			    {
					if($users->ID == $lu->user_id)
					{
						$button = '<a href="'.$lu->user_id.'" onclick="UserUnLock(this,event)" class="float-left"><i class="fa fa-unlock" style="font-size:20px;color:red;" aria-hidden="true">&nbsp&nbsp UnLock</i></a>';
						return $button;
					}			
                }
                $button = '<a href="'.$users->ID.'" onclick="UserLock(this,event)" class="float-left"><i class="fa fa-lock" style="font-size:20px;color:black;" aria-hidden="true">&nbsp&nbsp Lock</i></a>';
                return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
		}
		return view('Lock-Unlock.Lock-Unlock-Users');
    }
    public function LockUser($ID)
    {
      $LoginUser = Auth::user();
      if($LoginUser->hasRole(['superadmin','admin']))
      {
        $UnlockedUser=UserMeta::where('meta_key','status_lock_unlock')->where('meta_value','unlocked')->where('user_id',$ID)->first();
        if($UnlockedUser)
        {
            $UnlockedUser->meta_value = 'locked';
            $UnlockedUser->save();
            return response()->json('successfully locked this user');    
        }
        else
        {
            UserMeta::insert([
            'meta_key'   => 'status_lock_unlock' ,
            'meta_value' => 'locked' ,
            'user_id'    => $ID
             ]);
             return response()->json('successfully locked this user');
        }
      }
      else
      {
        return response()->json('sorry only SuperAdmin and Admin can users locked');
      } 
    }
    public function UnlockUser($ID)
    {
      $LoginUser = Auth::user();
      if($LoginUser->hasRole(['superadmin','admin']))
      {
        $LockUser = UserMeta::where('meta_key','status_lock_unlock')->where('meta_value','locked')->where('user_id',$ID)->first();
        if($LockUser)
        {
            $LockUser->meta_value = 'unlocked';
            $LockUser->save();
            return response()->json('successfully unlocked this user');
        }
      }
      else
      {
        return response()->json('sorry only SuperAdmin and Admin can users unlocked');
      }  
    }
}
