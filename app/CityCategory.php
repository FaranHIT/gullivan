<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityCategory extends Model
{
    protected $fillable = ['category_name'];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    // public function cityCategoryDetails()
    // {
    //     return $this->hasMany(CityCategoryDetail::class,'category_id','id');
    // }
}
