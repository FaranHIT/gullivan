<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    protected $table = 'user_notifications';
    protected $fillable = ['user_id','title','message','countries','nationalities','languages','users'];
    protected $casts = [
        'user_id' => 'array' ,
        'countries' => 'array',
        'nationalities'=>'array',
        'languages'=>'array',
        'users'=>'array'
    ];
}
