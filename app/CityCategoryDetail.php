<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityCategoryDetail extends Model
{
    protected $fillable = ['category_id','picture','title','details','address','website','number','moday_time','tuesday_time','wednesday_time','thursday_time','friday_time','saturday_time','sunday_time','city_id'];

    public function cityCategory()
    {
        return $this->belongsTo(CityCategory::class);
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
