<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryData extends Model
{
    protected $fillable = [
        'country_id','constitution','emergency_no','embassies','new_offer','rules','history',
        'driving','universities','visa_uni_acceptance_fee','tours','youtube_link','best_time_to_go',
        'transportation','weather','information','the_electric','language','currency'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
