<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPasswordMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $user_id;
    protected $token;
    protected $token_id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_id,$token,$token_id)
    {
        $this->user_id = $user_id;
        $this->token = $token;
        $this->token_id = $token_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user_id = $this->user_id;
        $token = $this->token;
        $token_id = $this->token_id;
        return $this->view('reset-pass-email',compact('user_id','token','token_id'));
    }
}
