<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FullCalendar extends Model
{
    protected $fillable=['title','start','end','user_id',];
}
