<?php
// Place this file on the Providers folder of your project
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;

class ResponseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('success', function ($message, $data = '', $status = 200) {
            $response =[
                'message' => $message,
                'status'  => '1',
                'result'  => boolval(true),
                'data'    => $data,
            ];
            return Response::json($response,$status);
        });

        Response::macro('error', function (string $message = '',$status = 404){
            return Response::json([
                'message' => $message,
                'status'  => '2',
                'result'  => boolval(false),
            ],$status);
        });
        Response::macro('errors', function (string $message = '',$status = 401){
            return Response::json([
                'message' => $message,
                'result'  => boolval(false),
            ],$status);
        });
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}