<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
	protected $table = 'wp_usermeta';
	protected $primaryKey = 'umeta_id';
	public $timestamps = false;
	protected $fillable = ['user_id', 'meta_key', 'meta_value'];
	public function getCountryAttribute()
	{
		return $this->meta_value;
	}
	public function getStatusAttribute()
	{
		return $this->meta_value;
	}
	public function getPrivateStatusAttribute()
	{
		return $this->meta_value;
	}
	public function getCityAttribute()
	{
		return $this->meta_value;
	}
	// public function getCoverPhotoAttribute()
	// {
	// 	return $this->meta_value;
	// }
	public function user()
    {
        return $this->belongsTo(User::class);
    }
}
