<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryMobile extends Model
{
    protected $table = 'country_mobiles';
}
