<?php

namespace App;

use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Support\Facades\Storage;
//use Laravel\Scout\Searchable;


class User extends Model implements AuthenticatableContract
{
    use HasApiTokens, Notifiable, AuthenticableTrait,LaratrustUserTrait;

    protected $table = 'wp_users';
    protected $primaryKey = 'ID';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_nicename', 'profile_picture','first_name','last_name','user_email', 'user_pass','c_password','phone', 'nationality','language','device_token','device_type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_pass', 'remember_token', 'profile_picture',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['country','status','private_status','city','cover_photo','profile_photo'];


        /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'users_index';
    }
       /**
     * Relationship with users.
     *
     */
    public function friendRequests()
    {
        return $this->hasMany(FriendRequest::class, 'recipient_id', 'ID');
    }

	public function senderStatus()
    {
        return $this->hasMany(FriendRequest::class, 'sender_id','ID');
    }
    public function recipientStatus()
    {
        return $this->hasMany(FriendRequest::class, 'recipient_id','ID');
    }


    public function friendShipSenders()
    {
        return $this->hasMany(FriendShip::class,'sender_id');
    }
    public function friendShipRecipients()
    {
        return $this->hasMany(FriendShip::class,'recipient_id');
    }

    public function userMeta()
    {
        return $this->hasMany(UserMeta::class, 'user_id', 'ID');
    }

    public static function getToken($userId)
    {
        $userMeta = UserMeta::where([ 'meta_key' => "fcm_device_token", 'user_id' => $userId ])->first();
        if (!is_null($userMeta['meta_value'])) {
            return $userMeta['meta_value'];
        }
        return false;
    }
    public function getCountry()
    {
        return $this->hasOne(UserMeta::class, 'user_id', 'ID')->where('wp_usermeta.meta_key','country');
    }
    public function getCountryAttribute()
    {
        return $this->getCountry->meta_value ?? null;
    }
    public function getstatus()
    {
        return $this->hasOne(UserMeta::class, 'user_id', 'ID')->where('wp_usermeta.meta_key','status');
    }
    public function getStatusAttribute()
    {
        return $this->getstatus->meta_value ?? null;
    }
    public function getPrivateStatus()
    {
        return $this->hasOne(UserMeta::class, 'user_id', 'ID')->where('wp_usermeta.meta_key','private_status');
    }
    public function getPrivateStatusAttribute()
    {
        return $this->getPrivateStatus->meta_value ?? null;
    }
    public function getCity()
    {
        return $this->hasOne(UserMeta::class, 'user_id', 'ID')->where('wp_usermeta.meta_key','city');
    }
    public function getCityAttribute()
    {
        return $this->getCity->meta_value ?? null;
    }
    // public function getCoverPhoto()
    // {
    //     return $this->hasOne(UserMeta::class, 'user_id', 'ID')->where('wp_usermeta.meta_key','cover_photo');
    // }
    public function getCoverPhotoAttribute()
    {
       $UserId = $this->ID;
       $base64_string = UserMeta::where('user_id',$UserId)->where('meta_key','cover_photo')->pluck('meta_value')->first();
       $CoverExists = Storage::disk('public')->exists('cover'.'-'.$UserId.'.'.'png');

        if($CoverExists == true)
        {
            return 'storage/'.'cover'.'-'.$UserId.'.'.'png';
        }
        else
        {
            if(!is_null($base64_string))
            {      
                $imageName = 'cover'.'-'.$UserId.'.'.'png';   
                Storage::disk('public')->put($imageName, base64_decode($base64_string));
                return  'storage/'.'cover'.'-'.$UserId.'.'.'png';
            }
        }
    }
    public function getProfilePhotoAttribute()
    {
        $base64_string = $this->profile_picture;
        $UserId        = $this->ID;
        $ProfileExists = Storage::disk('public')->exists('profile'.'-'.$UserId.'.'.'png'); 
        if($ProfileExists == true)
        {
            return 'storage/'.'profile'.'-'.$UserId.'.'.'png';
        }
        else
        {
            if(!is_null($base64_string))
            {      
                $imageName = 'profile'.'-'.$UserId.'.'.'png';   
                Storage::disk('public')->put($imageName, base64_decode($base64_string));
                return  'storage/'.'profile'.'-'.$UserId.'.'.'png';
            }
        } 
    }
}
