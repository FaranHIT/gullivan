<?php

namespace App\Traits;
use Image;

trait ImageuploadTrait 
{
    public function imagePhoto($request)
    {
        if ( ! $request->hasFile($this->profile_picture)) {
            return false;
        }
        $avatar = $request->file($this->profile_picture);
        $filename = time(). '.'.$avatar->getClientOriginalExtension();
        Image::make($avatar)->resize(300,300)->save(public_path('upload/'.$filename));
        $file_path = '/upload/'.$filename;
        return $file_path;
    }
}