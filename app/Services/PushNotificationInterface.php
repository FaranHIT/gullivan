<?php

namespace App\Services;

interface PushNotificationInterface
{
    public function sendNotification($token, $title, $body, array $arguments);
}