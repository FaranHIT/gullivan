<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friendship extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sender_id', 'recipient_id', 'status',
    ];
     /**
     * Relationship with users.
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
