<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityData extends Model
{
    protected $fillable = [
      'city_id','rent_a_car','real_estate','hotels','coffee_shops','restaurants','bars','add_ur_bussiness',
      'taxis','road_laws','lawyers','events','tours','youtube','best_time_to_go','transportation',
      'weather','information'
    ];
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
