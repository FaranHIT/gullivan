@extends('layout.adminpanel.default')
@section('content')
<section role="main" class="content-body">
	<header class="page-header">
		<h2>View Notifications</h2>

		<div class="right-wrapper text-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.html">
						<i class="fas fa-home"></i>
					</a>
				</li>
				<li><span>Table</span></li>
				<li><span>View Notifications</span></li>
			</ol>

			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
		</div>
	</header>

	<!-- start: page -->
	<section class="card">
			<header class="card-header">
				<div class="card-actions">
					<a href="#" class="card-action card-action-toggle" data-card-toggle></a>
					<a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
				</div>

				<h2 class="card-title">View Notifications</h2>
			</header>
			<div class="card-body">
            @if(session('success-msg'))
                    <div class="alert alert-success">
                        {{session('success-msg')}}
                    </div>
                @elseif(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                @elseif(session('alert-msg'))
                    <div class="alert alert-info">
                        {{session('alert-msg')}}
                    </div>
                @endif
			<table class="table table-bordered table-striped table-responsive view-notifications" id="view-notifications">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <!-- <th>User ID</th> -->
                        <th>Title</th>
                        <th>Message</th>
                        <th>Countries</th>
                        <th>Nationalities</th>
                        <th>Languages</th>
                        <th>Users</th>
                        <th>Created-At</th>
                        <!-- <th>Updated-At</th> -->
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

                <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5><b>Notifications Send To</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    </div> 
                    <div class="modal-body" id="project-content">
                    ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </div>
                </div>

			</div>
		</section>
	<!-- end: page -->
</section>
@push('scripts')
<script src="{!! asset('vendor/datatables/media/js/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/media/js/dataTables.bootstrap4.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js') !!}"></script>
<script type="text/javascript">
$(document).ready(function() {
                $("#view-notifications").DataTable({
                    processing: true,
                    serverSide: true,
                    "ajax": {
                        url: "{{route('view-notifications')}}"
                    },
                    order: [ 
                        [0, 'desc']
                           ],

                    "columns":[
                        {data: "id", name:"id"},
                        // {data: "user_id", name:"user_id"},
                        {data: "title", name:"title"},
                        {data: "message", name:"message"},
                        {data: "Countries", name:"Countries"},
                       //  {data: "countries", name:"countries"},
                        {data: "Nationalities", name:"Nationalities"},
                       //  {data: "nationalities", name:"nationalities"},
                        {data: "Languages", name:"Languages"},
                        // {data: "languages", name:"languages"},
                        {data: "Users", name:"Users"},
                        // {data: "users", name:"users"},
                        {data: "created_at", name:"created_at"},
                        // {data: "updated_at", name:"updated_at"},
                        {data:"action",name:"action",orderable:false},
                    ]
                });
            });
        
            function CountryClick(obj,e)
            {
                var cid = $(obj).attr("href");
                e.preventDefault();
                $.ajax
                ({
                    url:"{{route('more-countries')}}"+"/"+cid,
                    //url:'/public/more-countries/'+cid,
                    type: 'GET',
                    success: function(response)
                    {
                        var x=response;
                        $('#project-content').html(x);                        
                    }
                });
            }

            function NationalityClick(obj,e)
            {
                var nid = $(obj).attr("href");
                e.preventDefault();
                $.ajax
                ({
                    url:"{{route('more-nationalities')}}"+"/"+nid,
                    //url:'/public/more-nationalities/'+nid,
                    type: 'GET',
                    success: function(response)
                    {
                        var x=response;
                        $('#project-content').html(x);	                        
                    }
                });
            }

            function LanguageClick(obj,e)
            {
                var lid = $(obj).attr("href");
                e.preventDefault();
                $.ajax
                ({
                    url:"{{route('more-languages')}}"+"/"+lid,
                    //url:'/public/more-languages/'+lid,
                    type: 'GET',
                    success: function(response)
                    {
                        var x=response;
                        $('#project-content').html(x);	                        
                    }
                });
            }

            function UsersClick(obj,e)
            {
                var uid = $(obj).attr("href");
                e.preventDefault();
                $.ajax
                ({
                    url:"{{route('more-users')}}"+"/"+uid,
                    //url:'/public/more-users/'+uid,
                    type: 'GET',
                    success: function(response)
                    {
                        var x=response;
                        $('#project-content').html(x);	                        
                    }
                });
            }

</script>
@endpush
@endsection
