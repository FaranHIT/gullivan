@extends('layout.adminpanel.default')
@section('content')
    <section role="main" class="content-body">
        <header class="page-header">
            <h2>Send Notifications</h2>

            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="index.html">
                            <i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li><span>Tables</span></li>
                    <li><span>Notifications</span></li>
                </ol>

                <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
            </div>
        </header>

        <!-- start: page -->

        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>
                <b>Send Notifications</b>
            </header>
            <div class="card-body">
                @if(session('success-msg'))
                    <div class="alert alert-success">
                        {{session('success-msg')}}
                    </div>
                @elseif(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                @elseif(session('alert-msg'))
                    <div class="alert alert-info">
                        {{session('alert-msg')}}
                    </div>
                @endif
                <form method="post" action="{{route('send-notifications')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row justify-content-around">
                        <div class="form-group col-md-12 ">
                        <div>
                            <select class="form-control" multiple="multiple" id="select-countries"  name="countries[]">
                                @if(count($countries) > 0)
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}">{{$country->country_name_en}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <input id="countries" type="checkbox" >Select All Countries
                        </div>
                        <br/>
                        <div>    
                            <select class="form-control" multiple="multiple" id="select-nationalities" name="nationalities[]">
                                @if(count($nationalities) > 0)
                                    @foreach($nationalities as $nationality)
                                        <option value="{{$nationality->id}}">{{$nationality->country_name_en}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <input id="nationalities" type="checkbox" >Select All Nationalities
                         </div>
                         <br/>
                         <div>   
                            <select class="form-control" multiple="multiple" id="select-languages" name="languages[]">
                                @if(count($languages) > 0)
                                    @foreach($languages as $language)
                                        <option value="{{$language}}">{{$language}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <input id="languages" type="checkbox" >Select All Languages
                        </div>

                        <div>
                        </br>
                        <select class="form-control" multiple="multiple" id="users" name="users[]">
                        @if(count($users) > 0)
                        @foreach($users as $user)
                            <option value="{{$user->ID}}">{{$user->user_nicename}}</option>
                        @endforeach
                        @endif
                        </select>
                        <input id="select-all" type="checkbox">Select All Users 
                        </div>         
                    </div>
                        <div class="form-group col-md-10 ">
                        <label>Title</label>
                        <input type='text' class="form-control form-control-lg"  id="title" name="title" />
                        @if($errors->has('title'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('title') }}</span>
                        @endif
                        </div>

                        <div class="form-group col-md-10 ">
                        <label>Messsage</label>
                        <textarea class="form-control form-control-lg" id="message" name="message" rows="3"></textarea>
                        @if($errors->has('message'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('message') }}</span>
                        @endif
                        </div>
                    
                        <div class="form-group col-md-4 ">
                            <button type="submit" class="btn btn-primary btn-sm btn-block">Send</button>
                        </div>                      
                </form>
                </div>

        </section>
        <!-- end: page -->
        </section>
        @push('scripts')
        <script type="text/javascript">
	    $(document).ready(function() {
        $('#select-languages').select2({ placeholder: "Select Languages"});
        $('#select-nationalities').select2({placeholder: "Select Nationalities"});
        $('#select-countries').select2({placeholder: "Select Countries"});
        $('#users').select2({placeholder:"Select Users"});
        
        $("#languages").click(function(){
        if($("#languages").is(':checked')){
            $("#select-languages > option").prop("selected",true );
            $("#select-languages").trigger("change");
        } else {
            $("#select-languages > option").prop("selected",false);
            $("#select-languages").trigger("change");
        }
        });

        $("#nationalities").click(function(){
        if($("#nationalities").is(':checked')){
            $("#select-nationalities > option").prop("selected",true );
            $("#select-nationalities").trigger("change");
        } else {
            $("#select-nationalities > option").prop("selected",false);
            $("#select-nationalities").trigger("change");
        }
        });

        $("#countries").click(function(){
        if($("#countries").is(':checked')){
            $("#select-countries > option").prop("selected",true );
            $("#select-countries").trigger("change");
        } else {
            $("#select-countries > option").prop("selected",false);
            $("#select-countries").trigger("change");
        }
        });

        $("#select-all").click(function(){
        if($("#select-all").is(':checked')){
            $("#users> option").prop("selected",true );
            $("#users").trigger("change");
        } else {
            $("#users > option").prop("selected",false);
            $("#users").trigger("change");
        }
        });

    });
        </script>
        @endpush

@endsection