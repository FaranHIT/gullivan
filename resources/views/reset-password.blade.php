
<!doctype html>
<html class="fixed">
<head>

    <!-- Basic -->
    <meta charset="UTF-8">
    <title>
        {{ config('app.name', 'WorldGuide') }} | Reset Password
    </title>
    <meta name="keywords" content="HTML5 Admin Template" />
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,800,900|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{!! asset('vendor/bootstrap/css/bootstrap.css') !!}" />
    <link rel="stylesheet" href="{!! asset('vendor/animate/animate.css') !!}">

    <link rel="stylesheet" href="{!! asset('vendor/font-awesome/css/all.min.css') !!}" />
    <link rel="stylesheet" href="{!! asset('vendor/magnific-popup/magnific-popup.css') !!}" />
    <link rel="stylesheet" href="{!! asset('vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css') !!}" />

    <!--(remove-empty-lines-end)-->

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{!! asset('css/adminpanel/theme.css') !!}" />


    <!--(remove-empty-lines-end)-->



    <!-- Skin CSS -->
    <link rel="stylesheet" href="{!! asset('css/adminpanel/skins/default.css') !!}" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{!! asset('css/adminpanel/custom.css') !!}">

    <!-- Head Libs -->
    <script src="{!! asset('vendor/modernizr/modernizr.js') !!}"></script>

</head>
<body>
<!-- start: page -->
<section class="body-sign">
    <div class="center-sign">


        <div class="panel card-sign">
            <div class="card-title-sign mt-3 text-right">
                <h2 class="title text-uppercase font-weight-bold m-0"><i class="fas fa-user mr-1"></i> Reset Password</h2>
            </div>
            <div class="card-body">
                @if(session()->has('success-msg'))
                <div class="alert alert-success alert-dismissible fade show">
                    {{session('success-msg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @elseif(session()->has('alert-msg'))
                    <div class="alert alert-info alert-dismissible fade show">
                        {{session('alert-msg')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form action="{{route('resetPassword')}}" method="post">
                    {{ csrf_field() }}

                    <input type="hidden" name="user_id" value="{{$user_id ?? ''}}">
                    <input type="hidden" name="token_id" value="{{$token_id ?? ''}}">
                    <div class="form-group mb-3">
                        <div class="clearfix">
                            <label class="float-left">New Password</label>
                        </div>
                        <div class="input-group">
                            <input name="new_password" id="password" type="password" class="form-control form-control-lg" />
                            <span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-lock"></i>
										</span>
									</span>
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <div class="clearfix">
                            <label class="float-left">Confirm Password</label>
                        </div>
                        <div class="input-group">
                            <input name="confirm_password" id="password" type="password" class="form-control form-control-lg" />
                            <span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-lock"></i>
										</span>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 text-center">
                            <button type="submit" class="btn btn-primary mt-2">Reset</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <p class="text-center text-muted mt-3 mb-3">&copy; Copyright 2017. All Rights Reserved.</p>
    </div>
</section>
<!-- end: page -->

<!-- Vendor -->
<script src="{!! asset('vendor/jquery/jquery.js') !!}"></script>
<script src="{!! asset('vendor/jquery-browser-mobile/jquery.browser.mobile.js') !!}"></script>
<script src="{!! asset('vendor/popper/umd/popper.min.js') !!}"></script>
<script src="{!! asset('vendor/bootstrap/js/bootstrap.js') !!}"></script>
<script src="{!! asset('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}"></script>
<script src="{!! asset('vendor/common/common.js') !!}"></script>
<script src="{!! asset('vendor/nanoscroller/nanoscroller.js') !!}"></script>
<script src="{!! asset('vendor/magnific-popup/jquery.magnific-popup.js') !!}"></script>
<script src="{!! asset('vendor/jquery-placeholder/jquery.placeholder.js') !!}"></script>

<!-- Specific Page Vendor -->


<!--(remove-empty-lines-end)-->

<!-- Theme Base, Components and Settings -->
<script src="{!! asset('js/adminpanel/theme.js') !!}"></script>

<!-- Theme Custom -->
<script src="{!! asset('js/adminpanel/custom.js') !!}"></script>

<!-- Theme Initialization Files -->
<script src="{!! asset('js/adminpanel/theme.init.js') !!}"></script>

</body>
</html>
