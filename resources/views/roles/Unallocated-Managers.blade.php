@extends('layout.adminpanel.default')
@section('content')
<section role="main" class="content-body">
	<header class="page-header">
		<h2>View Unallocated Managers</h2>
	
		<div class="right-wrapper text-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.html">
						<i class="fas fa-home"></i>
					</a>
				</li>
				<li><span>Table</span></li>
				<li><span>View Unallocated Managers</span></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
		</div>
	</header>

	<!-- start: page -->
	<section class="card">
			<header class="card-header">
				<div class="card-actions">
					<a href="#" class="card-action card-action-toggle" data-card-toggle></a>
					<a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
				</div>
		
				<h2 class="card-title">Unallocated Managers</h2>
			</header>
			<div class="card-body">
            @if(session('success-msg'))
                    <div class="alert alert-success">
                        {{session('success-msg')}}
                    </div>
                @elseif(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                @elseif(session('alert-msg'))
                    <div class="alert alert-info">
                        {{session('alert-msg')}}
                    </div>
                @endif

                <div class="form-row">
<div class="form-group col-md-3 pt-2">
<select class="form-control" id="country" name="Country">
								<option disabled selected>Select Country</option>
                                @if(count($countries) > 0)
                                    @foreach($countries as $country)
                                        <option  value="{{$country->name}}">{{$country->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <button type="button" class="btn btn-info btn-block btn-sm" id="filter-country">Filter Country</button>
</div>

<div class="form-group col-md-3 pt-2">
<select class="form-control" id="city" name="City">
								<option disabled selected>Select City</option>
                                @if(count($cities) > 0)
                                    @foreach($cities as $city)
                                        <option class="c_id" value="{{str_replace(' ','',$city->name)}}">{{$city->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <button type="button" class="btn btn-info btn-block btn-sm" id="filter-city">Filter City</button>
</div>
<div class="form-group col-md-2 pt-0">
<button type="button" class="btn btn-danger btn-block btn-sm mt-5" name="reset" id="reset">Reset All</button>
</div>

<div class="form-group col-md-5 pt-5">
</div>

</div>
	<table class="table table-bordered table-striped assign-remove-permissions" id="assign-remove-permissions">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>User Name</th>
                        <th>Countries Permissions</th>
                        <th>Cities Permissions</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

                <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5><b>Permissions</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    </div> 
                    <div class="modal-body" id="project-content">
                    ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </div>
                </div>

				<div class="modal fade bd-example-modal-l" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5><b>Admins</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    </div> 
                    <div class="modal-body">
                    <form>
					<input type="hidden" name="id" id="managerID" value="">
					<label class="col-form-label">Select Admin:</label>
					<select id="admin-fill"  name="admins" class="form-control">                      
                	
					</select>
					</form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="button" id="allocate" class="btn btn-secondary" data-dismiss="modal">Allocate</button>
                    </div>
                </div>
                </div>
                </div>	

			</div>
		</section>		
	<!-- end: page -->
</section>
@push('scripts')
<script src="{!! asset('vendor/datatables/media/js/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js') !!}"></script>
<script type="text/javascript">
$(document).ready(function() {
    fill_datatable();
    function fill_datatable(country = '',city = '')
    {
        $("#assign-remove-permissions").DataTable({
                    processing: true,
                    serverSide: true,
                    "ajax": {
                        url: "{{route('unallocated-managers')}}",
                        data:{country:country,city:city}
                    },
                    "columns":[
                        {data: "ID", name:"ID"},
                        {data: "user_nicename", name:"user_nicename"},
                        {data: "CountryPermissions",name:"CountryPermissions",orderable:false},
                        {data: "CityPermissions",name:"CityPermissions",orderable:false},
                        {data: "action",name:"action",orderable:false},
                    ]
                });
    }
 
    $('#filter-country').click(function(){
        
        var selectedCountry = $('#country').val();
        $("#assign-remove-permissions").DataTable().destroy();
        fill_datatable(selectedCountry);
        });

    $('#filter-city').click(function(){
        var selectedCity = $('#city').val();
        $("#assign-remove-permissions").DataTable().destroy();
        fill_datatable(selectedCity);
        });        
            
    $('#reset').click(function(){
        var Country = $('#country').val('');
        var City = $('#city').val('');
        $("#assign-remove-permissions").DataTable().destroy();
        fill_datatable();
        });

	$('#allocate').click(function(){
		var managerID = $('#managerID').val();
		var AdminID = $('#admin-fill').val();
		$.ajax({
		url: "{{route('allocateAdmin')}}"+"/"+managerID+"/"+AdminID,
		type: "GET",
		success: function(response)
			{
				var x= response;
				if(x== "successfully allocate Admin")
				{
					toastr.info(x);
					$('#assign-remove-permissions').DataTable().ajax.reload();
				}
			},
		});
	});

    });

            function CountryPermissonsClick(obj,e)
            {
                var id = $(obj).attr("href");
                e.preventDefault();
                $.ajax
                ({
                    url:"{{route('view-country-permissions')}}"+"/"+id,
                    //url:'/public/view-country-permissions/'+id,
                    type: 'GET',
                    success: function(response)
                    {
                        var x=response;
                        $('#project-content').html(x);                        
                    }
                });
            }

            function CityPermissonsClick(obj,e)
            {
                var id = $(obj).attr("href");
                e.preventDefault();
                $.ajax
                ({
                    url:"{{route('view-city-permissions')}}"+"/"+id,
                    //url:'/public/view-city-permissions/'+id,
                    type: 'GET',
                    success: function(response)
                    {
                        var x=response;
                        $('#project-content').html(x);                        
                    }
                });
            }

			function AdminModal(obj,e)
			{
				var managerID = $(obj).attr("href");
				e.preventDefault();
				$.ajax({
					url: "{{route('admins')}}",
					type:"GET",
					success: function(response)
					{
						$('#admin-fill').empty();
						$('#managerID').val(managerID);
						var admins = response;
						$.each(admins , function(i,value){
							$('#admin-fill').append('<option value='+value.ID+'>'+value.user_nicename+'</option>');
						});	
					}
				});
			}

			
            
</script>
@endpush
@endsection