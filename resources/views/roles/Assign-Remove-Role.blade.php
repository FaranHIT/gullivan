@extends('layout.adminpanel.default')
@section('content')
<section role="main" class="content-body">
	<header class="page-header">
		<h2>Assign & Remove Managers</h2>
	
		<div class="right-wrapper text-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.html">
						<i class="fas fa-home"></i>
					</a>
				</li>
				<li><span>Table</span></li>
				<li><span>Assign & Remove Managers</span></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
		</div>
	</header>

	<!-- start: page -->
	<section class="card">
			<header class="card-header">
				<div class="card-actions">
					<a href="#" class="card-action card-action-toggle" data-card-toggle></a>
					<a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
				</div>
		
				<h2 class="card-title">Users</h2>
			</header>
			<div class="card-body">
            @if(session('success-msg'))
                    <div class="alert alert-success">
                        {{session('success-msg')}}
                    </div>
                @elseif(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                @elseif(session('alert-msg'))
                    <div class="alert alert-info">
                        {{session('alert-msg')}}
                    </div>
                @endif
			<table class="table table-bordered table-striped assign-remove-role" id="assign-remove-role">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>User Name</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <!-- <th>Email</th> -->
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
			</div>
		</section>		
	<!-- end: page -->
</section>
@push('scripts')
<script src="{!! asset('vendor/datatables/media/js/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js') !!}"></script>
<script type="text/javascript">
$(document).ready(function() {
              $("#assign-remove-role").DataTable({
                    processing: true,
                    serverSide: true,
                    "ajax": {
                        url: "{{route('assign-remove-role')}}"
                    },
                    "columns":[
                        {data: "ID", name:"ID"},
                        {data: "user_nicename", name:"user_nicename"},
                        {data: "first_name", name:"first_name"},
                        {data: "last_name", name:"last_name"},
                        // {data: "user_email", name:"user_email"},
                        {data: "action",name:"action",orderable:false},
                    ]
                });
            });
            function AssignManager(obj,e)
            {
                var uid = $(obj).attr("href");
                e.preventDefault();
                $.ajax
                ({
                    url: "{{route('assign-role')}}"+"/"+uid,
                    //url: '/latest/gullivan/public/assign-role/'+uid,
                    type: 'GET',
                    success: function(response)
                    {
                        var x = response;
                        if(x == "successfully assign Manager role")
                        {
                            toastr.info(x);
                            $('#assign-remove-role').DataTable().ajax.reload();
                        }
                        else
                        {
                            toastr.info(x);
                        }
                    }
                });
            }
            function RemoveManager(obj,e)
            {
                var uid = $(obj).attr("href");
                e.preventDefault();
                $.ajax
                ({
                    url: "{{route('remove-role')}}"+"/"+uid,
                    //url: '/latest/gullivan/public/remove-role/'+uid,
                    type: 'GET',
                    success: function(response)
                    {
                        var x = response;
                        if(x == "successfully remove Manager role")
                        {
                            toastr.info(x);
                            $('#assign-remove-role').DataTable().ajax.reload();
                        }
                        else
                        {
                            toastr.info(x);
                        }
                    }
                });
            }   
</script>
@endpush
@endsection