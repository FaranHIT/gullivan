<div class="container bg-light">
    <div class="row justify-content-around">
        <div class="col-md-8">
            <h3>Hello!</h3>
            <p>
                You are receiving this email because we received a password reset request for your account.
            </p>
        </div>
        <div class="col-md-8 text-center">
            <a href="{{route('resetPasswordForm',[$user_id,$token,$token_id])}}" class="btn btn-sm btn-primary">Reset Password</a><br>
            <small>Please Click On Above Link To Reset Your Password</small>
        </div>
        <div class="col-md-8 text-center">
            <h4>Thank You</h4>
            <h4>Regards: WorldGuide</h4>
        </div>
    </div>
</div>
