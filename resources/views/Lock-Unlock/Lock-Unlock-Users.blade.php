@extends('layout.adminpanel.default')
@section('content')
<section role="main" class="content-body">
	<header class="page-header">
		<h2>Users Lock Unlock</h2>
	
		<div class="right-wrapper text-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.html">
						<i class="fas fa-home"></i>
					</a>
				</li>
				<li><span>Table</span></li>
				<li><span>Users Lock Unlock</span></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
		</div>
	</header>

	<!-- start: page -->
	<section class="card">
			<header class="card-header">
				<div class="card-actions">
					<a href="#" class="card-action card-action-toggle" data-card-toggle></a>
					<a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
				</div>
		
				<h2 class="card-title">Users</h2>
			</header>
			<div class="card-body">
            @if(session('success-msg'))
                    <div class="alert alert-success">
                        {{session('success-msg')}}
                    </div>
                @elseif(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                @elseif(session('alert-msg'))
                    <div class="alert alert-info">
                        {{session('alert-msg')}}
                    </div>
                @endif
			<table class="table table-bordered table-striped lock-unlock-users" id="users-lock-unlock">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>User Name</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <!-- <th>Email</th> -->
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
			</div>
		</section>		
	<!-- end: page -->
</section>
@push('scripts')
<script src="{!! asset('vendor/datatables/media/js/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js') !!}"></script>
<script type="text/javascript">
$(document).ready(function() {
                $("#users-lock-unlock").DataTable({
                    processing: true,
                    serverSide: true,
                    "ajax": {
                        url: "{{route('users-lock-unlock')}}"
                    },
                    "columns":[
                        {data: "ID", name:"ID"},
                        {data: "user_nicename", name:"user_nicename"},
                        {data: "first_name", name:"first_name"},
                        {data: "last_name", name:"last_name"},
                        // {data: "user_email", name:"user_email"},
                        {data: "action",name:"action",orderable:false},
                    ]
                });
            });
            function UserLock(obj,e)
            {
                var uid = $(obj).attr("href");
                e.preventDefault();
                $.ajax
                ({
                    url:"{{route('lock-user')}}"+"/"+uid,
                    // url: '/public/lock-user/'+uid,
                    type: 'GET',
                    success: function(response)
                    {
                        var x = response;
                        if(x == "successfully locked this user")
                        {
                            toastr.info(x);
                            $('#users-lock-unlock').DataTable().ajax.reload();
                        }
                        else
                        {
                            toastr.info(x);
                        }
                    }
                });
            }
            function UserUnLock(obj,e)
            {
                var uid = $(obj).attr("href");
                e.preventDefault();
                $.ajax
                ({
                    url:"{{route('unlock-user')}}"+"/"+uid,
                    //url: '/public/unlock-user/'+uid,
                    type: 'GET',
                    success: function(response)
                    {
                        var x = response;
                        if(x == "successfully unlocked this user")
                        {
                            toastr.info(x);
                            $('#users-lock-unlock').DataTable().ajax.reload();
                        }
                        else
                        {
                            toastr.info(x);
                        }
                    }
                });
            }   
</script>
@endpush
@endsection