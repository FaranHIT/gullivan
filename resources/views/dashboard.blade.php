@extends('layout.adminpanel.default')
@section('content')
<section role="main" class="content-body">
	<header class="page-header">
		<h2>Default Layout</h2>
	
		<div class="right-wrapper text-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.html">
						<i class="fas fa-home"></i>
					</a>
				</li>
				<li><span>Layouts</span></li>
				<li><span>Default</span></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
		</div>
	</header>

	<!-- start: page -->
	@role(['superadmin','admin'])
	<div class="row m-0" style=" background: white;">
					@role('superadmin')
                    <div class="col-sm-4">
                        <div class="core-box">
                            <div class="heading">
							
                                <h2><div class="widget-summary">
								<div class="widget-summary-col widget-summary-col-icon">
									<div class="summary-icon bg-primary" style="width:50px;height:50px; position:relative">
										<i  class="fas fa-user-tag" style="position:absolute;font-size: 30px;top: 10px;right: 5px;"></i>
									</div>
								</div> <a href="{{route('assign-remove-admin')}}">Manage Admin</a></h2>
                            </div>
                            <div class="content">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                            </div>
                            <a class="view-more" href="#">
                                View More <i class="clip-arrow-right-2"></i>
                            </a>
                        </div>
                    </div>
					@endrole				
                    <div class="col-sm-4">
                        <div class="core-box">
                            <div class="heading">
							<h2><div class="widget-summary">
								<div class="widget-summary-col widget-summary-col-icon">
									<div class="summary-icon bg-primary" style="width:50px;height:50px;position:relative">
										<i class="fas fa-user-tag " style="position:absolute;font-size: 30px;top: 10px;right: 5px;"></i>
									</div>
								</div> <a href="{{route('assign-remove-role')}}">Manage Manager</a></h2>
                            </div>
                            <div class="content">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                            </div>
                            <a class="view-more" href="#">
                                View More <i class="clip-arrow-right-2"></i>
                            </a>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="core-box">
                            <div class="heading">
							<h2><div class="widget-summary">
								<div class="widget-summary-col widget-summary-col-icon">
									<div class="summary-icon bg-secondary" style="width:50px;height:50px; position:relative">
										<i class="fas fa-user-lock " style="position:absolute;font-size: 29px;top: 10px;right: 5px;"></i>
									</div>
								</div> <a href="{{route('users-lock-unlock')}}">Manage Lock/Unlock</a></h2>
                            </div>
                            <div class="content">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                            </div>
                            <a class="view-more" href="#">
                                View More <i class="clip-arrow-right-2"></i>
                            </a>
                        </div>
                    </div>
                </div>
	@endrole
				
	@role('superadmin')
	<div class="row">
		
			<div class="col-lg-6">
			<section class="card">
			<header class="card-header">
				<div class="card-actions">
					<a href="#" class="card-action card-action-toggle" data-card-toggle></a>
					<a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
				</div>

				<h2 class="card-title">Users</h2>
			</header>
			<div class="card-body">
			<table class="table table-bordered table-striped view-users" id="view-users">
                    <thead>
                    <tr>
                    	<th>ID</th>
                    	<th>User Name</th>
                    	<th>Email</th>
                    	</tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
			</div>
		</section>	
			</div>
		
			
			<div class="col-lg-6">

				<div class="row">
					<div class="col-lg-6">
					<section class="card card-featured-left card-featured-primary mb-3">
						<div class="card-body">
							<div class="widget-summary">
								<div class="widget-summary-col widget-summary-col-icon">
									<div class="summary-icon bg-primary">
										<i class="fas fa-users"></i>
									</div>
								</div>
								<div class="widget-summary-col">
									<div class="summary">
										<h4 class="title">Total Users</h4>
										<div class="info">
											<strong class="amount">{{$TotalUsers}}</strong>
											<!-- <span class="text-primary">(14 unread)</span> -->
										</div>
									</div>
									<div class="summary-footer">
										<!-- <a class="text-muted text-uppercase" href="{{ url('users') }}">(view all)</a> -->
									</div>
								</div>
							</div>
						</div>
					</section>
					</div>
					
					<div class="col-lg-6">
					<section class="card card-featured-left card-featured-quaternary">
						<div class="card-body">
							<div class="widget-summary">
								<div class="widget-summary-col widget-summary-col-icon">
									<div class="summary-icon bg-quaternary">
										<i class="fab fa-apple"></i>
									</div>
								</div>
								<div class="widget-summary-col">
									<div class="summary">
										<h4 class="title">IOS Users</h4>
										<div class="info">
											<strong class="amount">{{$TotalIosUsers}}</strong>
										</div>
									</div>
									<div class="summary-footer">
										<!-- <a class="text-muted text-uppercase" href="#">(withdraw)</a> -->
									</div>
								</div>
							</div>
						</div>
					</section>
					</div>
				</div>

				<div class= "row">
					<div class="col-lg-6">
					<section class="card card-featured-left card-featured-tertiary mb-3">
						<div class="card-body">
							<div class="widget-summary">
								<div class="widget-summary-col widget-summary-col-icon">
									<div class="summary-icon bg-tertiary">
										<i class="fab fa-android"></i>
									</div>
								</div>
								<div class="widget-summary-col">
									<div class="summary">
										<h4 class="title">Android Users</h4>
										<div class="info">
											<strong class="amount">{{$TotalAndroidUsers}}</strong>
										</div>
									</div>
									<div class="summary-footer">
										<!-- <a class="text-muted text-uppercase" href="#">(statement)</a> -->
									</div>
								</div>
							</div>
						</div>
					</section>	
					</div>

					<div class="col-lg-6">
					</div>
				</div>

			</div>
		
		</div>
	@endrole

	<div class="row">
		<div class="col-lg-7">
		<section class="card">
				<header class="card-header">
					<div class="card-actions">
						<a href="#" class="card-action card-action-toggle" data-card-toggle></a>
						<a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
					</div>
					<h2 class="card-title">Calendar</h2>
				</header>
				<div class="card-body">
	
					<!-- Flot: Curves -->
					<div class="response"></div>
					<div id="calendar"></div>
	
				</div>
				
<script type="text/javascript">
$(document).ready(function () {
	var SITEURL = "{{url('')}}";
	$.ajaxSetup({
	headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			 }
	});
	var calendar = $('#calendar').fullCalendar({
	editable: true,
 	events: SITEURL + "/fullcalendar",
	// events: "{{route('index')}}",
	displayEventTime: true,
	editable: true,
	eventRender: function (event, element, view) {
	if (event.allDay === 'true') 
	{
		event.allDay = true;
	} 
	else 
	{
		event.allDay = false;
	}
	},
	selectable: true,
	selectHelper: true,
	select: function (start, end, allDay) {
	var title = prompt('Event Title:');
	if (title) 
	{
		var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
		var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
	$.ajax({
 	url: SITEURL + "/fullcalendar/create",
	//url:"{{route('create')}}",
	//data: 'title=' + title + '&amp;start=' + start + '&amp;end=' + end,
	data:{title:title,start:start,end:end},
	type: "POST",
	success: function (data) 
	{
		displayMessage("Added Successfully");
		// $('#to_do').DataTable().ajax.reload();
		 location.reload();
	}
	});
	calendar.fullCalendar('renderEvent',{title: title,start: start,end: end,allDay: allDay},true);
	}
	calendar.fullCalendar('unselect');
	},
	eventDrop: function (event, delta) 
	{
		var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
		var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
	$.ajax({
	// url: '{{route('update')}}',
	url: SITEURL + "/fullcalendar/update",
	//data: 'title=' + event.title + '&amp;start=' + start + '&amp;end=' + end + '&amp;id=' + event.id,
	data:{title:event.title,start:start,end:end,id:event.id},
	type: "POST",
	success: function (response) 
	{
		displayMessage("Updated Successfully");
		$('#to_do').DataTable().ajax.reload();
	}
	});
	},
	eventClick: function (event) 
	{
		var deleteMsg = confirm("Do you really want to delete?");
	if (deleteMsg) 
	{
		$.ajax({
		type: "POST",
		// url: '{{route('destroy')}}',
		url: SITEURL + "/fullcalendar/delete",
		// data: "&amp;id=" + event.id,
		data:{id:event.id},
		success: function (response) {
		if(parseInt(response) > 0) 
		{
			$('#calendar').fullCalendar('removeEvents', event.id);
			displayMessage("Deleted Successfully");
			$('#to_do').DataTable().ajax.reload();
		}
	}
	});
	}
	}
	});
	});
	function displayMessage(message) 
	{
	$(".response").html("<div class='success'>"+message+"</div>");
	setInterval(function() { $(".success").fadeOut(); }, 1000);
	}				
</script>
			</section>
		</div>
	
		<div class="col-lg-5">
		<section class="card">
			<header class="card-header">
				<div class="card-actions">
					<a href="#" class="card-action card-action-toggle" data-card-toggle></a>
					<a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
				</div>

				<h2 class="card-title">To Do</h2>
			</header>
			<div class="card-body">
			<table class="table table-bordered table-striped to_do" id="to_do">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Start</th>
						<th>End</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
			</div>
		</section>
		</div>
	</div>
	
	<div class="row pt-4 mt-1">
		<div class="col-xl-12">
			<section class="card card-transparent mb-3">
				<header class="card-header">
					<div class="card-actions">
						<a href="#" class="card-action card-action-toggle" data-card-toggle></a>
						<a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
					</div>
	
					<h2 class="card-title">Global Stats</h2>
				</header>
				<div class="card-body">
					<!-- <div id="vectorMapWorld" style="height: 350px; width: 100%;"></div> -->
					<!-- <div id="world-map" style="width: 600px; height: 400px"></div> -->
					<div id="vmap" style="margin:0 auto;width:100%;height:520px;"></div>

				</div>
	<script type="text/javascript">

      var countries = '';
      function getCountryData(){
        $.ajax({
          url:'vendor/jqvmap/data/jquery.vmap.sampledata.json',
          type:'GET',
          success:function(res){
			countries = res;
          }
        });
      }
      getCountryData();
      $(document).ready(function(){
        $("#vmap").vectorMap({
          map: 'world_en',
          backgroundColor:'#FFFFFF',
          borderColor:'#555',
          color:'#555',
          hoverOpacity:0.7,
          selectedColor:'#666666',
          enableZoom:true,
		  enableDrag:true,
          showTooltip:true,
          normalizeFunction:'polynomial',
          onLabelShow:function(event,label,code){
            code = code.toUpperCase();
            country_name = countries[code];
			$.ajax({
				url:"{{route('checkCountryUsers')}}"+"/"+country_name,
				Type:'GET',
				success: function(response){
					var totalusers =response;
					label.html('<strong>'+country_name+':'+' '+totalusers+' '+'Users'+'</strong>');
				}
			});
		  }
        });
      });
    </script>
	</section>
	</div>
	</div>
	<!-- end: page -->
</section>

@push('scripts')
<script src="{!! asset('vendor/datatables/media/js/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js') !!}"></script>
<script type="text/javascript">
$(document).ready(function() {
		$("#view-users").DataTable({
		processing: true,
        serverSide: true,
		"destroy": true,
        "ajax": {
                	url: "{{route('dashboard-users')}}"
                },
                    "columns":[
                        {data: "ID", name:"ID"},
                        {data: "user_nicename", name:"user_nicename"},
                        {data: "user_email", name:"user_email"},
                    ]
		});
});
			
$(document).ready(function() {
    $("#to_do").DataTable({
		processing: true,
        serverSide: true,
        "ajax": {
                	url: "{{route('to-do-list')}}"
                },
                    "columns":[
                        {data: "title", name:"title"},
                        {data: "start", name:"start"},
                        {data: "end", name:"end"},
                    ]
	});
	
});
			
	
</script>
@endpush>
@endsection

