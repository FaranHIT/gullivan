@extends('layout.adminpanel.default')
@section('content')
<section role="main" class="content-body">
	<header class="page-header">
		<h2>Assign Country Permission</h2>
	
		<div class="right-wrapper text-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.html">
						<i class="fas fa-home"></i>
					</a>
				</li>
				<li><span>Form</span></li>
				<li><span>Assign Country Permission</span></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
		</div>
	</header>

	<!-- start: page -->
			<section class="body-sign">
					<div class="panel card-sign">
					<div class="alert alert-default">
					@if(session('success-msg'))
                    <div class="alert alert-success">
                        {{session('success-msg')}}
                    </div>
                    @elseif(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                    @elseif(session('alert-msg'))
                    <div class="alert alert-info">
                        {{session('alert-msg')}}
                    </div>
                    @endif
					<div class="card-title-sign mt-3 text-right">
					   <h2 class="title text-uppercase font-weight-bold m-0">Assign Country Permission</h2>
				    </div>
					<div class="card-body">
					<form action="{{url('post-assign-country-permissions-form')}}" method="post">
							{{ csrf_field() }}
							<div class="form-group mb-3">
								<label>Country Permission</label>
								<div class="input-group">
                                <select class="form-control" name="permission[]" id="add-permission" multiple="multiple">
                                <!-- <option disabled selected>Choose...</option> -->
                                @if(count($permissions) > 0)
                                    @foreach($permissions as $permission)
                                        <option value="{{$permission->name}}">{{$permission->name}}</option>
                                    @endforeach
                                @endif
                            </select>
							</div>
							<div>
							<input id="select-all" type="checkbox" >Select All Permissions
							</div>
							<input class="form-control form-control-lg" name="user_id" value="{{$id}}" type="hidden"/>
							<div class="form-group mb-3">
								<button type="submit" class="btn btn-primary mt-2">Submit</button>
							</div>
						</form>
					</div>
		</section>
	<!-- end: page -->
	@push('scripts')
	<script type="text/javascript">
	$(document).ready(function(){
		$("#add-permission").select2({placeholder: "Select Countries"});

		$("#select-all").click(function(){
        if($("#select-all").is(':checked')){
            $("#add-permission > option").prop("selected",true );
            $("#add-permission").trigger("change");
        } else {
            $("#add-permission > option").prop("selected",false);
            $("#add-permission").trigger("change");
        }
        });
	});
	</script>
	@endpush
@endsection