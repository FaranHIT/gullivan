@extends('layout.adminpanel.default')
@section('content')
<section role="main" class="content-body">
	<header class="page-header">
		<h2>Assign City Permission</h2>
	
		<div class="right-wrapper text-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.html">
						<i class="fas fa-home"></i>
					</a>
				</li>
				<li><span>Form</span></li>
				<li><span>Assign City Permission</span></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
		</div>
	</header>

	<!-- start: page -->
			<section class="body-sign">
					<div class="panel card-sign">
					<div class="alert alert-default">
					@if(session('success-msg'))
                    <div class="alert alert-success">
                        {{session('success-msg')}}
                    </div>
                    @elseif(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                    @elseif(session('alert-msg'))
                    <div class="alert alert-info">
                        {{session('alert-msg')}}
                    </div>
                    @endif
					</div>
					<div class="card-title-sign mt-3 text-right">
					   <h2 class="title text-uppercase font-weight-bold m-0">Assign City Permission</h2>
				    </div>
					<div class="card-body">
					<form action="{{url('post-assign-permissions-form')}}" method="post">
							{{ csrf_field() }}
							<div class="form-group mb-3">

								<label>Country Name</label>
								<div class="input-group">
                                <select class="form-control" id="country" name="Country-Permission">
								<option disabled selected>Select Country</option>
                                @if(count($permissions) > 0)
                                    @foreach($permissions as $permission)
                                        <option class="c_id" value="{{$permission->id}}">{{$permission->name}}</option>
                                    @endforeach
                                @endif
                            </select>
							@if($errors->has('Country-Permission'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Country-Permission') }}</span>
                        	@endif
							</div>
							</br>

							<label>City Permission Name</label>
								<div class="input-group">
                                <select class="form-control" multiple="multiple" id="city" name="City-Permission[]">     
                            </select>
							@if($errors->has('City-Permission'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('City-Permission') }}</span>
                        	@endif
							</div>
							<div>
							<input id="cities" type="checkbox" >Select All Cities
							</div>
							
							<input class="form-control form-control-lg" name="user_id" value="{{$id}}" type="hidden"/>
							
							<div class="form-group mb-3">
								<button type="submit" class="btn btn-primary mt-2">Submit</button>
							</div>
						</form>
					</div>
		</section>
	<!-- end: page -->
	@push('scripts')
        <script type="text/javascript">
	    $(document).ready(function() {
        
        $("#country").select2();
		$("#city").select2({placeholder: "Select cities"});

		$("#cities").click(function(){
        if($("#cities").is(':checked')){
            $("#city > option").prop("selected",true );
            $("#city").trigger("change");
        } else {
            $("#city > option").prop("selected",false);
            $("#city").trigger("change");
        }
        });
		
		$("#country").on('change',function(){
			var country_id = $(this).val();
			
			$.ajax({
				url:"{{route('select-countries-to-cities')}}"+"/"+country_id,
				//url: '/public/select-countries-to-cities/'+country_id,
				type: 'GET',
				success: function(response){
					var x = response;
					$("#city").empty();
					$.each(x, function(i, value)
					{	
						var cityname = value.name;
						var RemoveSpacesCityName = cityname.replace(/\s+/, "");
						$("#city").append('<option value='+RemoveSpacesCityName+'>'+value.name+'</option>');
					
   					});							
				}
			});
		});
		
        });

	
        </script>
    @endpush
@endsection