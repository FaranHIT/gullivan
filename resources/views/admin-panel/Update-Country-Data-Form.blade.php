@extends('layout.adminpanel.default')
@section('content')
    <section role="main" class="content-body">
        <header class="page-header">
            <h2>Update Country Data</h2>

            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="index.html">
                            <i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li><span>Tables</span></li>
                    <li><span>Update Countries Data</span></li>
                </ol>

                <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
            </div>
        </header>

        <!-- start: page -->

        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>
                <b>Update Country Data</b>
            </header>
            <div class="card-body">
                @if(session('success-msg'))
                    <div class="alert alert-success">
                        {{session('success-msg')}}
                    </div>
                @elseif(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                @elseif(session('alert-msg'))
                    <div class="alert alert-info">
                        {{session('alert-msg')}}
                    </div>
                @endif
                <form method="post" action="{{route('update-country-data')}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="country_id" value="{{$CountryData->country_id}}">
                    <div class="form-row justify-content-around">
                        <div class="form-group col-md-8 ">
                            <label>YouTube video link</label>
                            <input type="text" name="Video-Link" value="{{$CountryData->youtube_link}}" class="form-control" placeholder="Enter url">
                            @if($errors->has('Video-Link'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Video-Link') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Best Time To Go</label>
                            <input type="text" name="Best-Time" value="{{$CountryData->best_time_to_go}}" class="form-control" placeholder="Enter detail">
                            @if($errors->has('Best-Time'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Best-Time') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Transportation</label>
                            <input type="text" name="Transportation" value="{{$CountryData->transportation}}" class="form-control" placeholder="Enter detail">
                            @if($errors->has('Transportation'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Transportation') }}</span>
                            @endif
                        </div>


                        <div class="form-group col-md-8 ">
                            <label>Weather</label>
                            <input type="text" name="Weather" value="{{$CountryData->weather}}" class="form-control" placeholder="Enter detail">
                            @if($errors->has('Weather'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Weather') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Information</label>
                            <input type="text" name="Information" value="{{$CountryData->information}}" class="form-control" placeholder="Enter detail">
                            @if($errors->has('Information'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Information') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>The Electric</label>
                            <input type="text" name="Electric" value="{{$CountryData->the_electric}}" class="form-control" placeholder="Enter detail">
                            @if($errors->has('Electric'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Electric') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Language</label>
                            <input type="text" name="Language" value="{{$CountryData->language}}" class="form-control" placeholder="Enter detail">
                            @if($errors->has('Language'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Language') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Currency</label>
                            <input type="text" name="Currency" value="{{$CountryData->currency}}" class="form-control" placeholder="Enter detail">
                            @if($errors->has('Currency'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Currency') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Constitution</label>
                            <input type="text" name="Constitution" value="{{$CountryData->constitution}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Emergency No</label>
                            <input type="text" name="emergency-no" value="{{$CountryData->emergency_no}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Embassies</label>
                            <input type="text" name="embassies" value="{{$CountryData->embassies}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>New Offer</label>
                            <input type="text" name="New-Offer" value="{{$CountryData->new_offer}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Rules</label>
                            <input type="text" name="Rules" value="{{$CountryData->rules}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>History</label>
                            <input type="text" name="History" value="{{$CountryData->history}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Driving</label>
                            <input type="text" name="Driving" value="{{$CountryData->driving}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Universities</label>
                            <input type="text" name="Universities" value="{{$CountryData->universities}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Visa + University Acceptance Fee</label>
                            <input type="text" name="visa-uni-fee" value="{{$CountryData->visa_uni_acceptance_fee}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Tours</label>
                            <input type="text" name="Tours" value="{{$CountryData->tours}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-5 ">
                            <button type="submit" class="btn btn-primary btn-sm btn-block">Update</button>
                        </div>

                    </div>
                </form>
            </div>

        </section>
        <!-- end: page -->
    </section>
@endsection
