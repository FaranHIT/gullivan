@extends('layout.adminpanel.default')
@section('content')
    <section role="main" class="content-body">
        <header class="page-header">
            <h2>City Data</h2>

            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="index.html">
                            <i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li><span>Tables</span></li>
                    <li><span>City Data</span></li>
                </ol>

                <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
            </div>
        </header>

        <!-- start: page -->

        <section class="card">
        @if(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                @endif
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>
            </header>
            <div class="card-body">

            <div id="accordion">

  <div class="card">
    <div class="card-header">
      <a class="card-link" data-toggle="collapse" href="#collapseVideoLink">
        Video Link
      </a>
    </div>
    <div id="collapseVideoLink" class="collapse" data-parent="#accordion">
      <div class="card-body">
        {{$CityData->youtube}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseBestTimeToGo">
        Best Time To Go
      </a>
    </div>
    <div id="collapseBestTimeToGo" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CityData->best_time_to_go}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseTransportation">
        Transportation
      </a>
    </div>
    <div id="collapseTransportation" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CityData->transportation}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseWeather">
        Weather
      </a>
    </div>
    <div id="collapseWeather" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CityData->weather}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseInformation">
        Information
      </a>
    </div>
    <div id="collapseInformation" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CityData->information}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseRentACar">
        Rent A Car
      </a>
    </div>
    <div id="collapseRentACar" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CityData->rent_a_car}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseRealEstate">
        Real Estate
      </a>
    </div>
    <div id="collapseRealEstate" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CityData->real_estate}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseHotels">
        Hotels
      </a>
    </div>
    <div id="collapseHotels" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CityData->hotels}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseCoffee">
        Coffee Shops
      </a>
    </div>
    <div id="collapseCoffee" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CityData->coffee_shops}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseRestaurants">
        Restaurants
      </a>
    </div>
    <div id="collapseRestaurants" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CityData->restaurants}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseBars">
        Bars
      </a>
    </div>
    <div id="collapseBars" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CityData->bars}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseAddYourBuisness">
        Add Your Buisness
      </a>
    </div>
    <div id="collapseAddYourBuisness" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CityData->add_ur_bussiness}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseTaxis">
        Taxis
      </a>
    </div>
    <div id="collapseTaxis" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CityData->taxis}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseRoadLaw">
        Road Law
      </a>
    </div>
    <div id="collapseRoadLaw" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CityData->road_laws}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseLawyers">
        Lawyers
      </a>
    </div>
    <div id="collapseLawyers" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CityData->lawyers}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseEvents">
        Events
      </a>
    </div>
    <div id="collapseEvents" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CityData->events}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseTours">
        Tours
      </a>
    </div>
    <div id="collapseTours" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CityData->tours}}
      </div>
    </div>
  </div>

</div>
                  </section>
        <!-- end: page -->
    </section>
@endsection
