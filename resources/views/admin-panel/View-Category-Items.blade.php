@extends('layout.adminpanel.default')
@section('content')
<section role="main" class="content-body">
	<header class="page-header">
		<h2>View Category Items</h2>

		<div class="right-wrapper text-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.html">
						<i class="fas fa-home"></i>
					</a>
				</li>
				<li><span>Table</span></li>
				<li><span>View Category Items</span></li>
			</ol>

			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
		</div>
	</header>

	<!-- start: page -->
	<section class="card">
			<header class="card-header">
				<div class="card-actions">
					<a href="#" class="card-action card-action-toggle" data-card-toggle></a>
					<a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
				</div>

				<h2 class="card-title">View Category Items</h2>
			</header>
			<div class="card-body">
            @if(session('success-msg'))
                    <div class="alert alert-success">
                        {{session('success-msg')}}
                    </div>
                @elseif(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                @elseif(session('alert-msg'))
                    <div class="alert alert-info">
                        {{session('alert-msg')}}
                    </div>
                @endif
                <div class="form-row">  
                <div class="form-group col-md-4">
                <a href="{{route('cities')}}" class="float-left"><i style="color:black;font-size:30px;" class="fa fa-arrow-left" aria-hidden="true"></i></a>
                </div>
                </div>
                <input type="hidden" id="CityId" value="{{$CityId}}">
			<table class="table table-bordered table-responsive table-striped  " id="view-city-categories">
                    <thead>
                    <tr>
                    <th>ID</th>
                    <th>Picture</th>
                    <th>Title</th>
                    <th>Detail</th>
                    <th>Address</th>
                    <th>Website</th>
                    <th>Number</th> 
                    <th>Timing</th> 
                    <th>Action</th>   
                    </tr>
                    </thead>
                    <tbody>
					</tbody>
                </table>
			</div>
		</section>
	<!-- end: page -->
</section>
@push('scripts')
<script src="{!! asset('vendor/datatables/media/js/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js') !!}"></script>
<script src="{!! asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js') !!}"></script>
<script type="text/javascript">
$(document).ready(function() {
    var cityID = $('#CityId').val();
                $("#view-city-categories").DataTable({
                    processing: true,
                    serverSide: true,
                    "ajax": {
                        url: "{{route('ViewCategoryItems')}}"+"/"+cityID,
                    },
                    "columns":[
                        {data: "id", name:"id"},
                        {data: "picture",name:"picture",
                            "render": function (data, type, full, meta) {
                            //    data.forEach(function(dt){
                            //     return "<img src=\"{{asset('storage')}}"+"/" +dt+ "\" height=\"50\"/>";
                            //    });
                        if(data !== null)
                        {           
                            var count_images = data.length;
                            if(count_images == 1)
                            {
                                return "<img src=\"{{asset('storage')}}"+"/" +data[0]+ "\" height=\"50\"/>";  
                            }
                            else if(count_images == 2)
                            {
                                return "<img src=\"{{asset('storage')}}"+"/" +data[0]+ "\" height=\"50\"/>,<img src=\"{{asset('storage')}}"+"/" +data[1]+ "\" height=\"50\"/>";    
                            }
                            else if(count_images == 3)
                            {
                                return "<img src=\"{{asset('storage')}}"+"/" +data[0]+ "\" height=\"50\"/>,<img src=\"{{asset('storage')}}"+"/" +data[1]+ "\" height=\"50\"/>,<img src=\"{{asset('storage')}}"+"/" +data[2]+ "\" height=\"50\"/>";    
                            }
                            else if(count_images == 4)
                            {
                                return "<img src=\"{{asset('storage')}}"+"/" +data[0]+ "\" height=\"50\"/> <br> <img src=\"{{asset('storage')}}"+"/" +data[1]+ "\" height=\"50\"/> <br> <img src=\"{{asset('storage')}}"+"/" +data[2]+ "\" height=\"50\"/> <br> <img src=\"{{asset('storage')}}"+"/" +data[3]+ "\" height=\"50\"/>";    
                            }
                            else if(count_images == 5)
                            {
                                return "<img src=\"{{asset('storage')}}"+"/" +data[0]+ "\" height=\"50\"/>,<img src=\"{{asset('storage')}}"+"/" +data[1]+ "\" height=\"50\"/>,<img src=\"{{asset('storage')}}"+"/" +data[2]+ "\" height=\"50\"/>,<img src=\"{{asset('storage')}}"+"/" +data[3]+ "\" height=\"50\"/>,<img src=\"{{asset('storage')}}"+"/" +data[4]+ "\" height=\"50\"/>";    
                            }
                            else if(count_images == '')
                            {
                                return 'No Image';
                            }
                        }
                        return 'No Image';   
                           // return "<img src=\"{{asset('storage')}}"+"/" +data[0]+ "\" height=\"50\"/>,<img src=\"{{asset('storage')}}"+"/" +data[1]+ "\" height=\"50\"/>";
                            },
                            "title": "Picture",
                            "orderable": false,
                            "searchable": false},
                        {data: "title", name:"title"},
                        {data: "details", name:"details"},
                        {data: "address", name:"address"},
                        {data: "website", name:"website"},
                        {data: "number", name:"number"},
                        {data: "Timing", name:"Timing"},
                        // {data: "tuesday_time", name:"tuesday_time"},
                        // {data: "wednesday_time", name:"wednesday_time"},
                        // {data: "thursday_time", name:"thursday_time"},
                        // {data: "friday_time", name:"friday_time"},
                        // {data: "saturday_time", name:"saturday_time"},
                        // {data: "sunday_time", name:"sunday_time"},
                        {data: "action", name:"action",orderable:false},
                    ]
                });
            });
</script>
@endpush
@endsection
