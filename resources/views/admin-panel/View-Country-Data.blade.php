@extends('layout.adminpanel.default')
@section('content')
    <section role="main" class="content-body">
        <header class="page-header">
            <h2>Country Data</h2>

            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="index.html">
                            <i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li><span>Tables</span></li>
                    <li><span>Country Data</span></li>
                </ol>

                <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
            </div>
        </header>

        <!-- start: page -->

        <section class="card">
        @if(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                @endif
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>
            </header>
            <div class="card-body">

            <div id="accordion">

  <div class="card">
    <div class="card-header">
      <a class="card-link" data-toggle="collapse" href="#collapseVideoLink">
        Video Link
      </a>
    </div>
    <div id="collapseVideoLink" class="collapse" data-parent="#accordion">
      <div class="card-body">
        {{$CountryData->youtube_link}}
      </div>
    </div>
  </div>

   <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseBestTimeToGo">
        Best Time To Go
      </a>
    </div>
    <div id="collapseBestTimeToGo" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->best_time_to_go}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseTransportation">
        Transportation
      </a>
    </div>
    <div id="collapseTransportation" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->transportation}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseWeather">
        Weather
      </a>
    </div>
    <div id="collapseWeather" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->weather}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseInformation">
        Information
      </a>
    </div>
    <div id="collapseInformation" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->information}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseElectric">
        The Electric
      </a>
    </div>
    <div id="collapseElectric" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->the_electric}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapLanguage">
        Language
      </a>
    </div>
    <div id="collapLanguage" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->language}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseCurrency">
        Currency
      </a>
    </div>
    <div id="collapseCurrency" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->currency}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseConstitution">
        Constitution
      </a>
    </div>
    <div id="collapseConstitution" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->constitution}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseEmergencyNo">
        Emergency No
      </a>
    </div>
    <div id="collapseEmergencyNo" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->emergency_no}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseEmbassies">
        Embassies
      </a>
    </div>
    <div id="collapseEmbassies" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->embassies}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseNewOffer">
        New Offer
      </a>
    </div>
    <div id="collapseNewOffer" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->new_offer}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseRules">
        Rules
      </a>
    </div>
    <div id="collapseRules" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->rules}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseHistory">
        History
      </a>
    </div>
    <div id="collapseHistory" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->history}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseDriving">
        Driving
      </a>
    </div>
    <div id="collapseDriving" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->driving}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseUniversities">
        Universities
      </a>
    </div>
    <div id="collapseUniversities" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->universities}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseVisaUniAcceptanceFee">
        Visa & University Acceptance Fee
      </a>
    </div>
    <div id="collapseVisaUniAcceptanceFee" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->visa_uni_acceptance_fee}}
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseTours">
        Tours
      </a>
    </div>
    <div id="collapseTours" class="collapse" data-parent="#accordion">
      <div class="card-body">
      {{$CountryData->tours}}
      </div>
    </div>
  </div>

</div>
                  </section>
        <!-- end: page -->
    </section>
@endsection
