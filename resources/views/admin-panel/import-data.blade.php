@extends('layout.adminpanel.default')
@section('content')
    <section role="main" class="content-body">
        <header class="page-header">
            <h2>Import Data</h2>

            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="#">
                            <i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li><span>Tables</span></li>
                    <li><span>Import Data</span></li>
                </ol>

                <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
            </div>
        </header>

        <!-- start: page -->

        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>
                <b>Import Data</b>
            </header>
            <div class="card-body">
                @if(session('success-msg'))
                    <div class="alert alert-success">
                        {{session('success-msg')}}
                    </div>
                @elseif(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                @elseif(session('alert-msg'))
                    <div class="alert alert-info">
                        {{session('alert-msg')}}
                    </div>
                @endif
                <form method="post" action="{{route('saveFileData')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row justify-content-around">
                        <div class="form-group col-md-8">
                            <label>Upload File</label>
                            <input type="file" name="data_file" class="form-control" required="required">
                            @if($errors->has('data_file'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('data_file') }}</span>
                            @endif
                        </div>
                        <div class="form-group col-md-5 ">
                            <button type="submit" class="btn btn-primary btn-sm btn-block" id="importBtn">Import</button>
                            <span style="display: none" class="mt-2" id="data-loading">Data is being importing.....</span>
                        </div>
                    </div>
                </form>
            </div>

        </section>
        <!-- end: page -->
    </section>
    @push('scripts')
        <script>
            $('#importBtn').on('click',function () {
                $('#data-loading').css('display','block');
            });
        </script>
    @endpush
@endsection
