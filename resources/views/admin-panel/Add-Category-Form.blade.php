@extends('layout.adminpanel.default')
@section('content')
    <section role="main" class="content-body">
        <header class="page-header">
            <h2>Add City Category</h2>

            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="#">
                            <i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li><span>Tables</span></li>
                    <li><span>City Categories</span></li>
                </ol>

                <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
            </div>
        </header>

        <!-- start: page -->

        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>
                <b>Add New Categroy</b>
            </header>
            <div class="card-body">
                @if(session('success-msg'))
                <div class="alert alert-success">
                    {{session('success-msg')}}
                </div>
                @elseif(session('error-msg'))
                <div class="alert alert-danger">
                    {{session('error-msg')}}
                </div>
                @elseif(session('alert-msg'))
                <div class="alert alert-info">
                    {{session('alert-msg')}}
                </div>
                @endif
                <form method="post" action="{{route('SaveCategory')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row justify-content-around">
                        <div class="form-group text-class col-md-5 pt-3">
                            <label>Category Name</label>
                            <input type="text" name="category_name" id="category_name" class="form-control" required="required" placeholder="Enter Category Name">
                        </div>
                    </div>
                    <div class="form-row justify-content-around pt-2">
                        <div class="form-group col-md-5">
                            <button type="submit" id="save_category" class="btn btn-primary btn-sm btn-block">Save</button>
                        </div>
                    </div>
                </form>
            </div>

        </section>
        <!-- end: page -->
    </section>
    @push('scripts')
        <script type="text/javascript">
        </script>
    @endpush
@endsection
