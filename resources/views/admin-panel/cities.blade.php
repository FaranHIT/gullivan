@extends('layout.adminpanel.default')
@section('content')
    <section role="main" class="content-body">
        <header class="page-header">
            <h2>Cities</h2>

            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="index.html">
                            <i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li><span>Tables</span></li>
                    <li><span>Cities</span></li>
                </ol>

                <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
            </div>
        </header>

        <!-- start: page -->

        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>
            </header>
            <div class="card-body">
            @if(session('success-msg'))
                    <div class="alert alert-success">
                        {{session('success-msg')}}
                    </div>
                @elseif(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                @elseif(session('alert-msg'))
                    <div class="alert alert-info">
                        {{session('alert-msg')}}
                    </div>
                @endif
                <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5><b>Password</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    </div> 
                    <div class="modal-body">
                    <input type="hidden" name="city_id" id="city_id" value="" />
                    <input type="password" name="password" id="password" placeholder="Enter Password">
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="password-check" data-dismiss="modal">ok</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </div>
                </div>

                <table class="table table-bordered table-striped cities" id="cities">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Image</th>
                        <th>Country Name</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </section>
        <!-- end: page -->
    </section>
    @push('scripts')
        <script src="{!! asset('vendor/datatables/media/js/jquery.dataTables.min.js') !!}"></script>
        <script src="{!! asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') !!}"></script>
        <script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js') !!}"></script>
        <script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js') !!}"></script>
        <script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js') !!}"></script>
        <script src="{!! asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js') !!}"></script>
        <script src="{!! asset('vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js') !!}"></script>
        <script src="{!! asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js') !!}"></script>
        <script src="{!! asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js') !!}"></script>
        <script type="text/javascript">
            $(document).ready(function() {
               var table = $(".cities").DataTable({
                    processing: true,
                    serverSide: true,
                    "ajax": {
                        url: "{{route('cities')}}"
                    },
                    "columns":[
                        {data: "id",name:"id"},
                        {data: "city_image",name:"city_image",
                            "render": function (data, type, full, meta) {
                                if(data == null)
                                {
                                    return "No Image";
                                }
                                else
                                {
                                    return "<img src=\"{{asset('storage')}}"+"/" + data + "\" height=\"50\"/>";
                                }    
                            },
                            "title": "Image",
                            "orderable": true,
                            "searchable": true},
                        {data: "country_name",name:"country_name"},       
                        {data: "name",name:"name"},
                        {data: "action",name:"action",orderable:false},
                        
                    ]
                }); 
                $('#password-check').click(function(){
                   
                   var city_id = $('#city_id').val();
                   var password = $('#password').val();
                   $.ajax({
                    url:"{{route('removeCity')}}"+"/"+city_id+"/"+password,
                    //    url:'/public/remove-city/',
                    //    data: {  
                    //        city_id: city_id, 
                    //        password: password
                    //        },
                   
                    success: function(response)
                   {
                      var x = response;
                      if(x == "city delete successfully")
                      {
                        toastr.info(x);
                        table.ajax.reload();
                      }
                      else if(x == "Sorry City cannot be deleted")
                      {
                        toastr.info(x);
                      }
                      else if(x == "Sorry Only Admin Can City deleted")
                      {
                        toastr.info(x);
                      }
                      else
                      {
                        toastr.info(x);
                      }        
                   }    
               });

                });

            });

            function deleteCity(obj,e)
            {
                var cid = $(obj).attr("href");
                e.preventDefault();
                $('#city_id').val(cid);
            }

        </script>
    @endpush
@endsection
