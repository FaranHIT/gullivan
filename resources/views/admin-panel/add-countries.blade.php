@extends('layout.adminpanel.default')
@section('content')
    <section role="main" class="content-body">
        <header class="page-header">
            <h2>Add Countries</h2>

            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="index.html">
                            <i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li><span>Tables</span></li>
                    <li><span>Countries</span></li>
                </ol>

                <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
            </div>
        </header>

        <!-- start: page -->

        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>
                <b>Add New Country</b>
            </header>
            <div class="card-body">
                @if(session('success-msg'))
                    <div class="alert alert-success">
                        {{session('success-msg')}}
                    </div>
                @elseif(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                @elseif(session('alert-msg'))
                    <div class="alert alert-info">
                        {{session('alert-msg')}}
                    </div>
                @endif
                <form method="post" action="{{route('saveCountries')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row justify-content-around">
                        <div class="form-group col-md-8 ">
                            <label>Name</label>
                            <input type="text" name="name" id="country_name" class="form-control" placeholder="Enter Country Name">
                            @if($errors->has('name'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                        <div class="form-group col-md-8 ">
                            <label>Image</label>
                            <input type="file" name="country_img" class="form-control">
                            @if($errors->has('country_img'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('country_img') }}</span>
                            @endif
                        </div>
                        <div class="form-group col-md-5 ">
                            <button type="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                        </div>
                        <input type="hidden" name="geo_lat" id="country_lat">
                        <input type="hidden" name="geo_long" id="country_long">
                    </div>
                </form>
            </div>

        </section>
        <!-- end: page -->
    </section>
    @push('scripts')
        <script src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_API_KEY')}}&libraries=places&callback=initCountry" async defer></script>
        <script type="text/javascript">
            function initCountry() {
                var input = document.getElementById('country_name');
                var autocomplete = new google.maps.places.Autocomplete(input,{types: ["geocode"]});
                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    var place = autocomplete.getPlace();
                    document.getElementById('country_lat').value = place.geometry.location.lat();
                    document.getElementById('country_long').value = place.geometry.location.lng();
                });
            }
            google.maps.event.addDomListener(window, 'load', initCountry);
        </script>
    @endpush
@endsection
