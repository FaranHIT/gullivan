@extends('layout.adminpanel.default')
@section('content')
    <section role="main" class="content-body">
        <header class="page-header">
            <h2>Add City Data</h2>

            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="index.html">
                            <i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li><span>Tables</span></li>
                    <li><span>City Data</span></li>
                </ol>

                <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
            </div>
        </header>

        <!-- start: page -->

        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>
                <b>Add City Data</b>
            </header>
            <div class="card-body">
                @if(session('success-msg'))
                    <div class="alert alert-success">
                        {{session('success-msg')}}
                    </div>
                @elseif(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                @elseif(session('alert-msg'))
                    <div class="alert alert-info">
                        {{session('alert-msg')}}
                    </div>
                @endif
                <form method="post" action="{{route('save-city-data')}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="city_id" value="{{$city_id}}">
                    <div class="form-row justify-content-around">
                        <div class="form-group col-md-8 ">
                            <label>YouTube video link</label>
                            <input type="text" name="Video-Link" id="video_link" class="form-control" placeholder="Enter url">
                            @if($errors->has('Video-Link'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Video-Link') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Best Time To Go</label>
                            <input type="text" name="Best-Time" id="best-time" class="form-control" placeholder="Enter detail">
                            @if($errors->has('Best-Time'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Best-Time') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Transportation</label>
                            <input type="text" name="Transportation" id="transportation" class="form-control" placeholder="Enter detail">
                            @if($errors->has('Transportation'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Transportation') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Weather</label>
                            <input type="text" name="Weather" id="weather" class="form-control" placeholder="Enter detail">
                            @if($errors->has('Weather'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Weather') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Information</label>
                            <input type="text" name="Information" id="Information" class="form-control" placeholder="Enter detail">
                            @if($errors->has('Information'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Information') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Rent A Car</label>
                            <input type="text" name="Rent-A-Car" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Real Estate</label>
                            <input type="text" name="Real-Estate" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Hotels</label>
                            <input type="text" name="Hotels" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Coffee Shops</label>
                            <input type="text" name="Coffee" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Restaurants</label>
                            <input type="text" name="Restaurants" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Bars</label>
                            <input type="text" name="Bars" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Add Your Buisness</label>
                            <input type="text" name="Add-Your-Buisness" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Taxis</label>
                            <input type="text" name="Taxis" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Road Law</label>
                            <input type="text" name="Road-Law" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Lawyers</label>
                            <input type="text" name="Lawyers" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Events</label>
                            <input type="text" name="Events" class="form-control" placeholder="Enter detail/url">
                        </div>
    
                        <div class="form-group col-md-8 ">
                            <label>Tours</label>
                            <input type="text" name="Tours" id="Tours" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-5 ">
                            <button type="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                        </div>

                        <input type="hidden" name="geo_lat" id="country_lat">
                        <input type="hidden" name="geo_long" id="country_long">
                    </div>
                </form>
            </div>

        </section>
        <!-- end: page -->
    </section>
    @push('scripts')
        <script src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_API_KEY')}}&libraries=places&callback=initCountry" async defer></script>
        <script type="text/javascript">
            function initCountry() {
                var input = document.getElementById('country_name');
                var autocomplete = new google.maps.places.Autocomplete(input,{types: ["geocode"]});
                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    var place = autocomplete.getPlace();
                    document.getElementById('country_lat').value = place.geometry.location.lat();
                    document.getElementById('country_long').value = place.geometry.location.lng();
                });
            }
            google.maps.event.addDomListener(window, 'load', initCountry);
        </script>
    @endpush
@endsection
