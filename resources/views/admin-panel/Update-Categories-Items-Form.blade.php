@extends('layout.adminpanel.default')
@section('content')
    <section role="main" class="content-body">
        <header class="page-header">
            <h2>Update Category Items</h2>

            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="index.html">
                            <i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li><span>Tables</span></li>
                    <li><span>Update Categories Items</span></li>
                </ol>

                <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
            </div>
        </header>

        <!-- start: page -->

        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>
                <b>Update Item</b>
            </header>
            <div class="card-body">
                @if(session('success-msg'))
                <div class="alert alert-success">
                    {{session('success-msg')}}
                </div>
                @elseif(session('error-msg'))
                <div class="alert alert-danger">
                    {{session('error-msg')}}
                </div>
                @elseif(session('alert-msg'))
                <div class="alert alert-info">
                    {{session('alert-msg')}}
                </div>
                @endif
                <form action="{{route('PostUpdateItems')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row justify-content-around">
                    <input type="hidden" name="Items_id" value="{{$Items->id}}" class="form-control">
                        <!-- <div class="form-group text-class col-md-8 pt-3">
                            <label style="font-size:20px">Picture</label>
                            <input type="file" name="picture" value="{{$Items->picture}}" class="form-control">
                        </div> -->

                        <div class="form-group text-class col-md-8 pt-3">
                            <label style="font-size:20px">Title</label>
                            <input type="text" name="title" autocomplete="off" value="{{$Items->title}}" class="form-control" placeholder="Enter Title">
                        </div>
                        
                        <div class="form-group text-class col-md-8 pt-3">
                            <label style="font-size:20px">Detail</label>
                            <input type="text" name="detail" autocomplete="off" value="{{$Items->details}}" class="form-control" placeholder="Enter Detail">
                        </div>

                        <div class="form-group text-class col-md-8 pt-3">
                            <label style="font-size:20px">Address</label>
                            <input type="text" name="address" autocomplete="off" value="{{$Items->address}}" class="form-control" placeholder="Enter Address">
                        </div>

                        <div class="form-group text-class col-md-8 pt-3">
                            <label style="font-size:20px">Website</label>
                            <input type="text" name="website" autocomplete="off" value="{{$Items->website}}" class="form-control" placeholder="Enter Website url">
                        </div>

                        <div class="form-group text-class col-md-8 pt-3">
                            <label style="font-size:20px">Number</label>
                            <input type="text" name="number" autocomplete="off" value="{{$Items->number}}" class="form-control" placeholder="Enter Number">
                        </div>

                        <div class="form-group text-class col-md-8 pt-3">
                            <label style="font-size:20px">Timing</label>
                            <!-- <input type="text" name="timing" autocomplete="off" value="{{$Items->timing}}" class="form-control" placeholder="Enter Timing"> -->

                            <input type="text" name="monday_time" id="monday_time" value="{{$Items->moday_time}}" autocomplete="off" class="form-control" placeholder="Enter Monday Time">
                            <input type="text" name="tuesday_time" id="tuesday_time" value="{{$Items->tuesday_time}}" autocomplete="off" class="form-control" placeholder="Enter Tuesday Time">
                            <input type="text" name="wednesday_time" id="wednesday_time" value="{{$Items->wednesday_time}}" autocomplete="off" class="form-control" placeholder="Enter Wednesday Time">
                            <input type="text" name="thursday_time" id="thursday_time" value="{{$Items->thursday_time}}" autocomplete="off" class="form-control" placeholder="Enter Thursday Time">
                            <input type="text" name="friday_time" id="friday_time" value="{{$Items->friday_time}}" autocomplete="off" class="form-control" placeholder="Enter Friday Time">
                            <input type="text" name="saturday_time" id="saturday_time" value="{{$Items->saturday_time}}" autocomplete="off" class="form-control" placeholder="Enter Saturday Time">
                            <input type="text" name="sunday_time" id="sunday_time" value="{{$Items->sunday_time}}" autocomplete="off" class="form-control" placeholder="Enter Sunday Time">
                        </div>
                        
                        <div class="form-group col-md-5">
                            <button type="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                        </div>
                </form>
            </div>

        </section>
        <!-- end: page -->
    </section>
@endsection