@extends('layout.adminpanel.default')
@section('content')
    <section role="main" class="content-body">
        <header class="page-header">
            <h2>Update Cities</h2>

            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="index.html">
                            <i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li><span>Tables</span></li>
                    <li><span>Cities</span></li>
                </ol>

                <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
            </div>
        </header>

        <!-- start: page -->

        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>
                <b>Update City</b>
            </header>
            <div class="card-body">
                @if(session('success-msg'))
                    <div class="alert alert-success">
                        {{session('success-msg')}}
                    </div>
                @elseif(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                @elseif(session('alert-msg'))
                    <div class="alert alert-info">
                        {{session('alert-msg')}}
                    </div>
                @endif
                    @if($get_city)
                <form method="post" action="{{route('updateCity',$get_city->id)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row justify-content-around">
                        <div class="form-group col-md-6 pt-3">
                            <label>Name</label>
                            <input type="text" name="name" id="city_name" class="form-control" value="{{$get_city->name}}" placeholder="Enter Country Name">
                        </div>
                        <div class="form-group col-md-6 ">
                            <label>Country</label>
                            <select class="form-control" name="country" required>
                                <option selected value="{{$get_city->country->id}}">{{$get_city->country->name}}</option>
                                @if(count($countries) > 0)
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-12 ">
                            <label>Image</label>
                            <input type="file" name="city_img" required class="form-control">
                        </div>
                        
                        <div class="form-group col-md-12">
                        </div>
                        <div class="form-group col-md-5 ">
                            <button type="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                        </div>
                        <input type="hidden" name="cityName" value="{{$get_city->name}}" id="cityName">
                        <input type="hidden" name="city_lat" value="{{$get_city->geo_lat}}" id="city_lat">
                        <input type="hidden" name="city_long" value="{{$get_city->geo_long}}" id="city_long">
                    </div>
                </form>
                    @endif
            </div>

        </section>
        <!-- end: page -->
    </section>
    @push('scripts')
        <script src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_API_KEY')}}&libraries=places&callback=initUpdateCity" async defer></script>
        <script type="text/javascript">
            function initUpdateCity() {
                var input = document.getElementById('city_name');
                var autocomplete = new google.maps.places.Autocomplete(input,{types: ['(cities)']});
                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    var place = autocomplete.getPlace();
                    document.getElementById('cityName').value = place.name;
                    document.getElementById('city_lat').value = place.geometry.location.lat();
                    document.getElementById('city_long').value = place.geometry.location.lng();
                });
            }
            google.maps.event.addDomListener(window, 'load', initUpdateCity);
        </script>
    @endpush
@endsection