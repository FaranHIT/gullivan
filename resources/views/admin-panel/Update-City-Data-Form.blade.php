@extends('layout.adminpanel.default')
@section('content')
    <section role="main" class="content-body">
        <header class="page-header">
            <h2>Update City Data</h2>

            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="index.html">
                            <i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li><span>Tables</span></li>
                    <li><span>Update City Data</span></li>
                </ol>

                <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
            </div>
        </header>

        <!-- start: page -->

        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>
                <b>Update City Data</b>
            </header>
            <div class="card-body">
                @if(session('success-msg'))
                    <div class="alert alert-success">
                        {{session('success-msg')}}
                    </div>
                @elseif(session('error-msg'))
                    <div class="alert alert-danger">
                        {{session('error-msg')}}
                    </div>
                @elseif(session('alert-msg'))
                    <div class="alert alert-info">
                        {{session('alert-msg')}}
                    </div>
                @endif
                <form method="post" action="{{route('update-city-data')}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="city_id" value="{{$CityData->city_id}}">
                    <div class="form-row justify-content-around">
                        <div class="form-group col-md-8 ">
                            <label>YouTube video link</label>
                            <input type="text" name="Video-Link" value="{{$CityData->youtube}}" class="form-control" placeholder="Enter url">
                            @if($errors->has('Video-Link'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Video-Link') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Best Time To Go</label>
                            <input type="text" name="Best-Time" value="{{$CityData->best_time_to_go}}" class="form-control" placeholder="Enter detail">
                            @if($errors->has('Best-Time'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Best-Time') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Transportation</label>
                            <input type="text" name="Transportation" value="{{$CityData->transportation}}" class="form-control" placeholder="Enter detail">
                            @if($errors->has('Transportation'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Transportation') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Weather</label>
                            <input type="text" name="Weather" value="{{$CityData->weather}}" class="form-control" placeholder="Enter detail">
                            @if($errors->has('Weather'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Weather') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Information</label>
                            <input type="text" name="Information" value="{{$CityData->information}}" class="form-control" placeholder="Enter detail">
                            @if($errors->has('Information'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('Information') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Rent A Car</label>
                            <input type="text" name="Rent-A-Car" value="{{$CityData->rent_a_car}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Real Estate</label>
                            <input type="text" name="Real-Estate" value="{{$CityData->real_estate}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Hotels</label>
                            <input type="text" name="Hotels" value="{{$CityData->hotels}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Coffee Shops</label>
                            <input type="text" name="Coffee" value="{{$CityData->coffee_shops}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Restaurants</label>
                            <input type="text" name="Restaurants" value="{{$CityData->restaurants}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Bars</label>
                            <input type="text" name="Bars" value="{{$CityData->bars}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Add Your Buisness</label>
                            <input type="text" name="Add-Your-Buisness" value="{{$CityData->add_ur_bussiness}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Taxis</label>
                            <input type="text" name="Taxis" value="{{$CityData->taxis}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Road Law</label>
                            <input type="text" name="Road-Law" value="{{$CityData->road_laws}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Lawyers</label>
                            <input type="text" name="Lawyers" value="{{$CityData->lawyers}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-8 ">
                            <label>Events</label>
                            <input type="text" name="Events" value="{{$CityData->events}}" class="form-control" placeholder="Enter detail/url">
                        </div>
    
                        <div class="form-group col-md-8 ">
                            <label>Tours</label>
                            <input type="text" name="Tours" id="Tours" value="{{$CityData->tours}}" class="form-control" placeholder="Enter detail/url">
                        </div>

                        <div class="form-group col-md-5 ">
                            <button type="submit" class="btn btn-primary btn-sm btn-block">update</button>
                        </div>
                        
                    </div>
                </form>
            </div>

        </section>
        <!-- end: page -->
    </section>
@endsection
