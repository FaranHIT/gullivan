@extends('layout.adminpanel.default')
@section('content')
    <section role="main" class="content-body">
        <header class="page-header">
            <h2>Add Category Items</h2>

            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="index.html">
                            <i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li><span>Tables</span></li>
                    <li><span>Add Categories Items</span></li>
                </ol>

                <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
            </div>
        </header>

        <!-- start: page -->

        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>
                <b>Add New Item</b>
            </header>
            <div class="card-body">
                @if(session('success-msg'))
                <div class="alert alert-success">
                    {{session('success-msg')}}
                </div>
                @elseif(session('error-msg'))
                <div class="alert alert-danger">
                    {{session('error-msg')}}
                </div>
                @elseif(session('alert-msg'))
                <div class="alert alert-info">
                    {{session('alert-msg')}}
                </div>
                @endif
                <form action="{{route('SaveCategoryItems')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row justify-content-around">
                        <div class="form-group text-class col-md-8 pt-3">
                            <label style="font-size:20px">Categories</label>
                            <select class="form-control" name="category">
                                <option class="form-control" selected disabled>Choose Category</option>
                                @if(count($all_categories) > 0)
                                    @foreach($all_categories as $category)
                                        <option class="form-control" value="{{$category->id}}">{{$category->category_name}}</option>
                                    @endforeach
                                @else
                                    <option class="form-control"disabled>No Category Has Been Added Yet!</option>
                                @endif
                            </select>
                            @if($errors->has('picture'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('picture') }}</span>
                            @endif
                        </div>
                        <div class="form-group text-class col-md-8 pt-3">
                        <input type="hidden" name="city_id" value="{{$city_id}}" id="category_id" class="form-control">
                            <label style="font-size:20px">Picture</label>
                            <input type="file" name="picture[]" id="picture" class="form-control" multiple>
                            @if($errors->has('picture'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('picture') }}</span>
                            @endif
                        </div>

                        <div class="form-group text-class col-md-8 pt-3">
                            <label style="font-size:20px">Title</label>
                            <input type="text" name="title" autocomplete="off" id="title" class="form-control" placeholder="Enter Title">
                        </div>

                        <div class="form-group text-class col-md-8 pt-3">
                            <label style="font-size:20px">Detail</label>
                            <input type="text" name="detail" autocomplete="off" id="detail" class="form-control" placeholder="Enter Detail">
                        </div>

                        <div class="form-group text-class col-md-8 pt-3">
                            <label style="font-size:20px">Address</label>
                            <input type="text" name="address" autocomplete="off" id="address" class="form-control" placeholder="Enter Address">
                        </div>

                        <div class="form-group text-class col-md-8 pt-3">
                            <label style="font-size:20px">Website</label>
                            <input type="text" name="website" autocomplete="off" id="website" class="form-control" placeholder="Enter Website url">
                        </div>

                        <div class="form-group text-class col-md-8 pt-3">
                            <label style="font-size:20px">Number</label>
                            <input type="text" name="number" autocomplete="off" id="number" class="form-control" placeholder="Enter Number">
                        </div>

                        <div class="form-group text-class col-md-8 pt-3">
                            <label style="font-size:20px">Timing</label>
                            <input type="text" name="monday_time" id="monday_time" autocomplete="off" class="form-control" placeholder="Enter Monday Time">
                            @if($errors->has('monday_time'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('monday_time') }}</span>
                            @endif
                            <input type="text" name="tuesday_time" id="tuesday_time" autocomplete="off" class="form-control" placeholder="Enter Tuesday Time">
                            @if($errors->has('tuesday_time'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('monday_time') }}</span>
                            @endif
                            <input type="text" name="wednesday_time" id="wednesday_time" autocomplete="off" class="form-control" placeholder="Enter Wednesday Time">
                            @if($errors->has('wednesday_time'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('monday_time') }}</span>
                            @endif
                            <input type="text" name="thursday_time" id="thursday_time" autocomplete="off" class="form-control" placeholder="Enter Thursday Time">
                            @if($errors->has('thursday_time'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('monday_time') }}</span>
                            @endif
                            <input type="text" name="friday_time" id="friday_time" autocomplete="off" class="form-control" placeholder="Enter Friday Time">
                            @if($errors->has('friday_time'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('monday_time') }}</span>
                            @endif
                            <input type="text" name="saturday_time" id="saturday_time" autocomplete="off" class="form-control" placeholder="Enter Saturday Time">
                            @if($errors->has('saturday_time'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('monday_time') }}</span>
                            @endif
                            <input type="text" name="sunday_time" id="sunday_time" autocomplete="off" class="form-control" placeholder="Enter Sunday Time">
                            @if($errors->has('sunday_time'))
                                <span class="text-danger" style="font-size: 12px;">{{ $errors->first('monday_time') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-md-5">
                            <button type="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                        </div>
                </form>
            </div>

        </section>
        <!-- end: page -->
    </section>
@endsection
