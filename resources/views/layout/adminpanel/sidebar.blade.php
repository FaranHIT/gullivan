<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header">
        <div class="sidebar-title">
            Navigation
        </div>
        <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
            <img src="{{asset('img/world-guide-logo.png')}}" style="margin-left: 17px;" width="130" height="130" alt="Porto Admin" />
                <ul class="nav nav-main">
                    <li class="{{ Request::is('dashboard') ? 'nav-active' : '' }}">
                        <a class="nav-link" href="{{ url('dashboard') }}">
                            <i class="fas fa-tachometer-alt" aria-hidden="true"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    @role('superadmin')
                    <li class="{{ Request::is('users') ? 'nav-active' : '' }}">
                        <a class="nav-link" href="{{ route('users') }}">
                            <i class="fas fa-users" aria-hidden="true"></i>
                            <span>Users</span>
                        </a>
                    </li>
                    @endrole

                    @role(['superadmin','admin','manager'])
                    <li class="nav-parent {{ Request::is('countries') ? 'nav-active' : '' }}">
                        <a class="nav-link" href="#">
                            <i class="fas fa-flag" aria-hidden="true"></i>
                            <span>Countries</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="{{ url('countries') }}">
                                    <i class="fas fa-eye" aria-hidden="true"></i>
                                    <span>View</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{route('addCountryForm')}}">
                                    <i class="fas fa-plus" aria-hidden="true"></i>
                                    <span>Add New</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent {{ Request::is('cities') ? 'nav-active' : '' }}">
                        <a class="nav-link" href="#">
                            <i class="fas fa-city" aria-hidden="true"></i>
                            <span>Cities</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="{{ url('cities') }}">
                                    <i class="fas fa-eye" aria-hidden="true"></i>
                                    <span>View</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{ route('addCityForm') }}">
                                    <i class="fas fa-plus" aria-hidden="true"></i>
                                    <span>Add New</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-parent {{ Request::is('cities') ? 'nav-active' : '' }}">
                        <a class="nav-link" href="#">
                            <i class="fas fa-city" aria-hidden="true"></i>
                            <span>Categories</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="{{route('view-category-form')}}">
                                    <i class="fas fa-eye" aria-hidden="true"></i>
                                    <span>View</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{ route('add-category-form') }}">
                                    <i class="fas fa-plus" aria-hidden="true"></i>
                                    <span>Add New</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endrole

                    @role('superadmin')
                    <li class="nav-parent">
                        <a>
                            <i class="fas fa-key" aria-hidden="true"></i>
                            <span>RBAC</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="nav-parent">
                            <li>
                                <a class="nav-link" href="{{ url('assign-remove-admin') }}">
                                <!-- <i class="fas fa-plus" aria-hidden="true"></i> -->
                                    Assign & Remove Admin Role
                                </a>
                                </li>
                            </li>

                            <li>
                                <a class="nav-link" href="{{ url('view-admin') }}">
                                <!-- <i class="fas fa-plus" aria-hidden="true"></i> -->
                                    View Admin Role
                                </a>
                                </li>
                            </li>

                            <li>
                                <a class="nav-link" href="{{route('unallocated-managers') }}">
                                <!-- <i class="fas fa-plus" aria-hidden="true"></i> -->
                                    Unallocated Managers
                                </a>
                                </li>
                            </li>

                            <li>
                                <a class="nav-link" href="{{ url('assign-remove-role') }}">
                                <!-- <i class="fas fa-plus" aria-hidden="true"></i> -->
                                    Assign & Remove Manager Role
                                </a>
                                </li>
                            </li>

                            <li>
                                <a class="nav-link" href="{{ url('view-manager') }}">
                                <!-- <i class="fas fa-plus" aria-hidden="true"></i> -->
                                    View Manager Role
                                </a>
                            </li>

                            <li class="nav-parent">
                            <li>
                                <a class="nav-link" href="{{ url('assign-remove-permissions') }}">
                                <!-- <i class="fas fa-plus" aria-hidden="true"></i> -->
                                    Assign & Remove Permissions
                                </a>
                                </li>

                            </li>
                        </ul>
                    </li>
                    @endrole

                    @role('admin')
                    <li class="nav-parent">
                        <a>
                            <i class="fas fa-key" aria-hidden="true"></i>
                            <span>RBAC</span>
                        </a>
                        <ul class="nav nav-children">

                            <li>
                                <a class="nav-link" href="{{ url('assign-remove-role') }}">
                                <!-- <i class="fas fa-plus" aria-hidden="true"></i> -->
                                    Assign & Remove Manager Role
                                </a>
                                </li>
                            </li>

                            <li>
                                <a class="nav-link" href="{{ url('view-manager') }}">
                                <!-- <i class="fas fa-plus" aria-hidden="true"></i> -->
                                    View Manager Role
                                </a>
                            </li>

                            <li class="nav-parent">
                            <li>
                                <a class="nav-link" href="{{ url('assign-remove-permissions') }}">
                                <!-- <i class="fas fa-plus" aria-hidden="true"></i> -->
                                    Assign & Remove Permissions
                                </a>
                                </li>

                            </li>
                        </ul>
                    </li>
                    @endrole

                    @role(['superadmin','admin','manager'])
                    <li class="nav-parent">
                        <a>
                            <i class="fa fa-bell" aria-hidden="true"></i>
                            <span>Notifications</span>
                        </a>
                    <ul class="nav nav-children">
                                <li>
                                <a class="nav-link" href="{{ url('notifications') }}">
                                <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                    Send Notifications
                                </a>
                                </li>
                                <li>
                            <a class="nav-link" href="{{url('view-notifications') }}">
                                <i class="fas fa-eye" aria-hidden="true"></i>
                                    View Notification History
                                </a>
                            </li>
                            </ul>
                     @endrole

                        @role(['superadmin','admin'])
                        <li class="{{ Request::is('users-lock-unlock') ? 'nav-active' : '' }}">
                            <a class="nav-link" href="{{ url('users-lock-unlock') }}">
                            <i class="fas fa-user-lock" aria-hidden="true"></i>
                            <span>Users Lock/Unlock</span>
                            </a>
                        </li>
                        @endrole

                        @role(['superadmin','admin','manager'])
                        <li class="{{ Request::is('import-data') ? 'nav-active' : '' }}">
                            <a class="nav-link" href="{{ url('import-data') }}">
                                <i class="fas fa-file-import" aria-hidden="true"></i>
                                <span>Import Data</span>
                            </a>
                        </li>
                        @endrole
                </ul>
            </nav>
        </div>

        <script>
            // Maintain Scroll Position
            if (typeof localStorage !== 'undefined') {
                if (localStorage.getItem('sidebar-left-position') !== null) {
                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                        sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                    sidebarLeft.scrollTop = initialPosition;
                }
            }
        </script>


    </div>

</aside>
<!-- end: sidebar -->
