<!doctype html>
<html class="fixed sidebar-light">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>
            {{ config('app.name', 'Laravel') }} | Dashboard
        </title>
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link rel="shortcut icon" href="#">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,800,900|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{!! asset('vendor/bootstrap/css/bootstrap.css') !!}" />
		<link rel="stylesheet" href="{!! asset('vendor/animate/animate.css') !!}">

		<link rel="stylesheet" href="{!! asset('vendor/font-awesome/css/all.min.css') !!}" />
		<link rel="stylesheet" href="{!! asset('vendor/magnific-popup/magnific-popup.css') !!}" />
		<link rel="stylesheet" href="{!! asset('vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css') !!}" />

		<!-- JVectorMap -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqvmap/1.5.1/jqvmap.min.css">
		
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="{!! asset('vendor/jquery-ui/jquery-ui.css') !!}" />
		<link rel="stylesheet" href="{!! asset('vendor/jquery-ui/jquery-ui.theme.css') !!}" />
		<link rel="stylesheet" href="{!! asset('vendor/bootstrap-multiselect/css/bootstrap-multiselect.css') !!}" />
		<link rel="stylesheet" href="{!! asset('vendor/datatables/media/css/dataTables.bootstrap4.css') !!}" />
		<link rel="stylesheet" href="{!! asset('vendor/datatables/media/css/dataTables.bootstrap4.min.css') !!}" />
{{--        <link rel="stylesheet" href="{!! asset('vendor/datatables/media/css/jquery.dataTables.min.css') !!}" />--}}
{{--        <link rel="stylesheet" href="{!! asset('vendor/datatables/media/css/jquery.dataTables.css') !!}" />--}}
		<link rel="stylesheet" href="{!! asset('vendor/morris/morris.css') !!}" />

		<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
		
		<!-- jquery load first for jvectormap -->
		<script src="https://code.jquery.com/jquery-latest.min.js"></script>

		<!--(remove-empty-lines-end)-->

		<!-- Theme CSS -->
		<link rel="stylesheet" href="{!! asset('css/adminpanel/theme.css') !!}" />

		<!--(remove-empty-lines-end)-->

		<!-- Toaster CSS -->
		<link rel="stylesheet" href="{!! asset('vendor/toaster/css/toastr.min.css') !!}" />

		<!-- fullcalendar -->
		<link rel="stylesheet" href="{!! asset('vendor/fullcalendar/fullcalendar.css') !!}" />



		<!-- Skin CSS -->
		<link rel="stylesheet" href="{!! asset('css/adminpanel/skins/default.css') !!}" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{!! asset('css/adminpanel/custom.css') !!}">

		<!-- Head Libs -->
		<script src="{!! asset('vendor/modernizr/modernizr.js') !!}"></script>
        <style>
            div.dataTables_wrapper div.dataTables_filter input{
                /*margin-left: -44px !important;*/
            }
			.fc-time{
				display:none !important;
			}
        </style>
	</head>
	<body>
		<section class="body">
			@include('layout.adminpanel.header')
			<div class="inner-wrapper">
				@include('layout.adminpanel.sidebar')
                @yield('content')
			</div>
		</section>

		<!-- Vendor -->
		<script src="{!! asset('vendor/jquery/jquery.js') !!}"></script>
		<script src="{!! asset('vendor/jquery-browser-mobile/jquery.browser.mobile.js') !!}"></script>
		<script src="{!! asset('vendor/popper/umd/popper.min.js') !!}"></script>
		<script src="{!! asset('vendor/bootstrap/js/bootstrap.js') !!}"></script>
		<script src="{!! asset('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}"></script>
		<script src="{!! asset('vendor/common/common.js') !!}"></script>
		<script src="{!! asset('vendor/nanoscroller/nanoscroller.js') !!}"></script>
		<script src="{!! asset('vendor/magnific-popup/jquery.magnific-popup.js') !!}"></script>
		<script src="{!! asset('vendor/jquery-placeholder/jquery.placeholder.js') !!}"></script>

		<!-- Specific Page Vendor -->
		<script src="{!! asset('vendor/jquery-ui/jquery-ui.js') !!}"></script>
		<script src="{!! asset('vendor/jqueryui-touch-punch/jquery.ui.touch-punch.js') !!}"></script>
		<script src="{!! asset('vendor/jquery-appear/jquery.appear.js') !!}"></script>
		<script src="{!! asset('vendor/bootstrap-multiselect/js/bootstrap-multi') !!}select.js"></script>
		<script src="{!! asset('vendor/jquery.easy-pie-chart/jquery.easypiechart.js') !!}"></script>
		<script src="{!! asset('vendor/flot/jquery.flot.js') !!}"></script>
		<script src="{!! asset('vendor/flot.tooltip/jquery.flot.tooltip.js') !!}"></script>
		<script src="{!! asset('vendor/flot/jquery.flot.pie.js') !!}"></script>
		<script src="{!! asset('vendor/flot/jquery.flot.categories.js') !!}"></script>
		<script src="{!! asset('vendor/flot/jquery.flot.resize.js') !!}"></script>
		<script src="{!! asset('vendor/jquery-sparkline/jquery.sparkline.js') !!}"></script>
		<script src="{!! asset('vendor/raphael/raphael.js') !!}"></script>
		<script src="{!! asset('vendor/morris/morris.js') !!}"></script>
		<script src="{!! asset('vendor/gauge/gauge.js') !!}"></script>
		<script src="{!! asset('vendor/snap.svg/snap.svg.js') !!}"></script>
		<script src="{!! asset('vendor/liquid-meter/liquid.meter.js') !!}"></script>
	
		<!-- fullcalendar -->
		<script src="{!! asset('vendor/moment/moment.js') !!}"></script>
		<script src="{!! asset('vendor/fullcalendar/fullcalendar.js') !!}"></script>
		
		<!-- <script src="{!! asset('vendor/jqvmap/jquery.vmap.js') !!}"></script>
		<script src="{!! asset('vendor/jqvmap/data/jquery.vmap.sampledata.js') !!}"></script>
		<script src="{!! asset('vendor/jqvmap/maps/jquery.vmap.world.js') !!}"></script>
		<script src="{!! asset('vendor/jqvmap/maps/continents/jquery.vmap.africa.js') !!}"></script>
		<script src="{!! asset('vendor/jqvmap/maps/continents/jquery.vmap.asia.js') !!}"></script>
		<script src="{!! asset('vendor/jqvmap/maps/continents/jquery.vmap.australia.js') !!}"></script>
		<script src="{!! asset('vendor/jqvmap/maps/continents/jquery.vmap.europe.js') !!}"></script>
		<script src="{!! asset('vendor/jqvmap/maps/continents/jquery.vmap.north-america.js') !!}"></script>
		<script src="{!! asset('vendor/jqvmap/maps/continents/jquery.vmap.south-america.js') !!}"></script> -->
	
	<!-- jvectormap -->
	<!-- <script src="https://code.jquery.com/jquery-latest.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqvmap/1.5.1/jquery.vmap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqvmap/1.5.1/maps/jquery.vmap.world.js"></script>		
	<!-- end jvectormap	 -->
	
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

		<!--(remove-empty-lines-end)-->

		<!-- Theme Base, Components and Settings -->
		<script src="{!! asset('js/adminpanel/theme.js') !!}"></script>

		<!-- Theme Custom -->
		<script src="{!! asset('js/adminpanel/custom.js') !!}"></script>

		<!-- Theme Initialization Files -->
		<script src="{!! asset('js/adminpanel/theme.init.js') !!}"></script>

		<!-- toaster -->
		<script src="{!! asset('vendor/toaster/js/toastr.min.js') !!}"></script>

		<!-- Examples -->
		<script src="{!! asset('js/adminpanel/examples/examples.dashboard.js') !!}"></script>
		@stack('scripts')
	</body>
</html>
