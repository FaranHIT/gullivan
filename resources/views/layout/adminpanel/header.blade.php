<!-- start: header -->
<header class="header">
    <div class="logo-container">
        <a href="{{ url('dashboard') }}" class="logo">
            <!-- <img src="{{asset('img/world-guide-logo.png')}}" width="45" height="45" alt="Porto Admin" /> -->
            <h style="font-size: large;color: black;font-size: xx-large;">World Guide Holding Group</h>
        </a>
        <div class="d-md-none toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <!-- start: search & user box -->
    <div class="header-right">

        <span class="separator"></span>

        <div id="userbox" class="userbox">
            <a href="#" data-toggle="dropdown">
                <figure class="profile-picture">
                    <img src="{{asset('storage/'.'profile-'.Auth::id().'.png')}}" alt="Joseph Doe" class="rounded" data-lock-picture="img/!logged-user.jpg" />
                </figure>
                <div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
                    <span class="name">{{ Auth::user()->user_nicename }}</span>
                    @role(['admin','superadmin','manager'])
                    <span class="role">{{Auth::user()->roles->first()->display_name}}</span>
                    @endrole
                </div>

                <i class="fa custom-caret"></i>
            </a>

            <div class="dropdown-menu">
                <ul class="list-unstyled mb-2">
                    <li class="divider"></li>
                    <!-- <li>
                        <a role="menuitem" tabindex="-1" href="pages-user-profile.html"><i class="fas fa-user"></i> My Profile</a>
                    </li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fas fa-lock"></i> Lock Screen</a>
                    </li> -->
                    <li>
                        <a role="menuitem" tabindex="-1" href="{{ url('logout') }}"><i class="fas fa-power-off"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end: search & user box -->
</header>
<!-- end: header -->