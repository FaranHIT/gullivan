<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCityCategoryDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('city_category_details', function (Blueprint $table) {

            $table->dropColumn('timing');
            $table->string('moday_time');
            $table->string('tuesday_time');
            $table->string('wednesday_time');
            $table->string('thursday_time');
            $table->string('friday_time');
            $table->string('saturday_time');
            $table->string('sunday_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('city_category_details', function (Blueprint $table) {

        });
    }
}
