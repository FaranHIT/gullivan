<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateEnumToFriendRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE friend_requests CHANGE COLUMN status status ENUM('pending','confirmed','rejected','blocked','unblocked') NOT NULL DEFAULT 'pending'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE friend_requests CHANGE COLUMN status status ENUM('pending','confirmed','rejected','blocked') NOT NULL DEFAULT 'pending'");
    }
}
