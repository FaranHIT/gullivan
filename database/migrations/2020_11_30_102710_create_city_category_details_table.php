<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCityCategoryDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_category_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id');
            $table->string('picture')->nullable();
            $table->string('title')->nullable();
            $table->string('details')->nullable();
            $table->string('address')->nullable();
            $table->string('website')->nullable();
            $table->string('number')->nullable();
            $table->string('timing')->nullable();

            $table->foreign('category_id')->references('id')->on('city_categories')->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('city_category_details');
        Schema::enableForeignKeyConstraints();
    }
}
