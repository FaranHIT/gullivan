<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCityDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_data', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('city_id');
            $table->text('rent_a_car')->nullable();
            $table->text('real_estate')->nullable();
            $table->text('hotels')->nullable();
            $table->text('coffee_shops')->nullable();
            $table->text('restaurants')->nullable();
            $table->text('bars')->nullable();
            $table->text('add_ur_bussiness')->nullable();
            $table->text('taxis')->nullable();
            $table->text('road_laws')->nullable();
            $table->text('lawyers')->nullable();
            $table->text('events')->nullable();
            $table->text('tours')->nullable();
            $table->text('youtube');
            $table->text('best_time_to_go');
            $table->text('transportation');
            $table->text('weather');
            $table->text('information');

            $table->foreign('city_id')->references('id')->on('cities')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('city_data');
        Schema::enableForeignKeyConstraints();
    }
}
