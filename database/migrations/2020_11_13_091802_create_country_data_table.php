<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountryDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_data', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('country_id');
            $table->text('constitution')->nullable();
            $table->text('emergency_no')->nullable();
            $table->text('embassies')->nullable();
            $table->text('new_offer')->nullable();
            $table->text('rules')->nullable();
            $table->text('history')->nullable();
            $table->text('driving')->nullable();
            $table->text('universities')->nullable();
            $table->text('visa_uni_acceptance_fee')->nullable();
            $table->text('tours')->nullable();
            $table->text('youtube_link');
            $table->text('best_time_to_go');
            $table->text('transportation');
            $table->text('weather');
            $table->text('information');
            $table->text('the_electric');
            $table->text('language');
            $table->text('currency');

            $table->foreign('country_id')->references('id')->on('countries')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('country_data');
        Schema::enableForeignKeyConstraints();
    }
}
