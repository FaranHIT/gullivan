<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_notifications', function (Blueprint $table) {
            $table->json('user_id')->change();
            $table->json('countries');
            $table->json('nationalities');
            $table->json('languages');
            $table->json('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_notifications', function (Blueprint $table) {
            $table->json('user_id')->change();
            $table->json('countries');
            $table->json('nationalities');
            $table->json('languages');
            $table->json('users');
        });
    }
}
