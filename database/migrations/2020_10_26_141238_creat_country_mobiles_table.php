<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatCountryMobilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_mobiles', function (Blueprint $table) {
            $table->id();
            $table->string('country_name_en');
            $table->string('country_name_cn');
            $table->string('country_name_ar');
            $table->string('country_name_ru');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_mobiles');
    }
}
