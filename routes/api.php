<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Login/Sign-up
Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::get('get-countries-mobile', 'API\ProfileController@GetCountriesMobile');
Route::post('forgot-password','API\UserController@forgotPassword');
Route::post('check-username','API\UserController@CheckUsername');
Route::post('check-email','API\UserController@CheckEmail');




Route::post('details', 'API\UserController@details');
//Profile Section
Route::group(['middleware' => ['auth:api']], function () {
	Route::post('get-profile', 'API\ProfileController@getProfile');
	Route::post('update-profile', 'API\ProfileController@updateProfile');
	Route::post('update-profile-picture','API\ProfileController@UpdateProfilePicture');
	Route::post('update-cover-photo', 'API\ProfileController@UpdateCoverPhoto');
	Route::post('send-request','API\FriendController@sendFriendRequest');
	Route::post('request-feedback','API\FriendController@FriendRequestFeedback');
	Route::get('freind-for-sender','API\FriendController@FriendForSender');
	Route::get('freind-for-recipient','API\FriendController@FriendForRecipient');
	Route::post('all-users/','API\UserController@getAllUser');
	Route::post('friends-status/','API\UserController@friendsStatus');
	Route::post('all-pending-request','API\UserController@getAllPendingRequest');
	Route::get('/accept-request/{id}', 'API\FriendController@acceptRequest');
	Route::get('/deny-request/{id}','API\FriendController@denyRequest');
	Route::post('block-users/','API\UserController@getBlockUser');
    Route::post('friend-request-status-update','API\FriendController@FriendRequestStatusUpdate');
	Route::post('block-unblock','API\FriendController@BlockUnblock');
	Route::post('logout','API\UserController@logout');
	Route::post('sender-profile', 'API\ProfileController@SenderProfile');
	Route::post('accept-request-profile', 'API\ProfileController@RecipientProfile');
	Route::post('check-user-profile', 'API\ProfileController@CheckUserProfile');
	Route::post('update-device-token', 'API\ProfileController@updateDeviceToken');
	Route::post('update-password','API\UserController@UpdatePassword');
	Route::post('save-status', 'API\UserController@SaveStatus');
	Route::get('get-status-lock-unlock','API\ProfileController@GetStatusLockUnlock')->name('get-status-lock-unlock');
	Route::post('private-status', 'API\UserController@PrivateStatus');
	Route::get('notifications-by-admin-panel','API\UserController@NotificationByAdmin');
	Route::post('country-data', 'API\ProfileController@CountryData');
	Route::post('city-data', 'API\ProfileController@CityData');
	
});

// Route::get('/send-request/{id}', 'API\FriendController@sendrequest');
//Route::group(['middleware' => 'auth:api'], function(){
//Friend-Request-Section
//Route::get('/send-request/{sender_id}', 'API\FriendController@sendRequest');




//});




// Route::get('all-pending-request','API\FriendController@getAllUserPendingRequest');
// Route::get('user-profile','API\FriendController@getProfile');
// Route::get('/remove-friend/{id}','API\FriendController@removeFriend');
// Route::get('/pending-request','API\FriendController@pendingRequest');
// Route::get('/all-app-users','API\FriendController@appUsers');
//Route::post('/search-friends','API\FriendController@searchFriend');


Route::prefix('elasticsearch')->group(function(){
   Route::get('/search','API\FriendController@search');
});


