<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Redirect::to("login");
});

Route::match(['get', 'post'], '/login', 'AuthController@login')->name('login');
Route::get('reset-password/{id?}/{token?}/{token_id?}','AuthController@resetPasswordForm')->name('resetPasswordForm');
Route::post('reset-pass','AuthController@resetPassword')->name('resetPassword');
Route::group(['middleware' => ['auth']], function () {
	Route::get('logout', 'AuthController@logout')->name('logout');
	Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
	Route::get('/users', 'DashboardController@users')->name('users');
	Route::get('/dashboard-users', 'DashboardController@DashboardUsers')->name('dashboard-users');

	Route::get('/assign-remove-role','RbacController@AssignRemoveRole')->name('assign-remove-role');
	Route::get('assign-role/{ID?}','RbacController@AssignRole')->name('assign-role');
	Route::get('remove-role/{ID?}','RbacController@RemoveRole')->name('remove-role');
	Route::get('/assign-remove-admin','RbacController@AssignRemoveAdmin')->name('assign-remove-admin');
	Route::get('assign-admin/{ID?}','RbacController@AssignAdmin')->name('assign-admin');
	Route::get('remove-admin/{ID?}','RbacController@RemoveAdmin')->name('remove-admin');
	Route::get('view-admin','RbacController@ViewAdmin')->name('view-admin');
	Route::get('view-manager','RbacController@ViewManager')->name('view-manager');
	Route::get('unallocated-managers','RbacController@UnallocatedManager')->name('unallocated-managers');
	Route::get('/admins','RbacController@Admins')->name('admins');
	Route::get('/allocate-admin/{managerID?}/{adminID?}','RbacController@AllocateAdmin')->name('allocateAdmin');

	Route::get('/assign-remove-permissions','RbacController@AssignRemovePermissions')->name('assign-remove-permissions');
	Route::get('assign-permissions-form/{ID?}','RbacController@AssignPermissionsForm')->name('assign-permissions-form');
	Route::post('post-assign-permissions-form','RbacController@PostAssignPermissionsForm')->name('post-assign-permissions-form');
	Route::get('remove-permissions-form/{ID?}','RbacController@RemovePermissionsForm')->name('remove-permissions-form');
	Route::post('post-remove-permissions-form','RbacController@PostRemovePermissionsForm')->name('post-remove-permissions-form');
	Route::get('select-countries-to-cities/{id?}','RbacController@SelectCountriesToCities')->name('select-countries-to-cities');
	Route::get('assign-country-permissions-form/{ID?}','RbacController@AssignCountryPermissionsForm')->name('assign-country-permissions-form');
	Route::post('post-assign-country-permissions-form','RbacController@PostAssignCountryPermissionsForm')->name('post-assign-country-permissions-form');
	Route::get('/view-country-permissions/{ID?}','RbacController@ViewCountryPermissions')->name('view-country-permissions');
	Route::get('/view-city-permissions/{ID?}','RbacController@ViewCityPermissions')->name('view-city-permissions');


	Route::get('/countries','AdminController@countries')->name('countries');
    Route::get('/get-countries','AdminController@getCountries')->name('getCountries');
    Route::get('/add-countries','AdminController@addCountryForm')->name('addCountryForm');
	Route::post('/save-country', 'AdminController@saveCountries')->name('saveCountries');
    Route::get('/update-countries/{id?}','AdminController@updateCountryForm')->name('updateCountryForm');
    Route::post('/update-country/{id?}','AdminController@updateCountry')->name('updateCountry');
	Route::get('/remove-country/{cid?}/{pwd?}','AdminController@removeCountry')->name('removeCountry');
    Route::get('/cities','AdminController@cities')->name('cities');
    Route::get('/add-cities','AdminController@addCityForm')->name('addCityForm');
    Route::post('/save-city','AdminController@saveCity')->name('saveCity');
    Route::get('/update-cities/{id?}','AdminController@updateCityForm')->name('updateCityForm');
    Route::post('/update-city/{id?}','AdminController@updateCity')->name('updateCity');
	Route::get('/remove-city/{cid?}/{pwd?}','AdminController@removeCity')->name('removeCity');
	Route::get('/country-data-form/{id?}','AdminController@CountryDataForm')->name('country-data-form');
	Route::post('/save-country-data','AdminController@SaveCountryData')->name('save-country-data');
	Route::get('/update-country-data-form/{id?}','AdminController@UpdateCountryDataForm')->name('update-country-data-form');
	Route::post('/update-country-data','AdminController@UpdateCountryData')->name('update-country-data');
	Route::get('/view-country-data-form/{id?}','AdminController@ViewCountryDataForm')->name('view-country-data-form');
	Route::get('/city-data-form/{id?}','AdminController@CityDataForm')->name('city-data-form');
	Route::post('/save-city-data','AdminController@SaveCityData')->name('save-city-data');
	Route::get('/update-city-data-form/{id?}','AdminController@UpdateCityDataForm')->name('update-city-data-form');
	Route::post('/update-city-data','AdminController@UpdateCityData')->name('update-city-data');
	Route::get('/view-city-data-form/{id?}','AdminController@ViewCityDataForm')->name('view-city-data-form');
	Route::get('/add-category-form','AdminController@AddCategoryForm')->name('add-category-form');
	Route::get('/view-category-form','AdminController@ViewCategoryForm')->name('view-category-form');
	Route::post('/save-category','AdminController@SaveCategory')->name('SaveCategory');
	Route::get('/add-category-items-form/{id?}','AdminController@CategoryItemsForm')->name('add-category-items-form');
	Route::post('/save-category-items','AdminController@SaveCategoryItems')->name('SaveCategoryItems');
	Route::get('/delete-category/{id?}','AdminController@DeleteCategory')->name('DeleteCategory');
	Route::get('/view-category-items-form/{id?}','AdminController@ViewCategoryItems')->name('ViewCategoryItems');
	Route::get('/update-category-items-form/{id?}','AdminController@UpdateCategoryItemsForm')->name('UpdateItems');
	Route::post('/update-categories-items','AdminController@UpdateCategoryItems')->name('PostUpdateItems');
	Route::get('/delete-categories-items/{id?}','AdminController@DeleteCategoryItems')->name('DeleteItems');

	Route::get('notifications','NotificationsController@Notifications')->name('notifications');
	Route::post('send-notifications','NotificationsController@SendNotifications')->name('send-notifications');
	Route::get('view-notifications','NotificationsController@ViewNotifications')->name('view-notifications');
	Route::get('/resend-notificatons/{notification_id?}','NotificationsController@ResendNotifications')->name('resend-notificatons');
	Route::get('more-countries/{id?}','NotificationsController@MoreCountries')->name('more-countries');
	Route::get('more-nationalities/{id?}','NotificationsController@MoreNationalities')->name('more-nationalities');
	Route::get('more-languages/{id?}','NotificationsController@MoreLanguages')->name('more-languages');
	Route::get('more-users/{id?}','NotificationsController@MoreUsers')->name('more-users');

	Route::get('users-lock-unlock','LockUnlockController@UsersLockUnlock')->name('users-lock-unlock');
	Route::get('lock-user/{ID?}','LockUnlockController@LockUser')->name('lock-user');
	Route::get('unlock-user/{ID?}','LockUnlockController@UnlockUser')->name('unlock-user');

	Route::get('/check-country-users/{country_name?}','DashboardController@checkCountryUsers')->name('checkCountryUsers');

	Route::get('fullcalendar','FullCalendarController@index')->name('index');
	Route::post('fullcalendar/create','FullCalendarController@create')->name('create');
	Route::post('fullcalendar/update','FullCalendarController@update')->name('update');
	Route::post('fullcalendar/delete','FullCalendarController@destroy')->name('destroy');

	Route::get('/to-do-list','DashboardController@toDoList')->name('to-do-list');

    Route::get('/import-data','AdminController@ImportDataView')->name('ImportDataView');
    Route::post('/save-file-data','AdminController@saveFileData')->name('saveFileData');
});


Route::get('/home', 'HomeController@index')->name('home');
